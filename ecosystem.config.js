// This solves multiapp by environment with PM2
const CURRENT_ENV =
  process.argv.indexOf("--env") === -1
    ? ""
    : "-" + process.argv[process.argv.indexOf("--env") + 1];
module.exports = {
  apps: [
    {
      name: "Caustica" + CURRENT_ENV,
      script: "npm",
      args:"start",
      max_memory_restart: "300M", //activar en caso de que haya un 502 con un límite de memoria
      instances: "4",
      exec_mode: "cluster",
      watch: false,      
      watch_options: {
        followSymlinks: false,
      },
      env_development: {
        PORT: 3035,
        URL_DOMAIN_ELTIEMPO: "http://elastic2.ariadna.co:9200/elasticsearch_index_bbdd_drupal_caustica_caustica_buscador/_search?",
        BASE_SERVER: "http://localhost:3035",
        URL_ELTIEMPO: "https://caustica.eltiempo.com.co/",
        URL_ELTIEMPO_IMG: "https://dev-caustica.ariadna.co",
        ELASTIC_NODE: "http://elastic2.ariadna.co:9200",
        ELASTIC_INDEX: "elasticsearch_index_bbdd_drupal_caustica_caustica_buscador",
        EMPTOR: JSON.stringify({
          BASIC_TOKEN:
          "Basic ZWx0aWVtcG9hbmRyb2lkbmF0aXZvOjdIeEViVmVnNjJ0NW9FYVRDaU1C",
          BASE_DOMAIN: "https://seg.eltiempo.com/",
          REGISTER: {
            BASIC: "v2/{CLIENT_ID}/user/create/source/default",
          },
          LOGIN: {
            BASIC: "v2/server/login/{CLIENT_ID}/password/basic/",
            GOOGLE: "v2/social/google-login/",
            FACEBOOK: "v2/social/facebook-login/{CLIENT_ID}",
          },
          LOGOUT: {
            BASIC: "v2/server/logout/{CLIENT_ID}",
          },
          RECOVERY: "v2/{CLIENT_ID}/user/reset",
          RECOVERY_PASS: "v2/{CLIENT_ID}/user/forget/pass",
          USER_ZONE: {
            UPDATE_USER: "v2/{CLIENT_ID}/user/edit",
            CHANGE_EMAIL: "v2/{CLIENT_ID}/user/change-email",
            CHANGE_PASSWORD: "v2/{CLIENT_ID}/user/reset/changepass",
          },
          ME: {
            ME: "v2/verifytoken",
          },
        }),
        CLIENT_ID: "betacaustica",
        GOOGLE: JSON.stringify({
          GTM: {
            CONTAINER_ID: "GTM-5CHKJS8",
          },
        }),
      },
      env_production: {
        PORT: 3035,
        URL_DOMAIN_ELTIEMPO: "http://elastic2.ariadna.co:9200/elasticsearch_index_bbdd_drupal_caustica_caustica_buscador/_search?",
        BASE_SERVER: "https://dev-caustica-react.ariadna.co",
        URL_ELTIEMPO: "https://caustica.eltiempo.com.co/",
        URL_ELTIEMPO_IMG: "https://dev-caustica.ariadna.co",
        ELASTIC_NODE: { "node": "http://elastic2.ariadna.co:9200" },
        ELASTIC_INDEX: "elasticsearch_index_bbdd_drupal_caustica_caustica_buscador",
        EMPTOR: JSON.stringify({
          BASIC_TOKEN:
          "Basic ZWx0aWVtcG9hbmRyb2lkbmF0aXZvOjdIeEViVmVnNjJ0NW9FYVRDaU1C",
          BASE_DOMAIN: "https://seg.eltiempo.com/",
          REGISTER: {
            BASIC: "v2/{CLIENT_ID}/user/create/source/default",
          },
          LOGIN: {
            BASIC: "v2/server/login/{CLIENT_ID}/password/basic/",
            GOOGLE: "v2/social/google-login/{CLIENT_ID}",
            FACEBOOK: "v2/social/facebook-login/{CLIENT_ID}",
          },
          LOGOUT: {
            BASIC: "v2/server/logout/{CLIENT_ID}",
          },
          RECOVERY: "v2/{CLIENT_ID}/user/reset",
          RECOVERY_PASS: "v2/{CLIENT_ID}/user/forget/pass",
          USER_ZONE: {
            UPDATE_USER: "v2/{CLIENT_ID}/user/edit",
            CHANGE_EMAIL: "v2/{CLIENT_ID}/user/change-email",
            CHANGE_PASSWORD: "v2/{CLIENT_ID}/user/reset/changepass",
          },
          ME: {
            ME: "v2/verifytoken",
          },
        }),
        CLIENT_ID: "betacaustica",
        GOOGLE: JSON.stringify({
          GTM: {
            CONTAINER_ID: "GTM-5CHKJS8",
          },
        }),
      },
    },
  ],

  deploy: {
    production: {
      user: "node",
      host: "212.83.163.1",
      ref: "origin/master",
      repo: "git@github.com:repo.git",
      path: "/var/www/production",
      "post-deploy":
        "npm install && pm2 reload ecosystem.config.js --env production",
    },
  },
};
