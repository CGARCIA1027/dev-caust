const express = require("express");
const next = require("next");
const cors = require("cors");
const compression = require("compression");
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const port = process.env.PORT || 3035;
const { join } = require("path");
const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: "http://elastic2.ariadna.co:9200" })

app
  .prepare()
  .then(() => {
    const server = express();
    server.use(compression());
    server.use("/", express.static("public"));   
    server.use(cors());
    
     server.get("/api", async (req, res) => {
      const searchQuery = req.query.search      
      await client.search({               
              index: process.env.ELASTIC_INDEX,                            
              body: {                 
                  "query": {
                    "bool": {
                      "must": [
                        {
                          "multi_match": {
                            "query": searchQuery,
                            "fields": [
                              "title^5.0",
                              "descripcion_corta^3.0",                              
                            ],
                            "type": "best_fields",
                            "operator": "and",
                            "fuzziness": "auto"
                          }
                        }
                      ]
                    }
                  }               
              }
          })
          .then(response => {                 
              return res.json(response)
          })
          .catch(err => {
              return res.status(500).json({"message": "Error"})
          })    
    })

    server.get("/api/elastic", (req, res) => {
      let searchQuery = req.query
      // console.log(searchQuery)
        client.search({        
              index: process.env.ELASTIC_INDEX,
              body: {
                  size: "100",
                  query: {
                      match: searchQuery                  
                  }
              }
          })
          .then(response => {                                        
              return res.json(response)
          })
          .catch(err => {   
            // console.log(err)        
              return res.status(500).json({"message": "Error"})
          })
    })

    server.get("/api/elastic/random", (req, res) => {     
      const searchQuery = Object.values(req.query)[0].split(',')     
          client.search({        
                  index: process.env.ELASTIC_INDEX,
                  body: {
                      size: "120",
                      query: {
                        "terms": {
                          "name": searchQuery,
                          "boost": 1.0
                        }                
                      }
                  }
              })
              .then(response => {                 
                  return res.json(response)
              })
              .catch(err => {
                  return res.status(500).json({"message": "Error"})
              })
      
    })
    server.get("/api/random/filters", (req, res) => { 
      let filtros = {
        "bool": {
            "should": []
        }
      }
      let entries = Object.entries(req.query)
      
      try{
        if(entries[0][1].length > 0){
          if(entries[0][1].includes('*')){
            filtros.bool.should.push({         
              "match_all": {},
            })
          }else{
            for(var h = 0; h < entries[0][1].split(',').length; h++){
              let filterValue = entries[0][1].split(',')[h]
                if(filterValue !== ''){
                filtros.bool.should.push({         
                  "match": {
                    "subcategoria": filterValue
                  },
                })
              }         
            }
          }  
        }

        if(entries[1][1].length > 0){
          if(entries[1][1].includes('*')){
            filtros.bool.should.push({         
              "match_all": {},
            })
          }else{
            for(var m = 0; m < entries[1][1].split(',').length; m++){
              let filterValue = entries[1][1].split(',')[m]
              let fieldName = entries[1][0]
              filtros.bool.should.push({         
                "match": {                 
                  "name":   filterValue,                  
                },                  
              })         
            } 
          }   
        }
                 
    }catch(error){
      console.log(error)
    } 
          client.search({        
            index: process.env.ELASTIC_INDEX,                            
            body: {
                  "query": {
                  "bool": {
                    "filter": {
                      "bool": {
                        "must": [{
                            "bool": {
                              "should": [
                                {
                                  "match_all": {}
                                },                               
                              ]                          
                              }
                          },
                          filtros
                        ], 
                      }
                    }
                  }
                }
              }              
            })
            .then(response => {                 
                return res.json(response)
            })
            .catch(err => {
                return res.status(500).json({"message": "Error"})
            })
    })

    server.get("/promote",(req, res) => {     
          client.search({        
              index: process.env.ELASTIC_INDEX,
              body: {
                  size: "10",
                  query: {
                      match: {promote: "true"},
                      match: { content_type: 'detalle_del_articulo' }                                   
                  }
              }
          })
          .then(response => {                      
              return res.json(response)
          })
          .catch(err => {
          
              return res.status(500).json({"message": "Error"})
              
          })
    })

    server.get('/api/search/filters', (req, res) => { 
      const searchQuery = req.query            
      var entries = Object.entries(req.query)
      let filtros = {
        "bool": {
            "should": []
        }
      }
      try{
          if(entries[0][1].length > 0){
            if(entries[0][1].includes('*')){
              filtros.bool.should.push({         
                "match_all": {},
              })
            }else{
              for(var h = 0; h < entries[0][1].split(',').length; h++){
                let filterValue = entries[0][1].split(',')[h]
                  if(filterValue !== ''){
                  filtros.bool.should.push({         
                    "match": {
                      "subcategoria": filterValue
                    },
                  })
                }         
              }
            }  
          }

          if(entries[1][1].length > 0){
            if(entries[1][1].includes('*')){
              filtros.bool.should.push({         
                "match_all": {},
              })
            }else{
              for(var m = 0; m < entries[1][1].split(',').length; m++){
                let filterValue = entries[1][1].split(',')[m]
                filtros.bool.should.push({         
                  "match": {
                    "nombre_personaje": filterValue
                  },
                })         
              } 
            }   
          }

          if(entries[2][1].length > 0){
            if(entries[2][1].includes('*')){
              filtros.bool.should.push({         
                "match_all": {},
              })
            }else{
              for(var n = 0; n < entries[2][1].split(',').length; n++){
                let filterValue = entries[2][1].split(',')[n]
                filtros.bool.should.push({         
                  "match": {
                    "name": filterValue
                  },
                })         
              }
            }   
          }
    }catch(error){}       
          client.search({        
              index: process.env.ELASTIC_INDEX,                            
              body: {                 
                  "query": {
                    "bool": {
                      "must": [{
                          "bool": {
                            "should": [
                              {
                                "multi_match": {
                                  "query": req.query.search,
                                  "fields": [
                                    "title^5.0",
                                    "descripcion_corta^3.0",                                    
                                  ],
                                  "type": "best_fields",
                                  "operator": "and",
                                  "fuzziness": "auto"
                                }
                              }
                            
                          ],                          
                        }
                    },                                              
                    filtros
                    ]
                  }
                }               
            }
          })
          .then(response => {                 
              return res.json(response)
          })
          .catch(err => {
              return res.status(500).json({"message": "Error"})
          })
    })  
    
    server.get('/api/intern/filters', (req, res) => {      
            
      let entries = Object.entries(req.query)
      let intern = entries[2][0]
      let internVal = entries[2][1]     
      let searchQuery = {
        [intern]: internVal
      }	     
      let filtros = {
        "bool": {
            "should": []
        }
      }
      try{
          if(entries[0][1].length > 0){
            if(entries[0][1].includes('*')){
              filtros.bool.should.push({         
                "match_all": {},
              })
            }else{
              for(var h = 0; h < entries[0][1].split(',').length; h++){
                let filterValue = entries[0][1].split(',')[h]
                  if(filterValue !== ''){
                  filtros.bool.should.push({         
                    "match": {
                      "subcategoria": filterValue
                    },
                  })
                }         
              }
            }  
          }

          if(entries[1][1].length > 0){
            if(entries[1][1].includes('*')){
              filtros.bool.should.push({         
                "match_all": {},
              })
            }else{
              for(var m = 0; m < entries[1][1].split(',').length; m++){
                let filterValue = entries[1][1].split(',')[m]
                let fieldName = entries[1][0]
                filtros.bool.should.push({         
                  "match": {
                    [fieldName]: filterValue,                    
                  },                  
                })         
              } 
            }   
          }         
      }catch(error){}       
          client.search({        
              index: process.env.ELASTIC_INDEX,                            
              body: {
                    "query": {
                    "bool": {
                      "filter": {
                        "bool": {
                          "must": [{
                              "bool": {
                                "must": [
                                  {
                                    "term": searchQuery
                                  },]
                          
                                }
                            },
                            filtros
                          ], 
                        }
                      }
                    }
                  }
                }              
          })
          .then(response => {                 
              return res.json(response)
          })
          .catch(err => {
              return res.status(500).json({"message": "Error"})
          })
    })
 
    server.use(function (req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Methods", "DELETE, PUT, GET, POST");
      res.header(
        "Access-Control-Allow-Headers",
        "X-CSRF-Token, Origin, X-Requested-With, Content-Type, Accept"
      );
      next();
    });

    server.get("/", cors(), (req, res) => {
     return handle(req, res);
    });

    server.get("*", cors(), (req, res) => {
      if (req.url.includes("/sw")) {
        const filePath = join(__dirname, "static", "workbox", "sw.js");
        app.serveStatic(req, res, filePath);
      } else {
        handle(req, res, req.url);
      }
    });

    server.listen(port, (err) => {
      if (err) throw err;
      console.log(
        `> Ready on http://localhost:${port} > ENV ${process.env.NODE_ENV}`
      );
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });