// /**
//  * Se consulta el servicio de Elastic que contiene el contenido de Cultura
//  *  @returns {Promise}
//  */
// const BASE_DOMAIN = process.env.BASE_DOMAIN_FINAL_CATEGORIAS;
// const CULTURA = JSON.parse(process.env.CULTURA);

// export async function getCultura() {
//   //se busca la url en las variables de entorno
//   const URL_CULTURA = CULTURA.URL_CULTURA;
//   const itemsCultura = await getContent(`${BASE_DOMAIN}${URL_CULTURA}`);

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsCultura) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsCultura,
//     },
//   };
// }

// //URL_ARTE_Y_MUSICA
// //Cultura - Arte y Música
// export async function getArteYMusica() {
//   //se busca la url en las variables de entorno
//   const URL_CULTURA = CULTURA.URL_CULTURA;
//   const URL_ARTE_Y_MUSICA = CULTURA.URL_ARTE_Y_MUSICA;

//   const itemsMusica = await getContent(
//     `${BASE_DOMAIN}${URL_CULTURA}${URL_ARTE_Y_MUSICA}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsMusica) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsMusica,
//     },
//   };
// }

// //Cultura - Peliculas, Series y Libros
// export async function getSeriesYPeliculas() {
//   //se busca la url en las variables de entorno
//   const URL_CULTURA = CULTURA.URL_CULTURA;
//   const URL_PELICULAS_SERIES_LIBROS = CULTURA.URL_PELICULAS_SERIES_LIBROS;

//   const itemsPeliculas = await getContent(
//     `${BASE_DOMAIN}${URL_CULTURA}${URL_PELICULAS_SERIES_LIBROS}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsPeliculas) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsPeliculas,
//     },
//   };
// }

// //Cultura - Viajes y Gastronomía
// export async function getViajesYGastronomia() {
//   //se busca la url en las variables de entorno
//   const URL_CULTURA = CULTURA.URL_CULTURA;
//   const URL_VIAJES_Y_GASTRONOMIA = CULTURA.URL_VIAJES_Y_GASTRONOMIA;

//   const itemsViajes = await getContent(
//     `${BASE_DOMAIN}${URL_CULTURA}${URL_VIAJES_Y_GASTRONOMIA}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsViajes) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsViajes,
//     },
//   };
// }

// //Cultura - Gamers
// export async function getGamers() {
//   //se busca la url en las variables de entorno
//   const URL_CULTURA = CULTURA.URL_CULTURA;
//   const URL_GAMERS = CULTURA.URL_GAMERS;

//   const itemsGamers = await getContent(
//     `${BASE_DOMAIN}${URL_CULTURA}${URL_GAMERS}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsGamers) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsGamers,
//     },
//   };
// }

// //Función genérica encargada de recibir una url como parámetro para la obtención de datos
// async function getContent(path) {
//   const data = await fetch(path)
//     .then((response) => response.json())
//     .catch((err) => {
//       throw new Error(`${err}`);
//     });

//   return data;
// }
