// /**
//  * Se consulta el servicio de Elastic que contiene el contenido de Bienestar
//  *  @returns {Promise}
//  */
// const BASE_DOMAIN = process.env.BASE_DOMAIN_FINAL_CATEGORIAS;
// const BIENESTAR = JSON.parse(process.env.BIENESTAR);

// export async function getBienestar() {
//   //se busca la url en las variables de entorno
//   const URL_BIENESTAR = BIENESTAR.URL_BIENESTAR;

//   const itemsBienestar = await getContent(`${BASE_DOMAIN}${URL_BIENESTAR}`);

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsBienestar) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsBienestar,
//     },
//   };
// }

// //Bienestar - Sexo
// export async function getSex() {
//   //se busca la url en las variables de entorno
//   const URL_BIENESTAR = BIENESTAR.URL_BIENESTAR;
//   const URL_SEXO = BIENESTAR.URL_SEXO;

//   const itemsSexo = await getContent(`${BASE_DOMAIN}${URL_BIENESTAR}${URL_SEXO}`);

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsSexo) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsSexo,
//     },
//   };
// }

// //Bienestar - Habitos
// export async function getHabitos() {
//   //se busca la url en las variables de entorno
//   const URL_BIENESTAR = BIENESTAR.URL_BIENESTAR;
//   const URL_CAMBIO_HABITOS = BIENESTAR.URL_CAMBIO_HABITOS;

//   const itemsHabitos = await getContent(
//     `${BASE_DOMAIN}${URL_BIENESTAR}${URL_CAMBIO_HABITOS}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsHabitos) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsHabitos,
//     },
//   };
// }

// //Bienestar - Salud Mental
// export async function getSaludMental() {
//   //se busca la url en las variables de entorno
//   const URL_BIENESTAR = BIENESTAR.URL_BIENESTAR;
//   const URL_SALUD_MENTAL = BIENESTAR.URL_SALUD_MENTAL;

//   const itemsSalud = await getContent(
//     `${BASE_DOMAIN}${URL_BIENESTAR}${URL_SALUD_MENTAL}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsSalud) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsSalud,
//     },
//   };
// }

// //Función genérica encargada de recibir una url como parámetro para la obtención de datos
// async function getContent(path) {
//   const data = await fetch(path)
//     .then((response) => response.json())
//     .catch((err) => {
//       throw new Error(`${err}`);
//     });

//   return data;
// }
