// /**
//  * Se consulta el servicio de Elastic que contiene el contenido de Entorno
//  *  @returns {Promise}
//  */
// const BASE_DOMAIN = process.env.BASE_DOMAIN_FINAL_CATEGORIAS;
// const ENTORNO = JSON.parse(process.env.ENTORNO);

// export async function getEntorno() {
//   //se busca la url en las variables de entorno
//   const URL_ENTORNO = ENTORNO.URL_ENTORNO;

//   const itemsEntorno = await getContent(`${BASE_DOMAIN}${URL_ENTORNO}`);

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsEntorno) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsEntorno,
//     },
//   };
// }

// //Entorno - Sociedades
// export async function getSociedades() {
//   //se busca la url en las variables de entorno
//   const URL_ENTORNO = ENTORNO.URL_ENTORNO;
//   const URL_SOCIEDADES = ENTORNO.URL_SOCIEDADES;

//   const itemsSociedades = await getContent(
//     `${BASE_DOMAIN}${URL_ENTORNO}${URL_SOCIEDADES}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsSociedades) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsSociedades,
//     },
//   };
// }

// //Entorno - Activismo
// export async function getActivismo() {
//   //se busca la url en las variables de entorno
//   const URL_ENTORNO = ENTORNO.URL_ENTORNO;
//   const URL_ACTIVISMO = ENTORNO.URL_ACTIVISMO;

//   const itemsActivismo = await getContent(
//     `${BASE_DOMAIN}${URL_ENTORNO}${URL_ACTIVISMO}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsActivismo) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsActivismo,
//     },
//   };
// }

// //Entorno - Genero
// export async function getGenero() {
//   //se busca la url en las variables de entorno
//   const URL_ENTORNO = ENTORNO.URL_ENTORNO;
//   const URL_GENERO = ENTORNO.URL_GENERO;

//   const itemsGenero = await getContent(
//     `${BASE_DOMAIN}${URL_ENTORNO}${URL_GENERO}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsGenero) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsGenero,
//     },
//   };
// }

// //Entorno - Medio Ambiente
// export async function getMedioAmbiente() {
//   //se busca la url en las variables de entorno
//   const URL_ENTORNO = ENTORNO.URL_ENTORNO;
//   const URL_MEDIO_AMBIENTE = ENTORNO.URL_MEDIO_AMBIENTE;

//   const itemsMedioAmbiente = await getContent(
//     `${BASE_DOMAIN}${URL_ENTORNO}${URL_MEDIO_AMBIENTE}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsMedioAmbiente) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsMedioAmbiente,
//     },
//   };
// }

// //Función genérica encargada de recibir una url como parámetro para la obtención de datos
// async function getContent(path) {
//   const data = await fetch(path)
//     .then((response) => response.json())
//     .catch((err) => {
//       throw new Error(`${err}`);
//     });

//   return data;
// }
