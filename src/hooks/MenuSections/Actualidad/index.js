// import axios from "axios";
// /**
//  * Se consulta el servicio de Elastic que contiene el contenido de Actualidad
//  *  @returns {Promise}
//  */
// const BASE_DOMAIN = process.env.BASE_DOMAIN_FINAL_CATEGORIAS;  
// const ACTUALIDAD = JSON.parse(process.env.ACTUALIDAD);
 
// export async function getActualidad() {
//   //se busca la url en las variables de entorno
//   const URL_ACTUALIDAD = ACTUALIDAD.URL_ACTUALIDAD;

//   const itemsActualidad = await getContent(`${BASE_DOMAIN}${URL_ACTUALIDAD}`);
//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsActualidad) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsActualidad,
//     },
//   };
// }

// //Actualidad - Política
// export async function getPolitica() {
//   //se busca la url en las variables de entorno
//   // const URL_ACTUALIDAD = ACTUALIDAD.URL_ACTUALIDAD;
//   // const URL_POLITICA = ACTUALIDAD.URL_POLITICA;

//   // const itemsPolitica = await getContent(
//   //   `${BASE_DOMAIN}${URL_ACTUALIDAD}${URL_POLITICA}`
//   // );

//   let itemsPolitica = []
//   let params = new URLSearchParams()
//    params.set("subcategoria", "Política")
//      axios.get('/api/elastic/', {params}).then((res) => {
//        //console.log(res);
//        console.log(res.data.body.hits.hits)  
//        itemsPolitica.push(res.data.body.hits.hits)     
//      });

// // console.log("itemsPolitica")
// // console.log(itemsPolitica)
//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsPolitica) {
//     return {
//       notFound: true,
//     };
//   }

//   return itemsPolitica
// }

// //Actualidad - Educación
// export async function getEducacion() {
//   //se busca la url en las variables de entorno
//   const URL_ACTUALIDAD = ACTUALIDAD.URL_ACTUALIDAD;
//   const URL_EDUCACION = ACTUALIDAD.URL_EDUCACION;

//   const itemsEducacion = await getContent(
//     `${BASE_DOMAIN}${URL_ACTUALIDAD}${URL_EDUCACION}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsEducacion) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsEducacion,
//     },
//   };
// }

// //Actualidad - Economia
// export async function getEconomia() {
//   //se busca la url en las variables de entorno
//   const URL_ACTUALIDAD = ACTUALIDAD.URL_ACTUALIDAD;
//   const URL_ECONOMIA = ACTUALIDAD.URL_ECONOMIA;

//   const itemsEconomia = await getContent(
//     `${BASE_DOMAIN}${URL_ACTUALIDAD}${URL_ECONOMIA}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsEconomia) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsEconomia,
//     },
//   };
// }

// //Actualidad - Tecnología
// export async function getTecnologia() {
//   //se busca la url en las variables de entorno
//   const URL_ACTUALIDAD = ACTUALIDAD.URL_ACTUALIDAD;
//   const URL_TECNOLOGIA = ACTUALIDAD.URL_TECNOLOGIA;

//   const itemsTecnologia = await getContent(
//     `${BASE_DOMAIN}${URL_ACTUALIDAD}${URL_TECNOLOGIA}`
//   );

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!itemsTecnologia) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       itemsTecnologia,
//     },
//   };
// }

// //Función genérica encargada de recibir una url como parámetro para la obtención de datos
// async function getContent(path) {
//   const data = await fetch(path)
//     .then((response) => response.json())
//     .catch((err) => {
//       throw new Error(`${err}`);
//     });

//   return data;
// }
