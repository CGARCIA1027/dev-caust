// export async function useDetalleGaleria() {
//   //se busca url de cms
//   console.log("entraa galeria");
//   const galeriaURL = process.env.DETALLE_GALERIA_URL;

//   const req = await fetch(galeriaURL)
//     .then((response) => response.json())
//     .catch((err) => {
//       throw new Error(`${err}`);
//     });

//   const detalleGaleria = req;

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!detalleGaleria) {
//     return {
//       notFound: true,
//     };
//   }
//   //Se retorna la data obtenida y es pasada como prop mediante la funcion getStaticProps al componente About
//   return {
//     props: {
//       detalleGaleria,
//     },
//   };
// }

//export default useNewsContent;
// export async function getAboutUs() {
//   //se busca la url en las variables de entorno
//   const aboutURL = process.env.URL_ABOUT;
//   const aboutFAQS = process.env.URL_FAQS;

//   const [aboutURLContact, aboutFAQSContact] = await Promise.all([
//     getContent(aboutURL),
//     getContent(aboutFAQS),
//   ]);

//   //En caso de no obtener data, se retorna a la pagina 404
//   if (!aboutURLContact || !aboutFAQSContact) {
//     return {
//       notFound: true,
//     };
//   }

//   //Se retorna la data obtenida y es pasada como prop mediante la funcion getStaticProps al componente HomePage
//   return {
//     props: {
//       aboutURLContact,
//       aboutFAQSContact,
//     },
//   };
// }

//Función genérica encargada de recibir una url como parámetro para la obtención de datos
async function getContent(path) {
  const data = await fetch(path)
    .then((response) => response.json())
    .catch((err) => {
      throw new Error(`${err}`);
    });

  return data;
}