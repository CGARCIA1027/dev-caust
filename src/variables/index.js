export const defaultValues = {
  CATEGORIAS_MENU: {
    ACTUALIDAD: {
      nombre_categoria: "Actualidad",
      backgroundColor: "#E62E2E",
      field_imagen_sobrepuesta: "/static/assets/main-sections-actualidad.png",
      field_imagen_de_fondo: "/static/assets/back-sections-actualidad.jpg",
      field_imagen_de_fondo_mobile: "/static/assets/back-sections-actualidad-m.jpg",
    },
    BIENESTAR: {
      nombre_categoria: "Bienestar",
      backgroundColor: "#986B58",
      field_imagen_sobrepuesta: "/static/assets/main-sections-bienestar.png",
      field_imagen_de_fondo: "/static/assets/back-sections-bienestar.jpg",
      field_imagen_de_fondo_mobile: "/static/assets/back-sections-bienestar-m.jpg",
    },
    ENTORNO: {
      nombre_categoria: "Entorno",
      backgroundColor: "#F2CA50",
      field_imagen_sobrepuesta: "/static/assets/main-sections-entorno.png",
      field_imagen_de_fondo: "/static/assets/back-sections-entorno.jpg",
      field_imagen_de_fondo_mobile: "/static/assets/back-sections-entorno-m.jpg",
    },
    CULTURA: {
      nombre_categoria: "Cultura",
      backgroundColor: "#21C0A6",
      field_imagen_sobrepuesta: "/static/assets/main-sections-cultura.png",
      field_imagen_de_fondo: "/static/assets/back-sections-cultura.jpg",
      field_imagen_de_fondo_mobile: "/static/assets/back-sections-cultura-m.jpg",
    },
  },
};
