import Head from "next/head";
import Link from "next/link";
import BurguerMenu from "../components/BurguerMenu";
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { useEffect, useState, useRef  } from "react";
import axios from 'axios';
import { useRouter } from 'next/router' 
import Rodal from 'rodal';
import 'rodal/lib/rodal.css';


const LoginPage = () => {
  const router = useRouter()
  const [user, setUser] = useState(null)
  const [password, setPassword] = useState(null)
  const [isLoggedIn, setIsLoggedIn] = useState(null)
  const [nameWarning, setNameWarning] = useState(false)
  const [passwordWarning, setPasswordWarning] = useState(false)
  const [visible, setVisible] = useState(false)
  let userInput =  useRef(null);
  let passwordInput =  useRef(null);

 /////////////////////////////---------emptor verify-------////////////////////////////////////////
 function loginCheck () {    
  var authType = JSON.parse(window.localStorage.getItem("authType"));
  let EMPTOR = JSON.parse(process.env.EMPTOR) 
  let url, params, resp, register;    
  
  switch (authType) {    
    case "Google":         
      register = false;
      url = EMPTOR.BASE_DOMAIN + EMPTOR.LOGIN.GOOGLE + process.env.CLIENT_ID             
      resp = JSON.parse(window.localStorage.getItem("user"));      
        params = new URLSearchParams()
        params.set('code', resp.accessToken)
        params.set('idprofile', resp.googleId)
        params.set('username', resp.googleId)
        params.set('firstname', resp.profileObj.givenName)
        params.set('lastname', resp.profileObj.familyName)
        params.set('email', resp.profileObj.email)
        params.set('socialnetwork', 'google')
        params.set('client_id', process.env.CLIENT_ID)        
        params.set('codeExpiresAt', resp.tokenObj.expires_at)        
       
         socialLogin(params, url, authType, register); 
    break;
    
    case "Facebook":   
      resp = JSON.parse(window.localStorage.getItem("user"));
      register = false;
      url = EMPTOR.BASE_DOMAIN + EMPTOR.LOGIN.FACEBOOK.replace('{CLIENT_ID}', process.env.CLIENT_ID)        
      if (resp.accessToken == undefined) {     
        console.log('Error leyendo el token de seguridad')
      } else {
          params = new URLSearchParams()
          params.set('code', resp.accessToken)
          params.set('idprofile', resp.id)
          params.set('username', resp.userID)
          params.set('firstname', resp.name)
          params.set('lastname', resp.name)
          params.set('email', resp.email)
          params.set('socialnetwork', 'facebook')
          params.set('client_id', process.env.CLIENT_ID)
          params.set('codeExpiresAt', new Date().getTime() + resp.data_access_expiration_time)
          
          socialLogin(params, url, authType, register)        
      }                
    break; 

    case 'Classic':
      const checkLoginData = () => {               
          if( password == 'null' || password == '' || password == undefined || password== null){ setPasswordWarning(true) }else{ setPasswordWarning(false) }
          if( user == 'null' || user == '' || user == undefined || user== null){ setNameWarning(true) }else{ setNameWarning(false) }
      };

      checkLoginData();     
            
      register = false; 

      if ( user == undefined || password == undefined || user == null || password == null || user == '' || password == '' ) {     
        console.log('usuario o contraseña invalida')
      } else {
          params = new URLSearchParams()            
          params.set('email', user)
          params.set('password', password)    
          
          loginWithParams(params, register = false, resp)          
      }      
      break;      
    default: return null
      
  }
  
 }
function socialLogin(params, url, source='tradicional') {
  let EMPTOR = JSON.parse(process.env.EMPTOR);
  const now = new Date() 
  let expiry = now.getTime() + 10000000;
  const expiration = {
    value:  'classic',
    expiry: expiry
  }       
      axios.post(url, params,{        
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          'Authorization': EMPTOR.BASIC_TOKEN
        }
      }).then(response => {                                   
          if(response.data.data.message=='session was created'){               
            setIsLoggedIn(true);           
            localStorage.setItem('exp', JSON.stringify(expiration))
            emptorMe()                             
          }else{           
            console.log('no se pudo iniciar sesión')               
          }                      
          
      }).catch(err => {
          console.log('err social login ----', err) 
                         
      })            
      
  }
  function loginWithParams(params, register = false) {
        let EMPTOR = JSON.parse(process.env.EMPTOR);
        let basic = EMPTOR.LOGIN.BASIC.replace('{CLIENT_ID}', process.env.CLIENT_ID)
        let url = EMPTOR.BASE_DOMAIN + basic;         
        const now = new Date() 
        let expiry = now.getTime() + 10000000;
        const expiration = {
          value:  'classic',
          expiry: expiry
        }       
        axios.post(url, params, {
              headers: {
                "content-type": "application/x-www-form-urlencoded",
                Authorization: EMPTOR.BASIC_TOKEN,
              },
            })
            .then((response) => {
              if(response.data.data.accessToken){                
                setIsLoggedIn(true);
                localStorage.setItem('exp', JSON.stringify(expiration))                
                localStorage.setItem('user', JSON.stringify(response.data.data))
                window.localStorage.setItem("authType", JSON.stringify('Classic'));                         
                window.localStorage.setItem('IsLogged', true)                           
                emptorMe()
              }else{
                console.log('no se genero token de acceso')
              }             
            })
            .catch((err) => {
              console.log(err)
              setVisible(true)          
            });
        
      }
      function emptorMe() {        
        let EMPTOR = JSON.parse(process.env.EMPTOR);
        let url = EMPTOR.BASE_DOMAIN + EMPTOR.ME.ME;       
        let bearer = JSON.parse(window.localStorage.getItem('user')).accessToken;
        
        axios.post(
              url, 
              {},
              {
                headers: {
                  Authorization: "Bearer " + bearer,
                },
              }
            ).then((responseEmptor) => {               
              window.localStorage.setItem('userEmptor',JSON.stringify(responseEmptor.data.data))
              window.location.assign('/')     
              
            
            }).catch((error) => {
              console.log("error---->", error);              
            });
      
      }
    
   
//////////////////////////////////////////////////////////////////////////////////////////////////
  
  const handleChange = async (e) =>{    
     setUser(userInput.current.value);
     setPassword(passwordInput.current.value);
     window.localStorage.setItem("authType", JSON.stringify('Classic'));   
     window.localStorage.setItem("email", JSON.stringify(user));  
  }
  const handleKeypress = e => {       
    if (e.charCode === 13 ) { 
      e.preventDefault();    
      loginCheck()
    }
  } 
 
  const responseGoogle = (response) => {   
    try{
      if(response.accessToken){       
        const user=response;            
        window.localStorage.setItem("user", JSON.stringify(user));
        window.localStorage.setItem("authType", JSON.stringify('Google'));   
        loginCheck();  
          
      }else{
        // alert('El inicio de sesion falló')
        setUser(null);
      }
    }catch(error){
      console.log(error)
    }
   
  }
  const responseFacebook = (response) => { 
    try{
      if(!window.FB) return;
      window.FB.getLoginStatus(responseStatus =>{        
          if(responseStatus.status==='connected'){                
              const user=response;            
              window.localStorage.setItem("user", JSON.stringify(user));          
              window.localStorage.setItem("authType", JSON.stringify('Facebook'));
              loginCheck();            
              
          }else{       
              // alert('El inicio de sesion falló')
              setUser(null);     
          }        
      }) 
    }catch(error){
      console.log(error)
    }   
     
  }
  
  
  useEffect(() => {
    
    const checkSessions = () => {               
      if(user !== 'null' || user !== undefined){        
        setUser(user);
        if(isLoggedIn==true){
          router.push('/')
        }
        
      }
    };
    checkSessions();
  }, []);  

  const handleClick = (e) =>{         
    if(e.currentTarget.className=='eye'){
      if(e.currentTarget.previousSibling.getAttribute('type')== 'text'){
        e.currentTarget.previousSibling.setAttribute('type','password');
        document.querySelector('.capsule-pass').classList.remove('open');
      }else {
        e.currentTarget.previousSibling.setAttribute('type','text');
        document.querySelector('.capsule-pass').classList.add('open');
      }
    }else{
      setVisible(false)
    }      
}
const modalStyle = {
  boxShadow: "0 1px 7px #000000a1, #e62e2e -14px 14px"
};
  return (
    <>
      <Head>
        <title>Website Cáustica</title>
        <meta name="viewport" content="width=device-width" />

        <meta name="robots" content="No index"/>

        <meta name="distribution" content="global" /> 
        <meta name="rating" content="general"/>
        <meta name="description" content=""/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name="author" content="Caustica"/>
        <meta name="genre" content="News"/>
        <meta name="geo.placename" content="Colombia"/>
        <meta name="geo.region" content="CO"/>
        <meta name="language" content="spanish"/>

        <link rel="canonical" href="https://www.caustica.co/iniciar-sesion/"/>

        <meta property="og:type" content="website" />
        <meta property="og:title" content="{varTitulo}" />
        <meta property="og:description" content="{varDescripcion}" />
        <meta property="og:url" content="https://www.caustica.co/iniciar-sesion/" />
        <meta property="og:image" content="../../static/assets/1200x630-facebook.png" />
        <meta property="og:site_name" content="Caustica" />
        <meta property="fb:admins" content="723352678331419" />
        <meta property="og:locale" content="es_CO" />
        <meta property="og:locale:alternate" content="es_CO" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@CausticaCo" />
        <meta name="twitter:creator" content="@CausticaCo" />
        <meta name="twitter:title" content="{varTitulo}" />
        <meta name="twitter:description" content="{CausticaCo}" />
        <meta name="twitter:image" content="../../static/assets/280x150-twitter.png" />

      </Head>      
      
      <main className="loginMain">
        <div className="login-background">
            <div className="background-drops"/>            
            <div className="background-banner"/>
            <div className="login-info">
                <div className="login-info-content">
                        <div className="login-info-title">INICIAR SESIÓN</div>
                        <div className="login-info-icons">                          
                            <FacebookLogin
                                appId="181912180007128"
                                autoLoad={false}
                                fields="name, last_name, first_name, email"
                                callback={responseFacebook}
                                render={renderProps => (
                                  <button className="login-icon-facebook" onClick={renderProps.onClick}/>
                                )}                             
                            />                                                
                            <GoogleLogin
                                clientId="398456058126-fnua66kc86e986jbdh2il4r9eabqcs70.apps.googleusercontent.com"
                                render={renderProps => (
                                  <button className="login-icon-google" onClick={renderProps.onClick} disabled={renderProps.disabled}/>
                                )}
                                buttonText=""
                                onSuccess={responseGoogle}
                                onFailure={responseGoogle}
                                cookiePolicy={'single_host_origin'}
                            />                              
                        </div>
                        <form className="user-inputs" onKeyPress={handleKeypress}>
                            <div className="mail-title">Correo Electronico</div>
                            <div className="capsule" dataError={nameWarning ?  "active" : ""}>
                              <input 
                                  ref={userInput}
                                  type="text" 
                                  className="input-mail" 
                                  name="user"
                                  onChange={handleChange}
                              />
                              <p className="loggin-name-warning" hidden={nameWarning ?  false : true}>Ingresa tu correo electrónico</p>
                            </div>
                                    <br/>
                            <div className="password-title">Contraseña</div>    
                                <div className="capsule-pass" dataError={passwordWarning ?  "active" : ""}>
                                    <input
                                        ref={passwordInput} 
                                        type="password" 
                                        className="input-password" 
                                        name="password"
                                        onChange={handleChange} 
                                    />
                                    <span className="eye" onClick={handleClick}></span>
                                     <p className="loggin-password-warning" hidden={passwordWarning ?  false : true}>Ingresa tu contraseña</p>
                                </div>
                                    <br/>
                            <div className="under-password">    
                                <p>* Mínimo 6 caracteres, 1 minúscula, 1 mayúscula y un número</p>
                                <Link href="/olvide-mi-contrasena">                                
                                  <a href="/olvide-mi-contrasena">
                                    <u>¿Olvidaste tu contraseña?</u>
                                  </a>
                                </Link>
                            </div>
                            <div className="submit-button-login" 
                                  onClick={loginCheck}    >Continuar</div>                    
                            
                        </form>
                        
                        <div className="final-question-container">
                        <div className="final-question">
                            <p>¿Aún no tienes una cuenta?</p>
                            <Link href="/registro">
                              <a href="/registro"><u>Regístrate aquí</u></a>
                            </Link>
                        </div>    
                        </div>
                </div>
            </div>
                
            

        </div>       
      </main>
      <Rodal className="error" visible={visible} onClose={handleClick} customStyles={modalStyle}>
          <div className="modal-update-img"></div>
          <div className="modal-update-info">Correo o contraseña invalidos</div>
      </Rodal>
     
    </>
  );
};

  
export default LoginPage;
