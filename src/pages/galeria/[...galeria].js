import Head from "next/head";
import GaleryCopyAdd from "../../components/GaleryCopyAdd";
import SliderGaleria from "../../components/SliderGaleria"
import BurguerMenu from "../../components/BurguerMenu";
import React, {useState,useEffect, useRef } from "react";
import HomeItemGrid from '../../components/HomeItemGrid'
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import {useRouter} from "next/router"
import axios from "axios";

const DetalleGaleria = (detalleGaleria) => {

  const [itemCategoria, setItemCategoria] = useState([]);
  const [DataDinamica, setDataDinamica] = useState([]);
  const router = useRouter()
  const galeria = router.query.galeria

  useEffect( async() => {
    if (!router.isReady) return;

        var hayScrollLoad = document.querySelector("body")
        if (hayScrollLoad.classList.contains("scroll")) {
           try{
            document.querySelector("body").classList.remove("scroll");
           }catch(error){
             //console.log(error)
           }
         }
    
    const paramId = galeria[1].toString()
    let params = new URLSearchParams();
    params.set("nid", paramId);
    const newDetail = 
    
    await axios
      .get("/api/elastic", { params })
      .then((res) => {
        setDataDinamica(res.data.body.hits.hits)

        let subCategoria = res.data.body.hits.hits[0]._source.id_subcategoria
        const dataCategoria = encodeURI(process.env.URL_DOMAIN_ELTIEMPO+"q=id_subcategoria:"+subCategoria)
        fetch(dataCategoria)
            .then(res => res.json())
            .then(
              (result) => {
                setItemCategoria(result.hits.hits);
              },       
              (error) => {
                // console.log(error)
              }
            )
      })

      .catch((err) => {
        // console.error(err);
      });

  }, [router.isReady])

  return (
    <>
      <Head>
        <title>Cáustica - Galeria</title>
        <meta name="description" content="Lorem ipsum dolor sit amet" />
        <meta name="viewport" content="width=device-width" />
      </Head>
    

      {DataDinamica.length > 0 ? <SliderGaleria detalleGaleriaArticulo={DataDinamica} /> : null}
      {DataDinamica.length > 0 ? <GaleryCopyAdd detalleVideoArticulo={DataDinamica} /> : null}

      <div className="puede-interesar galeria">
        <h2>Te puede interesar</h2>
      <ResponsiveMasonry
        columnsCountBreakPoints={{ 576: 1, 768: 2, 1200: 3 }}
      >
        <Masonry className="masonry-content-dinamicas" style={{ padding: "90px" }}>
          {itemCategoria.length > 0 ? (
            itemCategoria.map((item) => {
              return <HomeItemGrid key={item._id} {...item} />;
            })
          ) : (
            <p>No se encontraron resultados para tu búsqueda</p>
          )}
        </Masonry>
      </ResponsiveMasonry>
      </div>

    </>
  );
};

export default DetalleGaleria;