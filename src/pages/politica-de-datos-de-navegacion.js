import Head from "next/head";
import BurguerMenu from "../components/BurguerMenu";
import React, {useState,useEffect, useRef } from "react";
import axios from "axios";
import FloatinButton from "../components/FloatinButton";

const politicaPrivacidad = () => {

  let params = new URLSearchParams()
  const [politicaNavegacion, setPoliticaNavegacion] = useState([]);

  useEffect(() => {
    params.set("nid", "35")
    axios.get('/api/elastic/', {params}).then((res) => {
      try{
        setPoliticaNavegacion(res.data.body.hits.hits)   
        }catch(error){
          //console.log(error)
        }
    });
  }, [])   

let bodyPolitica = ""
let titlePolitica = ""

politicaNavegacion.map((item, i) => { 
      bodyPolitica = item._source.body[0]
      titlePolitica = item._source.title[0]
    })

  return (
    <>
      <Head>
        <title>Caustica - {titlePolitica}</title>
        <meta name="viewport" content="width=device-width" />

        <meta name="robots" content="index,follow"/>

        <meta name="distribution" content="global" /> 
        <meta name="rating" content="general"/>
        <meta name="description" content=""/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name="author" content="Caustica"/>
        <meta name="genre" content="News"/>
        <meta name="geo.placename" content="Colombia"/>
        <meta name="geo.region" content="CO"/>
        <meta name="language" content="spanish"/>

        <link rel="canonical" href="https://www.caustica.co/politica-de-datos-de-navegacion/"/>

        <meta property="og:type" content="website" />
        <meta property="og:title" content="{varTitulo}" />
        <meta property="og:description" content="{varDescripcion}" />
        <meta property="og:url" content="https://www.caustica.co/politica-de-datos-de-navegacion/" />
        <meta property="og:image" content="../../static/assets/1200x630-facebook.png" />
        <meta property="og:site_name" content="Caustica" />
        <meta property="fb:admins" content="723352678331419" />
        <meta property="og:locale" content="es_CO" />
        <meta property="og:locale:alternate" content="es_CO" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@CausticaCo" />
        <meta name="twitter:creator" content="@CausticaCo" />
        <meta name="twitter:title" content="{varTitulo}" />
        <meta name="twitter:description" content="{CausticaCo}" />
        <meta name="twitter:image" content="../../static/assets/280x150-twitter.png" />
      </Head>
     

      <div className="content politica-privacidad">
        <h1>{titlePolitica}</h1>  
          <div className='info' dangerouslySetInnerHTML={{
                    __html: bodyPolitica,
                  }} />
        
      </div>
      <FloatinButton/>
    </>
  );
};

export default politicaPrivacidad;