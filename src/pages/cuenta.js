
import Head from "next/head";
import Link from "next/link";
import axios from 'axios'
import BurguerMenu from "../components/BurguerMenu";
import { useEffect, useState, useRef  } from "react";
import Rodal from 'rodal';
import 'rodal/lib/rodal.css';

var window = require("global/window")

const AccountPage = () => {
    const [user, setUser] = useState(null)
    const [firstName, setFirstName] = useState(undefined)
    const [lastName, setLastName] = useState(undefined)
    const [identificationCard, setIdentificationCard] = useState(undefined)
    const [numberCard, setNumberCard] = useState(undefined)
    const [email, setEmail] = useState(undefined)
    const [emailAlert, setEmailAlert] = useState(false)
    const [password, setPassword] = useState(null)
    const [newPassword, setNewPassword] = useState(null)
    const [showPasswords, setShowPasswords] = useState(false)
    const [authType, setAuthType] = useState(undefined)
    const [labelShow, setLabelShow] = useState(false)    
    const [visible, setVisible] = useState(false)
    const [visible2, setVisible2] = useState(false)
    const [visible3, setVisible3] = useState(false)
    const [visible4, setVisible4] = useState(false)
    const [newPassWarning, setNewPassWarning] = useState(false)
    const [currentPassWarning, setCurrentPassWarning] = useState(false)
    const [dataChangeModal, setDataChangeModal] = useState(false)    
   
    
    let userName =  useRef(null);
    let userLastName =  useRef(null);
    let userEmail = useRef(null);
    let userNumberCard = useRef(null);    
    let userPassword = useRef(null);
    let userNewPass = useRef(null);    

    const handleChange = async (e) =>{             
        setFirstName(userName.current.value);
        setLastName(userLastName.current.value);
        setEmail(userEmail.current.value);
        setNumberCard(userNumberCard.current.value);
        setPassword(userPassword.current.value);
        setNewPassword(userNewPass.current.value)                                               
    }

    const handleKeypress = e => {             
      if (e.charCode === 13 ) {
        e.preventDefault()        
      }
    } 

    const handleClick = (e) =>{               
        if(e.currentTarget.innerHTML=='Cambiar Contraseña'){
            if(showPasswords == true) setShowPasswords(false)
            else setShowPasswords(true)
        }
        else if(e.currentTarget.className=='submit-button-update'){
            setCurrentPassWarning(false)
            setNewPassWarning(false)
            setEmailAlert(false)         
            userDataChange()
        }
        else if(e.currentTarget.className=='eye'){
            if(e.currentTarget.previousSibling.getAttribute('type')== 'text'){
              e.currentTarget.previousSibling.setAttribute('type','password');
              if(e.currentTarget.parentNode.classList.contains('actual')){
                document.querySelector('.actual').classList.remove('open');
              }
              if(e.currentTarget.parentNode.classList.contains("new")){
                document.querySelector('.new').classList.remove('open');
              }
            }else {
              e.currentTarget.previousSibling.setAttribute('type','text');
              if(e.currentTarget.parentNode.classList.contains('actual')){
                document.querySelector('.actual').classList.add('open');
              }
              if(e.currentTarget.parentNode.classList.contains("new")){
                document.querySelector('.new').classList.add('open');
              }
            }
        }else if(e.currentTarget.className == "rodal-mask" || e.currentTarget.className == "rodal-close"){
            setVisible3(false)            
            setVisible2(false)
            setVisible4(false)
            setDataChangeModal(false)
            if( visible===true ){
                setVisible(false)                
            }

        }   
    }
    //Funcion que detecta cambios en los datos de la cuenta de usuario y lanza las respectivas funciones de actualización
    function userDataChange(){                
        let userDataOld= { first_name: "", last_name: "", correo: "", id_type: "", id_number: "", pass: ""};
        let catchUser = JSON.parse(window.localStorage.getItem('userEmptor'));       
       
            userDataOld['first_name'] = catchUser.first_name
            userDataOld['last_name'] =  catchUser.last_name
            userDataOld['correo'] = catchUser.email
            userDataOld['pass'] = password

            let documentID = document.getElementsByClassName('select-type-id')[0].value.toString()
              	userDataOld['id_type'] = documentID

            const regex = /^[0-9]*$/;
            let typeID = document.getElementsByClassName('input-id-number-account')[0].value.toString()
            
            if(catchUser.document === undefined){
             
            }else {
              if(catchUser.document.length < 20 && regex.test(catchUser.document) == true){
                userDataOld['id_number'] = catchUser.document
              }else{
                userDataOld['id_number'] = ""            
              } 
            }
            
            if(userDataOld['first_name'] == firstName && 
               userDataOld['last_name'] == lastName &&
               userDataOld['correo'] == email &&
               userDataOld['id_number'] == numberCard &&
               userDataOld['id_type'] == identificationCard &&
               password == null &&
               newPassword == null
            ){
              setDataChangeModal(true)
            }else{                                        
              if(userDataOld['first_name'] !== firstName || userDataOld['last_name'] !== lastName || userDataOld['id_number'] !== typeID || userDataOld['id_type'] !== documentID){ 
                updateUserInformation() 
              }              
              if(userDataOld['correo'] !== email){
                const regularMail = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;               
                if( regularMail.test(email) == true ){
                  changeEmail()
                }else{
                  setEmailAlert(true)
                }                
              }
             
              if(newPassword !==null && newPassword !== '' && password !==null && password !== ''){
                //funcion para validar que la nueva contraseña cumple con los requisitos
                function validatePassword (newPassword){                         
                  if(newPassword.length >= 6){
                      var mayuscula = false;
                      var minuscula = false;
                      var numero = false;           
                      
                      for(var i = 0; i < newPassword.length; i++){
                          if( newPassword.charCodeAt(i) >= 65 && newPassword.charCodeAt(i) <= 90){ mayuscula = true; }
                          else if( newPassword.charCodeAt(i) >= 97 && newPassword.charCodeAt(i) <= 122){ minuscula = true; }
                          else if(newPassword.charCodeAt(i) >= 48 && newPassword.charCodeAt(i) <= 57){ numero = true; }                
                      }
                      if(mayuscula == true && minuscula == true && numero == true){                          
                          changePassword()
                      }else{
                          setNewPassWarning(true)
                      }
                  }else{
                    setNewPassWarning(true)
                  }                     
                }

                //llamado de validatePassword
                validatePassword(newPassword)
                
              }else if(password.length < 6 && password.length > 0){
                setCurrentPassWarning(true)
              }
            }       
        }
       
     // funcion para actualizar los datos del usuario, contiene 3 subfunciones asincronas
    function updateUserInformation() {        
        let EMPTOR = JSON.parse(process.env.EMPTOR)       
        let url = EMPTOR.BASE_DOMAIN + EMPTOR.USER_ZONE.UPDATE_USER
        url = url.replace('{CLIENT_ID}', process.env.CLIENT_ID)

    //1. funcion para actualizar Nombres, Apellidos, Tipo y Numero de Documento 
       async function asyncCall()  {
            let documentID = document.getElementsByClassName('select-type-id')[0].value 
            let params = new URLSearchParams()
                params.set('first_name', firstName)
                params.set('last_name' , lastName)              
                params.set('document_type_id', documentID)
                params.set('document', numberCard)
            let userInfoUpdated = {
              user:{
                first_name: params.get('first_name'),
                last_name: params.get('last_name'),
                document_type_id: params.get('document_type_id'),
                document: params.get('document'),
              }
            }    
               
            let accessNumber = JSON.parse(window.localStorage.getItem('user')).accessToken
            await axios.post(url, params, {
                headers: {
                  'Authorization': 'Bearer ' + accessNumber,
                  'Content-Type': 'application/x-www-form-urlencoded'
                }
                
              }).then(res => {                             
                if(res.data.data.message.includes('User Data update')){
                  setVisible(true)
                  let catchUser = JSON.parse(window.localStorage.getItem('userEmptor'));                                     
                      catchUser.first_name = userInfoUpdated["user"]["first_name"]
                      catchUser.last_name = userInfoUpdated["user"]["last_name"]
                      catchUser.document_type_id = userInfoUpdated["user"]["document_type_id"]
                      catchUser.document = userInfoUpdated["user"]["document"] 
                  window.localStorage.setItem('userEmptor', JSON.stringify(catchUser))                       
                }
                      
              }).catch(err => {
                setVisible4(true)
                // console.log(err)
              })      
        }       
        asyncCall()
      }

      // 2.  funcion para actualizar el correo electrónico
      async function changeEmail() {
        
          let EMPTOR = JSON.parse(process.env.EMPTOR)      
          let url = EMPTOR.BASE_DOMAIN + EMPTOR.USER_ZONE.CHANGE_EMAIL
              url = url.replace('{CLIENT_ID}', process.env.CLIENT_ID)          
          let formData = new FormData()
          formData.append("email", email)
          formData.append("email_repeat", email)      
          let accessToken = JSON.parse(window.localStorage.getItem('user')).accessToken
          
            await axios.post(url, new URLSearchParams(formData), {
              method: 'POST',
              headers: {
                'Authorization': 'Bearer ' + accessToken,
                'Content-Type': 'application/x-www-form-urlencoded'
              }        
            }).then(res => {             
              setVisible3(true)           
            }).catch(err => {            
              setEmailAlert(true)
              console.log(err)
            }) 
      }
      
      //3. funcion para actualizar la contraseña
      async function changePassword() {
        let EMPTOR = JSON.parse(process.env.EMPTOR)       
        let url = EMPTOR.BASE_DOMAIN + EMPTOR.USER_ZONE.CHANGE_PASSWORD
        url = url.replace('{CLIENT_ID}', process.env.CLIENT_ID)        
        // let data = {
        //   old_password: password,
        //   new_password: newPassword,
        //   confirm_password: newPassword
        // }
        let accessToken = JSON.parse(window.localStorage.getItem('user')).accessToken
        var formData = new FormData()
        
        formData.append("old_password", password)
        formData.append("new_password", newPassword)
        formData.append("confirm_password", newPassword)
        
          await axios.post(url, new URLSearchParams(formData), {
            method: 'POST',
            headers: {
              'Authorization': 'Bearer ' + accessToken,
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(res => {          
            if(res.data.data.message.includes("el cambio de contraseña ha sido exitoso")){
              setVisible(true)              
            }
            
          }).catch(err => {            
            console.log(err)
            setVisible2(true)
          })
         
       
      }
    // funcion para traer los datos del usuario a la cuenta desde el localStorage
    function getUser() {
        try{                 
            let catchUser = JSON.parse(window.localStorage.getItem('userEmptor'));           
            let catchAuthType = JSON.parse(window.localStorage.getItem('authType')); 
            if(catchUser==null || catchUser==undefined){
                window.location.assign('/iniciar-sesion')
            }         
            setUser(catchUser)
            setAuthType(catchAuthType)           
            setFirstName(catchUser.first_name)
            setLastName(catchUser.last_name)
            document.getElementsByClassName('select-type-id')[0].value = catchUser.document_type_id     
            setIdentificationCard(catchUser.document_type_id)            
            setEmail(catchUser.email)
            const regex = /^[0-9]*$/;
            if(catchUser.document.length < 20 && regex.test(catchUser.document) == true){
              setNumberCard(catchUser.document)
            }else{
              setNumberCard("")
            }           
            if(catchAuthType == "Facebook" || catchAuthType == "Google"){              
              setShowPasswords(false)
              setLabelShow(false)
              document.getElementsByClassName('account-background')[0].classList.add('socialContent')
            }else{
              setShowPasswords(true)
              setLabelShow(true)              
            }

        }catch(error){console.log(error)}
                          
    }

    useEffect(() => {
        if (user===null){
            getUser()                                   
        }
    }, [])
    
    const modalStyle = {
      boxShadow: "0 1px 7px #000000a1, #e62e2e -14px 14px"
    };
    
  return (
    <>
      <Head>
        <title>Website Cáustica</title>
        <meta name="viewport" content="width=device-width" />
        <meta name="robots" content="No index"/>  
      </Head>      
      
      <main className="accountMain">
        <div className="account-background">
            <div className="background-drops"/>            
            <div className="background-banner"/>
            <div className="account-info">
                <div className="account-info-content">
                    <div className="account-info-title">CONFIGURAR MIS DATOS</div>
                    <div className="account-info-data">
                        <form onKeyPress={handleKeypress} id='form-account-user'>
                                <div className="Name">
                                        <label>Nombres:&nbsp;&nbsp;</label>
                                        <div className="box-input">
                                          <input
                                              className="input-name-account"
                                              defaultValue={firstName}
                                              onChange={handleChange}
                                              ref={userName}                                                                                       
                                          />
                                        </div>
                                        <p className="user-name-warning" hidden>Ingresa tu nombre</p>
                                </div>
                                <div className="LastName">
                                        <label>Apellidos:&nbsp;&nbsp;</label>
                                        <div className="box-input">
                                          <input
                                              className="input-last-names-account"
                                              defaultValue={lastName}
                                              onChange={handleChange}
                                              ref={userLastName}                                            
                                          />
                                        </div>
                                        <p className="user-lastname-warning" hidden>Ingresa tu apellido</p>
                                </div>
                                <div className="Email">
                                        <label>Correo Electrónico:&nbsp;&nbsp;</label>
                                        <div className="box-input">
                                          <input
                                              className="input-email-account"
                                              defaultValue={email}
                                              onChange={handleChange}
                                              ref={userEmail} 
                                          />
                                          <p className="email-registered-alert"hidden={emailAlert ?  false : true}>Este Correo ya se encuentra registrado o no es un correo válido</p>
                                        </div>
                                        <p className="user-email-warning" hidden>Ingresa tu email</p>
                                </div>
                                                               
                                        <div className="TipeDocument">
                                            <label>Tipo de Documento:&nbsp;&nbsp;</label>
                                            <div className="box-input">
                                              <select
                                                className="select-type-id"
                                                name="document_type_id"
                                                defaultValue={identificationCard}
                                                // onChange={this.activateSubmitButton}
                                              >
                                                  <option disabled>Seleccione</option>
                                                  <option value="1">Cédula de Ciudadanía</option>
                                                  <option value="3">Cédula de Extranjería</option>
                                                  <option value="4">N.I.T.</option>
                                                  <option value="5">Pasaporte</option>
                                                  <option value="10">Tarjeta de Identidad</option>
                                              </select>
                                            </div>
                                            <p className="user-tipedoc-warning" hidden>Selecciona tipo de documento</p>
                                        </div>
                               
                                <div className="Document">
                                        <label>Número de Documento:&nbsp;&nbsp;</label>
                                        <div className="box-input">
                                          <input
                                              className="input-id-number-account"
                                              defaultValue={numberCard}
                                              onChange={handleChange}
                                              ref={userNumberCard}  
                                          />
                                        </div>
                                        <p className="user-doc-warning" hidden>Ingresa tu documento</p>
                                </div>
                                <label className="btnChangePass" onClick={handleClick} hidden={labelShow ?  true : false}>Cambiar Contraseña</label>
                                <div className='change-password' hidden={showPasswords ?  false : true}>
                                        <div className="Pass">
                                              <label>Contraseña Actual:&nbsp;&nbsp;</label>
                                              <div className="box-input actual">
                                                <input
                                                    className="input-password-account"
                                                    defaultValue={""}
                                                    onChange={handleChange}
                                                    ref={userPassword}
                                                    type="password" 
                                                />
                                                <span className="eye" onClick={handleClick}></span>
                                              </div>
                                      </div>
                                      <div className="Pass">
                                          <label>Nueva Contraseña:&nbsp;&nbsp;</label>
                                          <div className="box-input new">
                                            <input
                                                className="input-pass-conf-account"
                                                defaultValue={""}
                                                onChange={handleChange}
                                                ref={userNewPass} 
                                                type="password" 
                                            />
                                            <span className="eye" onClick={handleClick}></span>
                                          </div>
                                          
                                        </div>
                                        <p className="user-current-pass-warning" hidden={currentPassWarning ?  false : true}>La contraseña actual no es valida</p>
                                        <p className="user-new-pass-warning" hidden={newPassWarning ?  false : true}>La nueva contraseña no es valida</p>
                                        <p className="legalPass">* Mínimo 6 caracteres, 1 minúscula, 1 mayúscula y 1 número</p>
                                </div>
                               
                               
                                 <div                                    
                                    className="submit-button-update"
                                    onClick={handleClick} >Guardar</div>                            
                                
                        </form>           

                    </div>
                </div>
            </div>
                
            

        </div>       
      </main>
      <Rodal className="ok" visible={visible} onClose={handleClick} customStyles={modalStyle}>
          <div className="modal-update-img"></div>
          <div className="modal-update-info">DATOS ACTUALIZADOS CON EXITO.</div>
      </Rodal>
      <Rodal className="error" visible={visible2} onClose={handleClick} customStyles={modalStyle}>
          <div className="modal-update-img"></div>
          <div className="modal-update-info">SE ENCONTRO UN ERROR!</div>
      </Rodal>
      <Rodal className="ok" visible={visible3} onClose={handleClick} customStyles={modalStyle}>
          <div className="modal-update-img"></div>
          <div className="modal-update-info">Hemos enviado un correo a {email}, tienes 24 horas para confirmar el cambio</div>
      </Rodal>
      <Rodal className="error" visible={visible4} onClose={handleClick} customStyles={modalStyle}>
          <div className="modal-update-img"></div>
          <div className="modal-update-info">Esta identidificación no es válida</div>
      </Rodal>
      <Rodal className="error" visible={dataChangeModal} onClose={handleClick} customStyles={modalStyle}>
          <div className="modal-update-img"></div>
          <div className="modal-update-info">Tus datos no han cambiado</div>
      </Rodal>
    </>
  );
};

  
export default AccountPage;
