import "../styles/index.scss";
import Layout from "../components/Layout";
import TagManager from "react-gtm-module";
import { gtmTransversal } from "../components/Gtm";
import { useEffect, useState } from "react";

const tagManagerArgs = {
  gtmId: JSON.parse(process.env.GOOGLE).GTM.CONTAINER_ID,
}

const MyApp = ({ Component, pageProps }) => {
  useEffect(() => {
    import("jquery").then(($) => {
      window.$ = window.jQuery = $;
      return import("bootstrap");
    });

    TagManager.initialize(tagManagerArgs)
    {gtmTransversal()}
  }, []);  
  
  return (
    <>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  )
}

export default MyApp;
