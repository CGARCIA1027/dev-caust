import Head from "next/head";
import BannerArticulos from "../../components/BannerArticulos";
import SliderArticulos from "../../components/SliderArticulos";
import InfoImage from "../../components/InfoImage";
import InfoImageSmall from "../../components/InfoImageSmall";
import InfoAdd from "../../components/InfoAdd";
import BlockQuote from "../../components/Blockquote";
import GaleryX3 from "../../components/GaleryX3";
import BurguerMenu from "../../components/BurguerMenu";
import { gtmDetalle } from "../../components/Gtm";

//Galeria
import SliderGaleria from "../../components/SliderGaleria"
import GaleryCopyAdd from "../../components/GaleryCopyAdd";

//Video
import VideoArticulo from "../../components/VideoArticulo"

import React, {useState,useEffect, useRef } from "react";
import InternalPagesItemGrid from '../../components/InternalPagesItemGrid';
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import {useRouter} from "next/router"
import axios from "axios";

/**
 * Componente funcional encargado de mostrar el detalle de los articulos *
*/ 

const DetalleCategoria = ({DataDinamica, dataTipo, itemCategoria, metaDescription, metaImage}) => {
  const router = useRouter()


  useEffect( async() => {
    if (!router.isReady) return;
    const creationDate = new Date(DataDinamica[0]._source.fecha_creacion[0] * 1000);
    gtmDetalle(
      DataDinamica[0]._source.title[0], 
      DataDinamica[0]._source.nombre_categoria[0],
      DataDinamica[0]._source.subcategoria[0],
      DataDinamica[0]._source.name[0],
      DataDinamica[0]._source.nombre_personaje[0],
      creationDate.toLocaleDateString("en-US"),
      DataDinamica[0]._source.tipo[0],
      DataDinamica[0]._source.nid[0]
    )
    
  }, [])

  return (
    <>
      <Head>
        <title>Cáustica - {DataDinamica[0]._source.title[0]}</title>
        <meta name="viewport" content="width=device-width" />

        <meta name="robots" content="index,follow"/>

        <meta name="distribution" content="global" /> 
        <meta name="rating" content="general"/>
        <meta name="description" content={metaDescription}/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name="author" content="Caustica"/>
        <meta name="genre" content="News"/>
        <meta name="geo.placename" content="Colombia"/>
        <meta name="geo.region" content="CO"/>
        <meta name="language" content="spanish"/>

        <link rel="canonical" href={"https://www.caustica.co"+router.asPath}/>

        <meta property="og:type" content="website" />
        <meta property="og:title" content={DataDinamica[0]._source.title[0]} />
        <meta property="og:description" content={metaDescription} />
        <meta property="og:url" content={"https://www.caustica.co"+router.asPath} />
        <meta property="og:image" content={metaImage} />
        <meta property="og:site_name" content="Caustica" />
        <meta property="fb:admins" content="723352678331419" />
        <meta property="og:locale" content="es_CO" />
        <meta property="og:locale:alternate" content="es_CO" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@CausticaCo" />
        <meta name="twitter:creator" content="@CausticaCo" />
        <meta name="twitter:title" content="Iniciar Sesión" />
        <meta name="twitter:description" content={metaDescription} />
        <meta name="twitter:image" content={metaImage} />
      </Head>

        {dataTipo == "Informativo" ? (
            <>
                {DataDinamica.length > 0 ? <BannerArticulos detalleBannerArticulo={DataDinamica} /> : null}
                <div className='content-max acticulos'>
                <div className='content'>
                    <article>
                      {DataDinamica.length > 0 ? <InfoImage detalleInfoImage={DataDinamica} /> : null}
                      {DataDinamica.length > 0 ? <InfoAdd detalleInfoAdd={DataDinamica} /> : null}
                      {DataDinamica.length > 0 ? <BlockQuote citaArticulo={DataDinamica} /> : null}
                      {DataDinamica.length > 0 ? <InfoImageSmall detalleInfoImageSmall={DataDinamica} /> : null}
                      <aside className='publicidad pauta_970x90 '>
                          <img src="/static/assets/add-970x90.png"/>
                      </aside>
                          {DataDinamica.length > 0 ? <GaleryX3 articuloGaleryx3={DataDinamica} /> : null}
                    </article>
                </div>
                </div>
                {DataDinamica.length > 0 ? <SliderArticulos detalleSliderArticulos={DataDinamica} /> : null}
            </>
          ) : (
            null
          )}

        {dataTipo == "Galería" ? (
            <>
                {DataDinamica.length > 0 ? <SliderGaleria detalleGaleriaArticulo={DataDinamica} /> : null}
                {DataDinamica.length > 0 ? <GaleryCopyAdd detalleVideoArticulo={DataDinamica} /> : null}
            </>
          ) : (
            null
          )}

        {dataTipo == "Video" ? (
            <>
              {DataDinamica.length > 0 ? <VideoArticulo detalleVideoArticulo={DataDinamica} /> : null}
              {DataDinamica.length > 0 ? <GaleryCopyAdd detalleVideoArticulo={DataDinamica} /> : null}
            </>
          ) : (
            null
          )}     

      <div className="puede-interesar video">
          <h2>Te puede interesar</h2>
        <ResponsiveMasonry columnsCountBreakPoints={{ 576: 1, 768: 2, 1200: 3 }}>
          <Masonry className="masonry-content-dinamicas" style={{ padding: "90px" }}>
            {itemCategoria.length > 0 ? (
              itemCategoria.map((item) => {
                return <InternalPagesItemGrid key={item._id} {...item} />;
              })
            ) : (
              <p>No se encontraron resultados para tu búsqueda</p>
            )}
          </Masonry>
        </ResponsiveMasonry>
      </div>  
    </>
  );
};

export async function getServerSideProps(context) {
  
  let DataDinamica = ""
  let dataTipo = ""
  let subCategoria = ""
  let itemCategoria = ""
  let metaDescription = ""
  let metaImage = ""

  const paramId = context.params.detalle[2]
  let params = new URLSearchParams();
  params.set("nid", paramId);

  await axios.get(process.env.BASE_SERVER + "/api/elastic", { params })
    .then((res) => {
      DataDinamica = res.data.body.hits.hits
      dataTipo = res.data.body.hits.hits[0]._source.tipo[0]
      subCategoria = res.data.body.hits.hits[0]._source.id_subcategoria[0]
    }).catch((err) => {
       console.error(err);
    })

  await axios.get(process.env.URL_DOMAIN_ELTIEMPO+"q=id_subcategoria:"+subCategoria)
    .then((res) => {      
      itemCategoria = res.data.hits.hits
    }).catch((err) => {
       console.error(err);
    });

    let metaTipo = DataDinamica[0]._source.tipo[0]
    if(metaTipo === "Informativo"){
      metaDescription = DataDinamica[0]._source.descripcion_corta[0]
      metaImage = process.env.URL_ELTIEMPO_IMG+DataDinamica[0]._source.imagen_contenido_relacionado[0]
    } else if(metaTipo === "Galería" || metaTipo === "Galeria"){         
      metaDescription = DataDinamica[0]._source.body[0]
      metaImage = process.env.URL_ELTIEMPO_IMG+DataDinamica[0]._source.imagen_contenido_relacionado[0]
    } else if(metaTipo === "Video"){
      metaDescription = DataDinamica[0]._source.body[0]
      metaImage = "https://img.youtube.com/vi/" + DataDinamica[0]._source.id_video_youtube[0] + "/sddefault.jpg"
    }

  return {
    props: {DataDinamica, dataTipo, itemCategoria, metaDescription, metaImage}, 
  }
}

export default DetalleCategoria;
