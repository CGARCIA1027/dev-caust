
import Head from "next/head";
import Link from "next/link";
import axios from 'axios'
import BurguerMenu from "../components/BurguerMenu";
import { useEffect, useState, useRef  } from "react";
import Rodal from 'rodal';
import 'rodal/lib/rodal.css';

const RegisterPage = () => {

    const [isLoggedIn, setIsLoggedIn] = useState(null)

    const [name, setName] = useState('null')
    const [nameWarning, setNameWarning] = useState(false)

    const [lastName, setLastName] = useState('null')
    const [lastNameWarning, setLastNameWarning] = useState(false)

    const [email, setEmail] = useState('null')
    const [emailWarning, setEmailWarning] = useState(false)
    const [emailExistWarning, setEmailExistWarning] = useState(false)

    const [password, setPassword] = useState('null')
    const [passConf, setPassConf] = useState('null')
    const [equalPass, setEqualPass] = useState(false)
    const [completePass, setCompletePass] = useState(false)

    const [dataPolicy, setDataPolicy] = useState(false)
    const [terms, setTerms] = useState(false)
    const [termsWarning, setTermsWarning] = useState(false)
    const [visible, setVisible] = useState(false)    
    
    let userName =  useRef(null);
    let userLastName =  useRef(null);
    let userEmail = useRef(null);
    let userPassConf = useRef(null);
    let userPassword = useRef(null);
    let userDataPolicy = useRef(null);
    let userTerms = useRef(null)    

    function validatePassword (password){       
        if(password.length >= 6){
            var mayuscula = false;
            var minuscula = false;
            var numero = false;           
            
            for(var i = 0; i < password.length; i++){
                if( password.charCodeAt(i) >= 65 && password.charCodeAt(i) <= 90){ mayuscula = true; }
                else if( password.charCodeAt(i) >= 97 && password.charCodeAt(i) <= 122){ minuscula = true; }
                else if(password.charCodeAt(i) >= 48 && password.charCodeAt(i) <= 57){ numero = true; }                
            }
            if(mayuscula == true && minuscula == true && numero == true){
                return setCompletePass(true);
            }
        }        
        return setCompletePass(false);       
    }

    const handleChange = async (e) =>{    
        setName(userName.current.value);
        setLastName(userLastName.current.value);
        setEmail(userEmail.current.value);
        setPassword(userPassword.current.value);
        setPassConf(userPassConf.current.value);             
         
        if(e.target.className == 'register-policy'){ setDataPolicy(e.target.checked) } else { setTerms(e.target.checked) }                                             
    }
    const handleKeypress = e => {       
        if (e.charCode === 13 ) { 
          e.preventDefault();    
          handleClick() 
        }
    } 
    useEffect(() => {
      validatePassword(password)
       
      if(password == passConf){ 
        setEqualPass(true) } 
      else { 
        setEqualPass(false) 
      }                 
              
      if(password == '' && passConf == '' || passConf == password){ 
        setEqualPass(true) 
      } else { 
        setEqualPass(false) 
      }   

      if(password == 'null' && passConf == 'null'){ 
          setEqualPass(true)
          setCompletePass(true)            
      }
        
    }, [password, passConf, equalPass, completePass, termsWarning, terms, dataPolicy]);

    const modalStyle = {
      boxShadow: "0 1px 7px #000000a1, #e62e2e -14px 14px"
    };

    const handleClick = (e) =>{       
        if(e.currentTarget.className=='eye'){
            if(e.currentTarget.previousSibling.getAttribute('type')== 'text'){
              e.currentTarget.previousSibling.setAttribute('type','password');

              if(e.currentTarget.parentNode.classList.contains('pass')){
                document.querySelector('.pass').classList.remove('open');
              }
              if(e.currentTarget.parentNode.classList.contains("confirmPass")){
                document.querySelector('.confirmPass').classList.remove('open');
              }
            }else {
              e.currentTarget.previousSibling.setAttribute('type','text');
             
              if(e.currentTarget.parentNode.classList.contains('pass')){
                document.querySelector('.pass').classList.add('open');
              }
              if(e.currentTarget.parentNode.classList.contains("confirmPass")){
                document.querySelector('.confirmPass').classList.add('open');
              }
            }
        }else if(e.currentTarget.className == "rodal-close" || e.currentTarget.className == "rodal-mask"){
          setVisible(false)
          window.location.assign('/')
        }else{

          if( name =='null'){ setNameWarning(true) } else if (name ==''){ setNameWarning(true) } else {setNameWarning(false)} 
          if( lastName=='null'){ setLastNameWarning(true) } else if (lastName ==''){ setLastNameWarning(true) } else {setLastNameWarning(false)} 
          if( email=='null'){ setEmailWarning(true) } else if (email ==''){ setEmailWarning(true) } else {setEmailWarning(false)}        
          if( dataPolicy === false || terms === false){ setTermsWarning(true) } else { setTermsWarning(false) }
          if( password == passConf){ setEqualPass(true) } else { setEqualPass(false) }      
          
          if( terms === true && dataPolicy === true && equalPass === true && completePass === true ){
              if( name !== '' && lastName !== '' && email.includes('@') && email.includes('.') && name !== 'null' ){
                const regularMail = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;               
                if (regularMail.test(email) == true){
                  register()
                 } else {
                  alert("La dirección de email es incorrecta.");
                 }               
              }else(
                  alert('Compruebe los campos del formulario')
              )
          }

        }      
    }
    function registerWithParams(params, url) {       
        let EMPTOR = JSON.parse(process.env.EMPTOR)        
        //Se hace la petición al servicio de registro de usuario de EMPTOR        
        return axios.post(url, params, {
          headers: {
            'content-type': 'application/x-www-form-urlencoded',
            'Authorization': EMPTOR.BASIC_TOKEN
          }
        })
    }
    

    function loginWithParams(params) {
        let EMPTOR = JSON.parse(process.env.EMPTOR);
        let basic = EMPTOR.LOGIN.BASIC.replace('{CLIENT_ID}', process.env.CLIENT_ID)
        let url = EMPTOR.BASE_DOMAIN + basic;         
        const now = new Date() 
        let expiry = now.getTime() + 100000;
        const expiration = {
          value:  'classic',
          expiry: expiry
        }
      
        axios.post(url, params, {
              headers: {
                "content-type": "application/x-www-form-urlencoded",
                Authorization: EMPTOR.BASIC_TOKEN,
              },
            })
            .then((response) => {              
              if(response.data.data.accessToken){                
                setIsLoggedIn(true);
                window.localStorage.setItem('exp', JSON.stringify(expiration))                
                window.localStorage.setItem('user', JSON.stringify(response.data.data))
                window.localStorage.setItem("authType", JSON.stringify('Classic'));                        
                window.localStorage.setItem('IsLogged', true)                
                emptorMe()                               
               
              }else{
                console.log('no se genero token de acceso')
              }             
            })
            .catch((err) => {
              console.log(err)          
            });
        
    }      
    function emptorMe() {        
      let EMPTOR = JSON.parse(process.env.EMPTOR);
      let url = EMPTOR.BASE_DOMAIN + EMPTOR.ME.ME;     
      let bearer = JSON.parse(window.localStorage.getItem('user')).accessToken; 
       
      axios.post(
            url, 
            {},
            {
              headers: {
                Authorization: "Bearer " + bearer,
              },
            }
          ).then((responseEmptor) => {               
            window.localStorage.setItem('userEmptor',JSON.stringify(responseEmptor.data.data))
            //modal
            setVisible(true)                     
          }).catch((error) => {
            console.log("error---->", error);              
          });
    
    } 
      function register () {       
        let EMPTOR = JSON.parse(process.env.EMPTOR) 
        let url, params, register;              
        register = true;
        url = EMPTOR.BASE_DOMAIN + EMPTOR.REGISTER.BASIC
        url = url.replace('{CLIENT_ID}', process.env.CLIENT_ID)            
          params = new URLSearchParams()
            params.set('client_id', process.env.CLIENT_ID)
            params.set('terms', terms)
            params.set('politics', dataPolicy)
            params.set('firstname', name)
            params.set('lastname' , lastName)
            params.set('email', email)
            params.set('password', password)
        
        registerWithParams(params, url)        
        .then(resp => {                   
            loginWithParams(params, true)                              
        }).catch(err => {
            console.log(err)
            setEmailExistWarning(true)            
        })   
    }


  return (
    <>
      <Head>
        <title>Website Cáustica</title>
        <meta name="viewport" content="width=device-width" />

        <meta name="robots" content="No index"/>

        <meta name="distribution" content="global" /> 
        <meta name="rating" content="general"/>
        <meta name="description" content=""/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name="author" content="Caustica"/>
        <meta name="genre" content="News"/>
        <meta name="geo.placename" content="Colombia"/>
        <meta name="geo.region" content="CO"/>
        <meta name="language" content="spanish"/>

        <link rel="canonical" href="https://www.caustica.co/registro/"/>

        <meta property="og:type" content="website" />
        <meta property="og:title" content="{varTitulo}" />
        <meta property="og:description" content="{varDescripcion}" />
        <meta property="og:url" content="https://www.caustica.co/registro/" />
        <meta property="og:image" content="../../static/assets/1200x630-facebook.png" />
        <meta property="og:site_name" content="Caustica" />
        <meta property="fb:admins" content="723352678331419" />
        <meta property="og:locale" content="es_CO" />
        <meta property="og:locale:alternate" content="es_CO" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@CausticaCo" />
        <meta name="twitter:creator" content="@CausticaCo" />
        <meta name="twitter:title" content="{varTitulo}" />
        <meta name="twitter:description" content="{CausticaCo}" />
        <meta name="twitter:image" content="../../static/assets/280x150-twitter.png" />
      </Head>      
     
      <main className="registerMain">
        <div className="register-background">
            <div className="background-drops"/>            
            <div className="background-banner"/>
            <div className="register-info">
                <div className="register-info-content">
                    <div className="register-info-title">REGÍSTRATE</div>
                    <div className="register-info-icons">
                        <div className="register-icon-facebook"/>
                        <div className="register-icon-google"/>
                    </div>
                    <form className="user-inputs" onKeyPress={handleKeypress}>
                        <div className="user-names-inputs">
                            <div className="name-container">
                                <div className="user-name">Nombre</div>
                                    <div className="box-input">
                                        <input
                                            ref={userName} 
                                            type="text" 
                                            className="input-name"
                                            name="name"
                                            onChange={handleChange}
                                        />
                                    </div>
                                <p className="user-name-warning" hidden={nameWarning ?  false : true}>Ingresa tu nombre</p>
                            </div> 
                            <div className="last-name-container">                                  
                                <div className="user-last-name">Apellido</div>    
                                    <div className="box-input">
                                        <input
                                            ref={userLastName} 
                                            type="text" 
                                            className="input-last-name"
                                            name="lastName"
                                            onChange={handleChange}
                                        />
                                    </div>
                                <p className="user-lastname-warning" hidden={lastNameWarning ?  false : true}>Ingresa tu apellido</p>
                            </div>                           
                        </div> 
                        <br/>
                        <div className="mail-container">                                  
                                <div className="user-mail-title">Correo eléctronico</div>
                                    <div className="box-input">    
                                        <input
                                            ref={userEmail} 
                                            type="text" 
                                            className="input-mail"
                                            name="email"
                                            onChange={handleChange}
                                        />
                                    </div>
                                <p className="user-email-warning" hidden={emailWarning ?  false : true}>Ingresa tu correo electrónico</p>
                                <p className="user-email-warning" hidden={emailExistWarning ?  false : true}>Este correo ya se encuentra registrado o no es un correo válido</p>

                        </div>
                        <br/>
                        <div className="user-names-passwords">
                            <div className="password-container">
                                <div className="user-password-title">Contraseña</div>
                                    <div className="box-input pass">
                                        <input
                                            ref={userPassword} 
                                            type="password" 
                                            className="input-password"
                                            name="password"
                                            onChange={handleChange}
                                        />
                                        <span className="eye" onClick={handleClick}></span>
                                    </div>
                            </div> 
                            <div className="password-container">                                  
                                <div className="user-password-title">Confirmar contraseña</div>  
                                    <div className="box-input confirmPass">  
                                        <input
                                            ref={userPassConf} 
                                            type="password" 
                                            className="input-password"
                                            name="passConf"
                                            onChange={handleChange}
                                        />
                                        <span className="eye" onClick={handleClick}></span>
                                    </div>
                            </div>                           
                        </div>                       
                        <div className="under-password">    
                            <p>* Mínimo 6 caracteres, 1 minúscula, 1 mayúscula y 1 número</p>
                            <p className="equal-pass-alert" hidden={equalPass ?  true: false}>Las Contraseñas no coinciden</p>
                            <p className="complete-pass-alert" hidden={completePass ?   true : false}>Esta no es un contraseña valida</p>                              
                        </div>
                        <div className="terms-condtions-container">    
                            <form className="terms-condtions-container">                                                               
                                <input
                                    ref={userTerms}
                                    className="register-terms" 
                                    type="checkbox"
                                    defaultChecked={false} 
                                    onChange={handleChange}
                                />
                                <>He leído  y autorizo los</>&nbsp;
                                <a href="http://mailpush.eltiempo.com/Terminos%20y%20condiciones/Politicas.pdf" target="_blank">Términos & Condiciones</a>&nbsp;
                                <>de este portal.</>
                            </form>
                            <div className="terms-condtions-container">                                                               
                                <input
                                    className="register-policy" 
                                    type="checkbox"
                                    ref={userDataPolicy} 
                                    defaultChecked={false} 
                                    onChange={handleChange}
                                />
                                <>He leído, entendido y autorizo la</>&nbsp;
                                <a href="https://www.eltiempo.com/legal/POLITICA_DE_TRATAMIENTO_Y_PROCEDIMIENTOS_EN_MATERIA_DE_PROTECCION_DE_DATOS_PERSONALES.pdf" target="_blank">Política de tratamiento de Datos CASA EDITORIAL EL TIEMPO S.A.</a>&nbsp;
                                <>y su</>&nbsp;
                                <a href="https://www.eltiempo.com/politica-de-cookies" target="_blank">Política de Datos de Navegación / Cookies.</a>
                                <p className="user-terms-warning" hidden={termsWarning ?  false : true}>Recuerda aceptar los Términos y Condiciones, las políticas de tratamiento de datos y las políticas de navegación</p>
                            </div>                             
                        </div>
                        <div 
                            className="submit-button-register"
                            onClick={handleClick}>Continuar</div>
                        <div className="final-question">
                            <p>¿Ya tienes una cuenta?</p>&nbsp;
                            <Link  href="/iniciar-sesion">
                                <a><u>Ingresa aquí</u></a>
                            </Link>    
                        </div>

                        </form>
                </div>
            </div>
                
            

        </div>       
      </main>  
      <Rodal className="ok" visible={visible} onClose={handleClick} customStyles={modalStyle}>
          <div className="modal-update-img"></div>
          <div className="modal-update-info">REGISTRO EXITOSO</div>
      </Rodal>    
    </>
  );
};

  
export default RegisterPage;
