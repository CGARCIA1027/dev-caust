import Head from "next/head";
import MasonryGrid from "../components/MasonryGrid";
import BannerHome from "../components/BannerHome";
import FloatinButton from "../components/FloatinButton";
import BurguerMenu from "../components/BurguerMenu";
import Characters from "../components/Characters";
import React, { useEffect, useRef } from "react";
import axios from "axios";

const HomePage = ({promoteItems, tipoData, charactersData}) => {  
  
  useEffect(() => {    
    let hayScrollLoad = document.querySelector("body")
    if (hayScrollLoad.classList.contains("scroll")) {
       try{
          document.querySelector("body").classList.remove("scroll");
       }catch(error){
         //console.log(error)
       }
     }
  }, []);

     try{
        if(window.localStorage.getItem('IsLogged')){
           window.localStorage.removeItem('IsLogged')
           window.location.assign('/');
        }
     }catch(error){}
  return (
    <> 
      <Head>
        <title>Website Cáustica</title> 
        <meta name="viewport" content="width=device-width" />

        <meta name="robots" content="index,follow"/>

        <meta name="distribution" content="global" /> 
        <meta name="rating" content="general"/>
        <meta name="description" content=""/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name="author" content="Caustica"/>
        <meta name="genre" content="News"/>
        <meta name="geo.placename" content="Colombia"/>
        <meta name="geo.region" content="CO"/>
        <meta name="language" content="spanish"/>

        <link rel="canonical" href="https://www.caustica.co/"/>

        <meta property="og:type" content="website" />
        <meta property="og:title" content="{varTitulo}" />
        <meta property="og:description" content="{varDescripcion}" />
        <meta property="og:url" content="https://www.caustica.co/" />
        <meta property="og:image" content="../../static/assets/1200x630-facebook.png" />
        <meta property="og:site_name" content="Caustica" />
        <meta property="fb:admins" content="723352678331419" />
        <meta property="og:locale" content="es_CO" />
        <meta property="og:locale:alternate" content="es_CO" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@CausticaCo" />
        <meta name="twitter:creator" content="@CausticaCo" />
        <meta name="twitter:title" content="{varTitulo}" />
        <meta name="twitter:description" content="{CausticaCo}" />
        <meta name="twitter:image" content="../../static/assets/280x150-twitter.png" />
         
      </Head> 
     
      
      <main className="main home">        
        <BannerHome promoteItems={promoteItems}/>
        <MasonryGrid tipoData={tipoData}/>
        <aside className='publicidad pauta_970x90 home'>
            <img src="../../static/assets/add-970x90.png"/>
        </aside>
        <Characters charactersData={charactersData}/>
        <FloatinButton/>             
      </main>
      
    </>
  );
};
export async function getServerSideProps() { 
  //peticion del contenido promovido
  let promoteItems=""
  let tipoData = [] 
      await axios.get(process.env.BASE_SERVER + '/promote')
      .then((res) => {           
          promoteItems = JSON.parse(JSON.stringify(res.data.body.hits.hits))                    
      }).catch(err => {                
          // console.log(err)
      }) 
  //Peticion de las tarjetas
  let tipoIds = ["Calendario","Informativo","Galería","Video"]
  let params = new URLSearchParams()
     for(let i = 0; i < tipoIds.length; i++) {
       params.set("tipo", tipoIds[i])
       await axios.get(process.env.BASE_SERVER + '/api/elastic/', {params})
       .then((res) => {
          try{              
            for(var j = 0; j<=res.data.body.hits.hits.length-1; j++ ){
              if(res.data.body.hits.hits[j]._source.content_type[0] !== "privacidad"){
                tipoData.push(res.data.body.hits.hits[j])
              }
            }
          }catch(error){
            
          }
       }).catch(err => {                
          // console.log(err)
       });
     }    
 //peticion para los personajes
    let params2 = new URLSearchParams()
    let charactersData = []
    let charactersIds = ["3","39","40","41"]    
    for(let i = 0; i < charactersIds.length; i++) {      
        params2.set("id_personaje", charactersIds[i])
        await axios.get(process.env.BASE_SERVER + '/api/elastic/', {params2})
          .then((res) => {         
            try{              
              charactersData.push(res.data.body.hits.hits[0])
            }catch(error){
              // console.log(error)
            }
          }).catch((err) => {
            //  console.log(err)
          });
    }
    // console.log(charactersData) 
  return {
    props: {promoteItems, tipoData, charactersData}, 
  }
}

export default HomePage;
