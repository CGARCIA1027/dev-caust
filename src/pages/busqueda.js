import Head from "next/head";
import { useEffect, useState, useRef  } from "react";
import FloatinButton from "../components/FloatinButton";
import FilterBoard from '../components/FilterSelects/FilterBoard';
import BurguerMenu from "../components/BurguerMenu";
import SearchItemGrid from '../components/SearchItemGrid';
import FilterSelect from '../components/FilterSelects/index'
import { filterValues1,  filterValues2,  filterValues3, groupedOptions  } from "../components/FilterSelects/filterValues";
import axios from 'axios'

const searchPage = () => {   
    const [searchResult, setSearchResult] = useState([])
    const [isLoaded, setIsLoaded] = useState(false)
    const [itemsLoaded, setItemsLoaded] = useState(15)
    const [loading, setLoading] = useState(true)
    const [hideLoadButton, setHideLoadButton] = useState(true)     


    const handleChange = async (e) =>{
    }

    const handleClick = (e) => { 
      if(e.target.className == "home-grid-loadMore"){
        setItemsLoaded(itemsLoaded + 6)
      }else{
        window.location.reload()
      }    
     
    }        

    useEffect( async () => {  
      
        if(itemsLoaded >= searchResult.length){
          document.getElementById("btn-load").style.display = "none";
        }

        let searchInput = JSON.parse(window.localStorage.getItem('search'));        
        var filter = [[], [], []]; 
        let path = window.location.pathname
        try{
          for(var i=0; i < 3; i++){
            var filterPass = JSON.parse(window.localStorage.getItem('filters'+i+path)) 
            if(filterPass === null){}else{
              for(var j=0; j < filterPass.length; j++){                         
                filter[i].push(filterPass[j].value);            
              } 
            }       
          }
        }catch(error){
          // console.log(error)
        }  
       
        if(filter[0].length > 0 || filter[1].length > 0 || filter[2].length > 0){
         
          let params = new URLSearchParams()  
          for(var k = 0; k < 3; k++){                       
                switch (k) {
                  case 0:                    
                    params.set('categorias', filter[0])                                 
                    // break;
                  case 1:                   
                    params.set('personajes', filter[1])
                    // break;                 
                  case 2:                   
                    params.set('sentidos', filter[2])
                  // break;
                  default:
                   
                  // break;
                }           
           
            params.set('search', searchInput)           
            await axios.get('/api/search/filters', {params})
            .then((res) => {                       
                setSearchResult(res.data.body.hits.hits)
                setIsLoaded(true)
                             
            })
            .catch(err => {
                console.log(err)                              
            })
            setLoading(false)           
          }
        }else{
            let params = new URLSearchParams()
            params.set('search', searchInput)            
            await axios.get('/api', {params})
            .then((res) => {              
              console.log(res)
              setSearchResult(res.data.body.hits.hits)
              setIsLoaded(true)
                           
            })
            .catch(err => {
              console.log(err) 
                            
            })               
            params.delete('search')            
            setLoading(false)
            if(filterPass === null || filterPass === undefined){              
            }else{
              for(var j=0; j < filterPass.length; j++){                         
                filter[i].push(filterPass[j].value);            
              }
            }     
        }
             
       
    }, [itemsLoaded]);
    
    

  return (    
    <>    
      <Head>
        <title>Website Cáustica</title>
        <meta name="viewport" content="width=device-width" />
        <meta name="robots" content="No index"/>
      </Head>      
      <BurguerMenu/>
      <main className="main search-results"> 
      
        <div className="search-selects">
          <FilterSelect onChange={handleChange} filterValues={filterValues1} filterName ='Categorias' id='0'/>
          <FilterSelect filterValues={filterValues2} filterName ='Personajes' id='1'/>
          <FilterSelect filterValues={filterValues3} filterName ='Sentidos' id='2'/>
          <button onClick={handleClick} >Aplicar Filtros</button>          
        </div>
    
        <FilterBoard/>
    
        <div className="search-items-results">              
              {
               loading === false ? (                
                searchResult.length > 0                
                ? searchResult.map((item, q) => {                  
                  if(item._source.content_type){                                        
                    if(q <= itemsLoaded && isLoaded === true && item._source.name){                                      
                        return (                                               
                            <SearchItemGrid  key={q} {...item}/>                         
                          );
                      }  
                  } 
                  }) :  <div className="noItems">
                          <h2><span>X</span> No hay resultados</h2>
                          <div className="content">
                            <p><b>Lo sentimos esta búsqueda no está disponible</b></p>
                            <p>Para obtener lo mejores resultados de búsqueda sigue estos consejos</p>
                            <ul>
                              <li>Compruebe la ortografía</li>
                              <li>Prueba con términos similares o sinónimos</li>
                              <li>Prueba con más de una sola palabra</li>
                            </ul>
                          </div>
                        </div>
              ) : (                           
                <h2>Loading...</h2>
              )
            }          
                      
        </div>
        <div id="btn-load" className="home-grid-loadMore" onClick={handleClick}>Cargar más</div>                
        <FloatinButton/>               
      </main>
    
    </>
  );
};
  

export default searchPage;
