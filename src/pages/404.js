import Head from "next/head";
import Link from "next/link";
import BurguerMenu from "../components/BurguerMenu";
import React from 'react'

/**
 * Componente funcional encargado de mostrar una pagina 404 en rutas o paginas no encontradas
 * 
 */


const Custom404 = () => {
  return (
    <>
      <Head>
        <title>Cáustica - Página no encontrada</title>
        <meta name="viewport" content="width=device-width" />
        <meta name="robots" content="No index"/>
      </Head>

      <div className='caustica-404'>
        <div className='content-max'>
        
            <div className='content'>
              
              <div className='info'>

                <div className='content-info'>
                  <h1>404</h1>
                  <h2>¡Lo sentimos!</h2>
                  <p>La página que busca se encuentra dañada o no existe</p>
                  <Link href="/">
                  <a></a>
                  </Link>
                </div>
                
              </div>

              <div className='image-banner'>
                  <picture>
                        <source 
                          srcSet="/static/assets/404-m.png"
                          media="(max-width: 1024px)" />
                        <img
                          src="/static/assets/404.png"
                          alt="404"
                          title="404"
                    />
                    </picture>
                </div>

            </div>
        </div>
    </div>

    </>
  );
}

export default Custom404;
