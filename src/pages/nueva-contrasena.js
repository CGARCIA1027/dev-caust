import Head from "next/head";

import { useState, useRef  } from "react";

const NewPassword = () => { 
  
const [newPassword, setNewPassword] = useState(null)
const [confPassword, setConfPass] = useState(null)
const [passwordWarning, setPasswordWarning] = useState(false)

let userNewPassword = useRef(null);
let userNewPassConf = useRef(null);

const handleChange = async (e) =>{
  setNewPassword(userNewPassword.current.value);
  setConfPass(userNewPassConf.current.value);                                        
}

const handleClick = (e) =>{    
  if(e.currentTarget.className=='eye'){
    if(e.currentTarget.previousSibling.getAttribute('type')== 'text'){
      e.currentTarget.previousSibling.setAttribute('type','password');
    }else {
      e.currentTarget.previousSibling.setAttribute('type','text');
    }
  }  
  
  if(e.currentTarget.className=='continuar-button'){
     
    (newPassword == confPassword) ? recoveryPasswordEmptor() : setPasswordWarning(true);
                                                 
   
  }
}

function recoveryPasswordEmptor (user_hash) { 
  let EMPTOR = JSON.parse(process.env.EMPTOR)
  let url = EMPTOR.BASE_DOMAIN + EMPTOR.RECOVERY_PASS.replace('{CLIENT_ID}', process.env.CLIENT_ID)
  console.log(url)
  console.log(EMPTOR)

  //   axios.post(url,
  //     'new_password=' + newPassword + '&confirm_password=' + confPassword,
  //     {
  //       headers: {
  //         'Content-Type': 'application/x-www-form-urlencoded',
  //         'Authorization': 'Bearer ' + user_hash
  //       }
  //     }).then(response => {
  //       console.log(response)     
  //   }).catch(error => {
  //     console.log(error)      
  // })
}

  return (
    <>
      <Head>
        <title>Website Cáustica</title>
        <meta name="viewport" content="width=device-width" />
        <meta name="robots" content="No index"/>
      </Head>      
      
      <main className="newPass">
        <div className="newPass-background">
            <div className="background-drops"/>            
            <div className="background-banner"/>
            <div className="newPass-info">
                <div className="newPass-info-content">
                       
                        <div className="newPass-info-title">NUEVA CONTRASEÑA</div>
                       
                        <div className="content new-passwords">
                            <div className="password-container">
                                <div className="password-title">Nueva contraseña</div>
                                    <div className="box-input-new">
                                        <input
                                            type="password" 
                                            className="input-new-password"
                                            name="password"
                                            onChange={handleChange}
                                            ref={userNewPassword}
                                        />
                                        <span className="eye" onClick={handleClick}></span>
                                    </div>
                            </div>

                            <div className="password-container">                                  
                                <div className="password-title">Confirmar contraseña</div>  
                                    <div className="box-input-confirm">  
                                        <input
                                            type="password" 
                                            className="confirm-input-new-password"
                                            name="passConf"
                                            onChange={handleChange}
                                            ref={userNewPassConf}
                                        />                                      
                                        <span className="eye" onClick={handleClick}></span>
                                    </div>
                            </div>
                            <p className="user-name-warning" hidden={passwordWarning ?  false : true}>Las contraseñas no coinciden</p>                           
                        </div>  

                        <div className="buttons-container">    
                            <div  
                              className="continuar-button"
                              onClick={handleClick}
                            />
                        </div>
                            
                </div>
            </div>
                
            

        </div>       
      </main>      
    </>
  );
};

  
export default NewPassword;
