export const getServerSideProps = async (ctx) => {
  // Method to source urls from cms

  const fields = [
    {
      loc: 'https://www.caustica.co', 
      lastmod: new Date().toISOString(),
    },
  ]

  return getServerSideSitemap(ctx, fields)
}

// Default export to prevent next.js errors
export default () => {}
