import Head from "next/head";
import React, {useState,useEffect, useRef } from "react";
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import InternalPagesItemGrid from '../components/InternalPagesItemGrid';
import { gtmMasonryCategoriasCargarMas, gtmBtnBuscadorSentidos } from "../components/Gtm";
import axios from "axios";
import FloatinButton from "../components/FloatinButton";
import FilterSelect from '../components/FilterSelects/index'
import { filterValues1, filterValues2, groupedOptions  } from "../components/FilterSelects/filterValues";
import FilterBoard from "../components/FilterSelects/FilterBoard";
/**
 * Componente funcional encargado de mostrar la pagina de secciones del menu
 *
*/

const menuSections = ({verItems}) => {
  const [cleanFiltersShow, setCleanFiltersShow] = useState(false)
  const [itemsLoaded, setItemsLoaded] = useState(4)
  const [seeItems, setSeeItems] = useState(false)
  const [loading, setLoading] = useState(true)
  const [searchParams , setSearchParams] = useState('')
  let searchInput = useRef('');

  const handleChange = async (e) =>{    
    setSearchParams(searchInput.current.value);
  }  

  const handleKeypress = (e) => {              
    if (e.charCode === 13 ) {
      e.preventDefault();
      window.localStorage.setItem('search', JSON.stringify(searchParams))
      window.location.assign("/busqueda")
    }
  }

  useEffect( async () => {        
    let filter = [[], []]; 
    let path = window.location.pathname
    
    try{
      for(var i=0; i < 2; i++){
        let filterPass = JSON.parse(window.localStorage.getItem('filters'+i+path)) 
        if(filterPass === null){}else{
          for(var j=0; j < filterPass.length; j++){                         
            filter[i].push(filterPass[j].value);            
          } 
        }       
      }
    }catch(error){
      console.log(error)
    }  
  if(filter[0].length > 0 || filter[1].length > 0 ){
    setCleanFiltersShow(true)
    let params = new URLSearchParams()  
    for(var k = 0; k < 3; k++){                       
          switch (k) {
            case 0:                    
              params.set('categorias', filter[0])                                 
              // break;
            case 1:                   
              params.set('nombre_personaje', filter[1])
              // break;              
            default:              
            // break;
          }           
     
        params.set("name", "Ver")           
        await axios.get('/api/intern/filters', {params})
        .then((res) => {                                         
          setSeeItems(res.data.body.hits.hits)                                
        })
        .catch(err => {
            console.log(err)                              
        })
        setLoading(false)           
    }
  }else{setSeeItems(verItems)}
    console.log(itemsLoaded)
    console.log(verItems.length)
  if(itemsLoaded >= verItems.length){
    document.getElementById("btn-load").style.display = "none";
  }

}, [itemsLoaded]);
  
  const handleClick = (e) => {    
    if(e.target.className == "clean-filters-button"){
      let path = window.location.pathname
      try{
        for(var i=0; i < 3; i++){         
         window.localStorage.removeItem('filters'+i+path)             
        }
      }catch(error){
        // console.log(error)
      }
      window.location.reload()  
    }else if(e.target.className == "intern-search-click"){
      window.localStorage.setItem('search', JSON.stringify(searchParams))
      gtmBtnBuscadorSentidos(JSON.stringify(searchParams))
      window.location.assign("/busqueda")
    }else if(e.target.className == "home-grid-loadMore"){
        setItemsLoaded(itemsLoaded + 6)
        gtmMasonryCategoriasCargarMas()
    }else {
      window.location.reload()
    }   
    
  }
  return (
    <>
      <Head>
        <title>Caustica - Ver</title>
        <meta name="viewport" content="width=device-width" />

        <meta name="robots" content="index,follow"/>

        <meta name="distribution" content="global" /> 
        <meta name="rating" content="general"/>
        <meta name="description" content=""/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name="author" content="Caustica"/>
        <meta name="genre" content="News"/>
        <meta name="geo.placename" content="Colombia"/>
        <meta name="geo.region" content="CO"/>
        <meta name="language" content="spanish"/>

        <link rel="canonical" href="https://www.caustica.co/ver/"/>

        <meta property="og:type" content="website" />
        <meta property="og:title" content="{varTitulo}" />
        <meta property="og:description" content="{varDescripcion}" />
        <meta property="og:url" content="https://www.caustica.co/ver/" />
        <meta property="og:image" content="../../static/assets/1200x630-facebook.png" />
        <meta property="og:site_name" content="Caustica" />
        <meta property="fb:admins" content="723352678331419" />
        <meta property="og:locale" content="es_CO" />
        <meta property="og:locale:alternate" content="es_CO" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@CausticaCo" />
        <meta name="twitter:creator" content="@CausticaCo" />
        <meta name="twitter:title" content="{varTitulo}" />
        <meta name="twitter:description" content="{CausticaCo}" />
        <meta name="twitter:image" content="../../static/assets/280x150-twitter.png" />
      </Head>

      <div className="sentidos-menu-banner">
        <div className='content'>

            <nav className="main-breadcrumb">
                <ol className="breadcrumb">
                          <li className="breadcrumb-item ">
                                  <a href="/">Home</a>
                              </li>
                          <li className="breadcrumb-item active">
                                  Ver
                              </li>
                      </ol>
              </nav>

              <div className="item">
                  <img
                      src="/static/assets/ver.png"
                      alt="ver"
                      title="ver"
                  />
              </div>

          </div>

          <div className='background-color' style={{ backgroundColor: '#000000' }}></div>

          <div className='background'>
                    <picture>
                          <source 
                            srcSet="/static/assets/back-banner-sentidos.jpg" 
                            media="(max-width: 1024px)" />
                          <img
                            src="/static/assets/back-banner-sentidos.jpg"
                            alt="banner"
                            title="banner"
                      />
                      </picture>
                  </div>

      </div>
      <div className="search-results"> 
          <div className="search-selects">        
              <FilterSelect filterValues={filterValues1} filterName ='Categorias' id='0'/>         
              <FilterSelect filterValues={filterValues2} filterName ='Sentidos' id='1'/>
              <button onClick={handleClick} >Aplicar Filtros</button>
              <input 
                type="text" 
                id="second-search"
                className="intern-search-input"                
                placeholder="BUSCAR"
                onKeyPress={handleKeypress}
                onChange={handleChange}
                ref={searchInput}
                style={{background:"url(/static/assets/search-intern-icon.png) no-repeat right"}}                
              />
              <div className="intern-search-click" onClick={handleClick}/>              
          </div>
          
      </div>
      <div className="filter-board-container">
          <FilterBoard/>
          <button className="clean-filters-button" onClick={handleClick} hidden={cleanFiltersShow  ?  false : true}>Limpiar filtros</button>
      </div>
      <div className="main-masonry-sentidos">
      <ResponsiveMasonry
        columnsCountBreakPoints={{ 576: 1, 768: 2, 1500: 3 }}
      >
        <Masonry className="masonry-content-dinamicas" style={{ padding: "90px" }}>
          {seeItems.length > 0 ? (
            seeItems.map((item, q) => {
              if(q <= itemsLoaded) {
                return <InternalPagesItemGrid key={item._id} {...item} />;
               }
            })
          ) : (
            <p>No se encontraron resultados para tu búsqueda</p>
          )}
        </Masonry>
      </ResponsiveMasonry>
      <FloatinButton/>
      <div id="btn-load" className="home-grid-loadMore" onClick={handleClick} >Cargar más</div>
      </div>
      
    </>
  );
};

export async function getServerSideProps() {  
  let verItems
  let params = new URLSearchParams()
      params.set("name", "Ver")
    await axios.get(process.env.BASE_SERVER + '/api/elastic/', {params})
    .then((res) => {
      try{
          verItems = JSON.parse(JSON.stringify(res.data.body.hits.hits))      
        }catch(error){
          console.log(error)
        }      
    }).catch(err => {
      // console.log(err)                              
    });;
  
    
  return {
    props: {verItems}, 
  }
}

export default menuSections;