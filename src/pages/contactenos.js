import Head from "next/head";
// import { CanonicalTag } from "../components/CanonicalTag";
import BurguerMenu from "../components/BurguerMenu";
import BannerAbout from "../components/BannerContactenos";
import FaqsAbout from "../components/FaqsContactenos";
import FloatinButton from "../components/FloatinButton";

/**
 * Componente funcional encargado de mostrar la pagina de contactenos
**/

//Seo
// const canonicalTagUrl = CanonicalTag();

const Contactenos = () => {
  return (
    <>
      <Head>
      <title>Cáustica - Contáctenos</title>
      <meta name="viewport" content="width=device-width" />

      <meta name="robots" content="index,follow"/>

      <meta name="distribution" content="global" /> 
      <meta name="rating" content="general"/>
      <meta name="description" content=""/>
      <meta name="format-detection" content="telephone=no"/>
      <meta name="author" content="Caustica"/>
      <meta name="genre" content="News"/>
      <meta name="geo.placename" content="Colombia"/>
      <meta name="geo.region" content="CO"/>
      <meta name="language" content="spanish"/>

      <link rel="canonical" href="https://www.caustica.co/contactenos/"/>

      <meta property="og:type" content="website" />
      <meta property="og:title" content="{varTitulo}" />
      <meta property="og:description" content="{varDescripcion}" />
      <meta property="og:url" content="https://www.caustica.co/contactenos/" />
      <meta property="og:image" content="../../static/assets/1200x630-facebook.png" />
      <meta property="og:site_name" content="Caustica" />
      <meta property="fb:admins" content="723352678331419" />
      <meta property="og:locale" content="es_CO" />
      <meta property="og:locale:alternate" content="es_CO" />

      <meta name="twitter:card" content="summary" />
      <meta name="twitter:site" content="@CausticaCo" />
      <meta name="twitter:creator" content="@CausticaCo" />
      <meta name="twitter:title" content="{varTitulo}" />
      <meta name="twitter:description" content="{CausticaCo}" />
      <meta name="twitter:image" content="../../static/assets/280x150-twitter.png" />
      
      </Head>
      <BannerAbout/>
      <FloatinButton/>
      <FaqsAbout/>
      
    </>
  );
};

export default Contactenos;