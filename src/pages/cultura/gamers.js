import Head from "next/head";
import BannerMenuSections from "../../components/BannerMenuSections";
import MenuChild from "../../components/MenuSectionsChild";
import BurguerMenu from "../../components/BurguerMenu";
import InternalPagesItemGrid from '../../components/InternalPagesItemGrid';
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import { ItemsCultura } from "../../components/MenuChild/SubCategorias";
import React, {useState,useEffect, useRef } from "react";
import axios from "axios";
import FloatinButton from "../../components/FloatinButton";

/**
 * Componente funcional Cultura - Gamers
 *
 */

const Gamers = ({ itemsGamers }) => {
  const [gamers, setgamers] = useState([]);
  useEffect( async () => {
      let itemsGamers = []
      let params = new URLSearchParams()
      params.set("id_subcategoria", "23")
        axios.get('/api/elastic/', {params}).then((res) => {
          itemsGamers.push(res.data.body.hits.hits)
          setgamers (itemsGamers)
        });
    },[])
  
    let lengthData = ""
  
    gamers.map((item, i) => { 
      if(item.length > 0){
        lengthData = "1"
      }  
    })
  const subCategorias = ItemsCultura();

  return (
    <>
      <Head>
        <title>Caustica - Cultura</title>
        <meta name="viewport" content="width=device-width" />

        <meta name="robots" content="index,follow"/>

        <meta name="distribution" content="global" /> 
        <meta name="rating" content="general"/>
        <meta name="description" content=""/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name="author" content="Caustica"/>
        <meta name="genre" content="News"/>
        <meta name="geo.placename" content="Colombia"/>
        <meta name="geo.region" content="CO"/>
        <meta name="language" content="spanish"/>

        <link rel="canonical" href="https://www.caustica.co/cultura/gamers/"/>

        <meta property="og:type" content="website" />
        <meta property="og:title" content="{varTitulo}" />
        <meta property="og:description" content="{varDescripcion}" />
        <meta property="og:url" content="https://www.caustica.co/cultura/gamers/" />
        <meta property="og:image" content="../../static/assets/1200x630-facebook.png" />
        <meta property="og:site_name" content="Caustica" />
        <meta property="fb:admins" content="723352678331419" />
        <meta property="og:locale" content="es_CO" />
        <meta property="og:locale:alternate" content="es_CO" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@CausticaCo" />
        <meta name="twitter:creator" content="@CausticaCo" />
        <meta name="twitter:title" content="{varTitulo}" />
        <meta name="twitter:description" content="{CausticaCo}" />
        <meta name="twitter:image" content="../../static/assets/280x150-twitter.png" />
      </Head>
      
      <BannerMenuSections categoria="Cultura"/>
      <MenuChild MenuSectionsChild={subCategorias} />
      <div className="main-masonry-secciones">
      <ResponsiveMasonry
        columnsCountBreakPoints={{ 576: 1, 768: 2, 1200: 3 }}
      >
        <Masonry className="masonry-internas-dinamicas">
        {lengthData == 1 ? (
            gamers[0].map((item) => {
              return <InternalPagesItemGrid key={item._id} {...item} />;
            })
          ) : (
            <p>No se encontraron resultados para tu búsqueda</p>
          )}
        </Masonry>
      </ResponsiveMasonry>
      <FloatinButton/>
      </div>
    </>
  );
};

export default Gamers;
