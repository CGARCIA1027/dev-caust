import Head from "next/head";
import Link from "next/link";
import React from "react";
import BurguerMenu from "../components/BurguerMenu";
import { useEffect, useState, useRef  } from "react";
import Rodal from 'rodal';
import 'rodal/lib/rodal.css';
import axios from 'axios'
 

const ForgotPassword = () => {
  const [email, setEmail] = useState(null)
  const [visible, setVisible] = useState(false)
  const [visibleError, setVisibleError] = useState(false)
  
  let userEmail = useRef(null);
  
  const handleChange = async (e) =>{   
    setEmail(userEmail.current.value);                                                 
  }
  const handleClick = (e) =>{    
    if(e.currentTarget.className=='enviar-button'){      
      const regularMail = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
        if (regularMail.test(email)){              
          recoveryPassword()
        } else {         
          setVisibleError(true)
        }    
    }else{
      setVisible(false)
      setVisibleError(false)
    }
  }
  function recoveryPassword() {        
    let EMPTOR = JSON.parse(process.env.EMPTOR)
    let url = EMPTOR.BASE_DOMAIN + EMPTOR.RECOVERY
    let params = new URLSearchParams()
        params.set('email', email);      
        
        axios.post(url, params, {
          headers: { 
            'Content-Type': 'application/x-www-form-urlencoded'         
          }
        }).then(res => {                   
          if(res.data.hasOwnProperty('error')){            
            setVisibleError(true)           
          }else{        
            setVisible(true)
          }  
        }).catch(err => {
          console.log(err)
        })    
  }
  const modalStyle = {
    boxShadow: "0 1px 7px #000000a1, #e62e2e -14px 14px"
  };  
  return (
    <>
      <Head>
        <title>Website Cáustica</title>
        <meta name="viewport" content="width=device-width" />
        <meta name="robots" content="No index"/>
      </Head>      
      
      <main className="forgotMain">
        <div className="login-background">
            <div className="background-drops"/>            
            <div className="background-banner"/>
            <div className="login-info">
                <div className="login-info-content">
                       
                        <div className="login-info-title">¿OLVIDASTE TU CONTRASEÑA?</div>
                        <p>Ingresa tu correo electrónico para enviar tu nueva contraseña  </p>
                        <div className="mail-title">Correo Electronico</div>
                        <div className="inputBox">
                          <input type="text" 
                                 placeholder="Ingresa tu correo electrónico"
                                 className="input-mail"
                                 ref={userEmail}
                                 onChange={handleChange}
                          />                       
                        </div>
                        <div className="buttons-container">
                           <Link href="/iniciar-sesion"> 
                              <a>
                                  <div  className="volver-button">Volver</div>
                              </a>
                           </Link>     
                            <div 
                            className="enviar-button"
                            onClick={handleClick}>Enviar</div>
                            
                        </div>
                            
                </div>
            </div>
                
            

        </div>            
      </main> 
      <Rodal visible={visible} onClose={handleClick} customStyles={modalStyle} >
          <img className="modal-update-img"></img>
          <div className="modal-email-sended">CORREO ENVIADO</div>
          <div className="modal-email-sended-sub">Hemos enviado un correo a {email} para que puedas recuperar tu contraseña</div>
          <div className="modal-email-sended-sub">Revisa tu bandeja de entrada o tu carpeta de correo no deseado</div>
      </Rodal>
      <Rodal visible={visibleError} onClose={handleClick} customStyles={modalStyle} >
          <img className="modal-update-img"></img>
          <div className="modal-email-sended">SE ENCONTRO UN ERROR</div>
          <div className="modal-email-sended-sub">Revisa tus datos, no es un correo valido o no se encuentra registrado.</div>          
      </Rodal>    
    </>
  );
};

  
export default ForgotPassword;
