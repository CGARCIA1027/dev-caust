import Head from "next/head";
import React, {useState,useEffect, useRef } from "react";
import HomeItemGrid from '../components/HomeItemGrid'
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import InternalPagesItemGrid from '../components/InternalPagesItemGrid';
import { gtmMasonryPersonajesCargarMas, gtmBtnBuscadorPersonajes } from "../components/Gtm";
import FilterSelect from '../components/FilterSelects/index'
import FloatinButton from "../components/FloatinButton";
import { filterValues1, filterValues3, groupedOptions  } from "../components/FilterSelects/filterValues";
import FilterBoard from "../components/FilterSelects/FilterBoard"
import axios from "axios";

/**
 * Componente funcional encargado de mostrar la pagina de secckiones del menu
 *
*/

const menuSections = ({personajePaloma}) => {
  let serverURL = process.env.URL_ELTIEMPO_IMG;
  const [itemsLoaded, setItemsLoaded] = useState(2)
  const [searchResult, setSearchResult] = useState([])
  const [isLoaded, setIsLoaded] = useState(false)
  const [loading, setLoading] = useState(true)
  const [cleanFiltersShow, setCleanFiltersShow] = useState(false)
  const [searchParams , setSearchParams] = useState('') 

  const handleClick = (e) => {    
    if(e.target.className == "clean-filters-button"){
      const path = window.location.pathname
      try{
        for(var i=0; i < 3; i++){         
         window.localStorage.removeItem('filters'+i+path)             
        }
      }catch(error){
        // console.log(error)
      }
      window.location.reload()  
    }else if(e.target.className == "intern-search-click"){
      window.localStorage.setItem('search', JSON.stringify(searchParams))
      gtmBtnBuscadorPersonajes(JSON.stringify(searchParams))
      window.location.assign("/busqueda")
    }else if(e.target.className == "home-grid-loadMore"){
      setItemsLoaded(itemsLoaded + 6)
      gtmMasonryPersonajesCargarMas()
    }else{
      window.location.reload()
    }   
    
  }

  let searchInput = useRef('');

  const handleChange = async (e) =>{  
    setSearchParams(searchInput.current.value);
  }  

  const handleKeypress = (e) => { 
             
    if (e.charCode === 13 ) {
      e.preventDefault();
      window.localStorage.setItem('search', JSON.stringify(searchParams))
      window.location.assign("/busqueda")
      alert("click buscxador enter")
    }
  }
  
  useEffect( async () => {        
    let filter = [[], []]; 
    let path = window.location.pathname
    
    try{
      for(var i=0; i < 2; i++){
        let filterPass = JSON.parse(window.localStorage.getItem('filters'+i+path)) 
        if(filterPass === null){}else{
          for(var j=0; j < filterPass.length; j++){                         
            filter[i].push(filterPass[j].value);            
          } 
        }       
      }
    }catch(error){
      console.log(error)
    }  
   
    if(filter[0].length > 0 || filter[1].length > 0 ){
      setCleanFiltersShow(true)
      let params = new URLSearchParams()  
      for(var k = 0; k < 3; k++){                       
            switch (k) {
              case 0:                    
                params.set('categorias', filter[0])                                 
                // break;
              case 1:                   
                params.set('name', filter[1])
                // break;              
              default:              
              // break;
            }           
       
          params.set("id_personaje", "38")           
          await axios.get('/api/intern/filters', {params})
          .then((res) => {                                 
              setSearchResult(res.data.body.hits.hits)
              setIsLoaded(true)                        
          })
          .catch(err => {
              console.log(err)                              
          })
          setLoading(false)           
      }
    }

    if(itemsLoaded >= personajePaloma.length){
      document.getElementById("btn-load").style.display = "none";
    }
   
}, [itemsLoaded]);

  let descripcionPersonaje = ""
  let imagenPersonaje = ""
  let nombrePersonaje = ""

        personajePaloma.map((item) => { 
          descripcionPersonaje = item._source.descripcion_corta_personaje[0]
          imagenPersonaje = item._source.imagen_personaje[0]
          nombrePersonaje = item._source.nombre_personaje[0]
        })

  return (
    <>
      <Head>
        <title>Caustica - {nombrePersonaje}</title>
        <meta name="viewport" content="width=device-width" />

        <meta name="robots" content="index,follow"/>

        <meta name="distribution" content="global" /> 
        <meta name="rating" content="general"/>
        <meta name="description" content=""/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name="author" content="Caustica"/>
        <meta name="genre" content="News"/>
        <meta name="geo.placename" content="Colombia"/>
        <meta name="geo.region" content="CO"/>
        <meta name="language" content="spanish"/>

        <link rel="canonical" href="https://www.caustica.co/paloma/"/>

        <meta property="og:type" content="website" />
        <meta property="og:title" content="{varTitulo}" />
        <meta property="og:description" content="{varDescripcion}" />
        <meta property="og:url" content="https://www.caustica.co/paloma/" />
        <meta property="og:image" content="../../static/assets/1200x630-facebook.png" />
        <meta property="og:site_name" content="Caustica" />
        <meta property="fb:admins" content="723352678331419" />
        <meta property="og:locale" content="es_CO" />
        <meta property="og:locale:alternate" content="es_CO" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@CausticaCo" />
        <meta name="twitter:creator" content="@CausticaCo" />
        <meta name="twitter:title" content="{varTitulo}" />
        <meta name="twitter:description" content="{CausticaCo}" />
        <meta name="twitter:image" content="../../static/assets/280x150-twitter.png" />
      </Head>
     
      <div className="character-menu-banner">
        <div className='content'>

            <nav className="main-breadcrumb">
                <ol className="breadcrumb">
                          <li className="breadcrumb-item ">
                                  <a href="/">Home</a>
                              </li>
                          <li className="breadcrumb-item active">
                                  {nombrePersonaje}
                              </li>
                      </ol>
              </nav>

              <div className="character">
                  <img
                      src={serverURL+imagenPersonaje.replace("default", "caustica").replace("html-caustica", "")}
                      alt={nombrePersonaje}
                      title={nombrePersonaje}
                  />
              </div>

              <blockquote>
                <h1>{nombrePersonaje}</h1>
                <p>{descripcionPersonaje}</p>
              </blockquote>

          </div>

          <div className='background'>
                    <picture>
                          <source 
                            srcSet="/static/assets/back-characters.png" 
                            media="(max-width: 1024px)" />
                          <img
                            src="/static/assets/back-characters.png"
                            alt="banner"
                            title="banner"
                      />
                      </picture>
                  </div>

      </div>
      <div className="search-results"> 
          <div className="search-selects">        
              <FilterSelect filterValues={filterValues1} filterName ='Categorias' id='0'/>         
              <FilterSelect filterValues={filterValues3} filterName ='Sentidos' id='1'/>
              <button className="btnFiltros" onClick={handleClick} >Aplicar Filtros</button>
              <input 
                type="text" 
                id="second-search"
                className="intern-search-input"                
                placeholder="BUSCAR"
                onKeyPress={handleKeypress}
                onChange={handleChange}
                ref={searchInput}
                style={{background:"url(/static/assets/search-intern-icon.png) no-repeat right"}}                                
              />
              <div className="intern-search-click" onClick={handleClick}/>
          </div>
      </div>
      <div className="filter-board-container">
          <FilterBoard/>
          <button className="clean-filters-button" onClick={handleClick} hidden={cleanFiltersShow  ?  false : true}>Limpiar filtros</button>
      </div>
      

      <div className="main-masonry-sentidos">
          <ResponsiveMasonry
            columnsCountBreakPoints={{ 576: 1, 768: 2, 1500: 3 }}
          >
            <Masonry className="masonry-content-dinamicas" style={{ padding: "90px" }}>
              {
                cleanFiltersShow === false ? (
                  personajePaloma.length > 0 ? (
                    personajePaloma.map((item, q) => {
                      if(q <= itemsLoaded) {
                      return <InternalPagesItemGrid key={item._id} {...item} />;
                      }
                    })
                  ) : (  <p>No se encontraron resultados para tu búsqueda</p> )
                ) : 
                searchResult.length > 0 ? (
                  searchResult.map((item, q) => {
                    if(q <= itemsLoaded) {
                      return <InternalPagesItemGrid key={item._id} {...item} />;
                    }
                    })
                ) : (  <p>No se encontraron resultados para tu búsqueda</p> )
              }
            </Masonry>
          </ResponsiveMasonry>
          <FloatinButton/>
          <div id="btn-load" className="home-grid-loadMore" onClick={handleClick} >Cargar más</div>
      </div>
      
    </>
  );
};

export async function getServerSideProps() {
  let personajePaloma=""
  let params = new URLSearchParams()
      params.set("id_personaje", "38")
   await axios.get(process.env.BASE_SERVER + '/api/elastic/', {params})
   .then((res) => {
      try{
          personajePaloma = JSON.parse(JSON.stringify(res.data.body.hits.hits))        
        }catch(error){
          //console.log(error)
        }
    }).catch(err => {
        // console.log(err)                              
    });
    return {
      props: {personajePaloma}, 
    }
   
}

export default menuSections;