import Head from "next/head";
import BurguerMenu from "../components/BurguerMenu";
import Link from "next/link";
import FloatinButton from "../components/FloatinButton";


/**
 * Componente funcional encargado de mostrar la pagina de contactenos
**/

const MapaSitio = () => {
  return (
    <>
      <Head>
        <title>Caustica - Mapa del sitio</title>
        <meta name="viewport" content="width=device-width" />

        <meta name="robots" content="index,follow"/>

        <meta name="distribution" content="global" /> 
        <meta name="rating" content="general"/>
        <meta name="description" content=""/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name="author" content="Caustica"/>
        <meta name="genre" content="News"/>
        <meta name="geo.placename" content="Colombia"/>
        <meta name="geo.region" content="CO"/>
        <meta name="language" content="spanish"/>

        <link rel="canonical" href="https://www.caustica.co/mapa-sitio/"/>

        <meta property="og:type" content="website" />
        <meta property="og:title" content="{varTitulo}" />
        <meta property="og:description" content="{varDescripcion}" />
        <meta property="og:url" content="https://www.caustica.co/mapa-sitio/" />
        <meta property="og:image" content="../../static/assets/1200x630-facebook.png" />
        <meta property="og:site_name" content="Caustica" />
        <meta property="fb:admins" content="723352678331419" />
        <meta property="og:locale" content="es_CO" />
        <meta property="og:locale:alternate" content="es_CO" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@CausticaCo" />
        <meta name="twitter:creator" content="@CausticaCo" />
        <meta name="twitter:title" content="{varTitulo}" />
        <meta name="twitter:description" content="{CausticaCo}" />
        <meta name="twitter:image" content="../../static/assets/280x150-twitter.png" />
      </Head>
      
      
      <div className="mapa-sitio">
        <div className="content">
        <h1>Mapa del sitio</h1>
          <div className="secciones">
            <h2>Secciones</h2>
              <ul>
                <h3><Link href="/actualidad/"><a>Actualidad</a></Link></h3>
                <li><Link href="/actualidad/politica/"><a>Política</a></Link></li>
                <li><Link href="/actualidad/economia/"><a>Economía</a></Link></li>
                <li><Link href="/actualidad/educacion/"><a>Educación</a></Link></li>
                <li><Link href="/actualidad/tecnologia/"><a>Tecnología</a></Link></li>
              </ul>

              <ul>
                <h3><Link href="/bienestar/"><a>Bienestar</a></Link></h3>
                <li><Link href="/bienestar/salud-mental/"><a>Salud mental</a></Link></li>
                <li><Link href="/bienestar/cambio-de-habitos/"><a>Cambio de hábitos</a></Link></li>
                <li><Link href="/bienestar/sexo/"><a>Sexo</a></Link></li>
              </ul>

              <ul>
                <h3><Link target="_blank" href="/entorno/"><a>Entorno</a></Link></h3>
                <li><Link href="/entorno/sociedades/"><a>Sociedades</a></Link></li>
                <li><Link href="/entorno/activismo/"><a>Activismo</a></Link></li>
                <li><Link href="/entorno/genero/"><a>Género</a></Link></li>
                <li><Link href="/entorno/medio-ambiente/"><a>Medio ambiente</a></Link></li>
              </ul>

              <ul>
                <h3><Link target="_blank" href="/cultura/"><a>Cultura</a></Link></h3>
                <li><Link href="/cultura/arte-y-musica/"><a>Arte y música</a></Link></li>
                <li><Link href="/cultura/peliculas-series-y-libros/"><a>Películas, series y libros</a></Link></li>
                <li><Link href="/cultura/viajes-y-gastronomia/"><a>Viajes y gastronomía</a></Link></li>
                <li><Link href="/cultura/gamers/"><a>Gamers</a></Link></li>
              </ul>

          </div>
        </div>
 
        <div className="content">
          <div className="otrasecciones">
            <h2>Otras secciones</h2>
              <ul>
                <li><Link href="/aviso-de-privacidad/"><a>Aviso de privacidad</a></Link></li>
                <li><Link href="/terminos-y-condiciones/"><a>Términos y condiciones</a></Link></li>
                <li><Link href="https://www.eltiempo.com/legal/POLITICA_DE_TRATAMIENTO_Y_PROCEDIMIENTOS_EN_MATERIA_DE_PROTECCION_DE_DATOS_PERSONALES.pdf">
                      <a target="_blank">
                      Política de datos personales
                      </a>
                    </Link>
                </li>  
                <li><Link href="/politica-de-datos-de-navegacion/"><a>Política de datos de navegación</a></Link></li>    
                <li><Link href="/mapa-sitio/"><a>Mapa del sitio</a></Link></li>
                <li><Link href="/contactenos/"><a>Contáctenos</a></Link></li>
              </ul> 
          </div>

          <div className="sentidos">
            <h2>Sentidos</h2>
              <ul>
                <li><Link href="/ver/"><a>Ver</a></Link></li>
                <li><Link href="/leer/"><a>Leer</a></Link></li>
                <li><Link href="/oir/"><a>Oir</a></Link></li>
                <li><Link href="/random/"><a>Random</a></Link></li>
              </ul> 
          </div>

          <div className="personajes">
            <h2>Personajes</h2>
              <ul>
                <li><Link href="/paloma/"><a>Paloma</a></Link></li>
                <li><Link href="/semaforo/"><a>Semaforo</a></Link></li>
                <li><Link href="/rata/"><a>Rata</a></Link></li>
                <li><Link href="/payaso/"><a>Payaso</a></Link></li>
              </ul> 
          </div>
        </div>
      </div>
      <FloatinButton/>
    </>
  );
};

export default MapaSitio;