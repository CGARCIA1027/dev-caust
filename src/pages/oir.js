import Head from "next/head";
import React, {useState} from "react";
import HomeItemGrid from '../components/HomeItemGrid'
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import InternalPagesItemGrid from '../components/InternalPagesItemGrid';
import { gtmMasonryCategoriasCargarMas } from "../components/Gtm";
import axios from "axios";
import FloatinButton from "../components/FloatinButton";

/**
 * Componente funcional encargado de mostrar la pagina de secckiones del menu
 *
*/

const menuSections = ({oirItems}) => { 

  const [itemsLoaded, setitemsLoaded] = useState(11)
  const handleClick = (e) =>{
    e.preventDefault();
    setitemsLoaded(itemsLoaded + 6)
  }  

  return (
    <>
      <Head>
        <title>Caustica - Oir</title>
        <meta name="viewport" content="width=device-width" />

        <meta name="robots" content="index,follow"/>

        <meta name="distribution" content="global" /> 
        <meta name="rating" content="general"/>
        <meta name="description" content=""/>
        <meta name="format-detection" content="telephone=no"/>
        <meta name="author" content="Caustica"/>
        <meta name="genre" content="News"/>
        <meta name="geo.placename" content="Colombia"/>
        <meta name="geo.region" content="CO"/>
        <meta name="language" content="spanish"/>

        <link rel="canonical" href="https://www.caustica.co/oir/"/>

        <meta property="og:type" content="website" />
        <meta property="og:title" content="{varTitulo}" />
        <meta property="og:description" content="{varDescripcion}" />
        <meta property="og:url" content="https://www.caustica.co/oir/" />
        <meta property="og:image" content="../../static/assets/1200x630-facebook.png" />
        <meta property="og:site_name" content="Caustica" />
        <meta property="fb:admins" content="723352678331419" />
        <meta property="og:locale" content="es_CO" />
        <meta property="og:locale:alternate" content="es_CO" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@CausticaCo" />
        <meta name="twitter:creator" content="@CausticaCo" />
        <meta name="twitter:title" content="{varTitulo}" />
        <meta name="twitter:description" content="{CausticaCo}" />
        <meta name="twitter:image" content="../../static/assets/280x150-twitter.png" />
      </Head>

      <div className="sentidos-menu-banner">
        <div className='content'>

            <nav className="main-breadcrumb">
                <ol className="breadcrumb">
                          <li className="breadcrumb-item ">
                                  <a href="/">Home</a>
                              </li>
                          <li className="breadcrumb-item active">
                                  Oir
                              </li>
                      </ol>
              </nav>

              <div className="item">
                  <img
                      src="/static/assets/oir.png"
                      alt="oir"
                      title="oir"
                  />
              </div>

          </div>

          <div className='background-color' style={{ backgroundColor: '#000000' }}></div>

          <div className='background'>
                    <picture>
                          <source 
                            srcSet="/static/assets/back-banner-sentidos.jpg" 
                            media="(max-width: 1024px)" />
                          <img
                            src="/static/assets/back-banner-sentidos.jpg"
                            alt="banner"
                            title="banner"
                      />
                      </picture>
                  </div>

      </div>
      <div className="main-masonry-sentidos">
      <ResponsiveMasonry
        columnsCountBreakPoints={{ 576: 1, 768: 2, 1500: 3 }}
      >
        <Masonry className="masonry-content-dinamicas" style={{ padding: "90px" }}>
          {oirItems.length > 0 ? (
            oirItems.map((item) => {
              return <InternalPagesItemGrid key={item._id} {...item} />;
            })
          ) : (
            <p>No se encontraron resultados para tu búsqueda</p>
          )}
        </Masonry>
      </ResponsiveMasonry>
      {oirItems.length > 0 ? (
         <div className="home-grid-loadMore" onClick={handleClick} />
      ):  null
      }
      <FloatinButton/>
      </div>
    </>
  );
};

export async function getServerSideProps() {
  let oirItems
  let params = new URLSearchParams()
      params.set("name", "Oir")
    await axios.get(process.env.BASE_SERVER + '/api/elastic/', {params}).then((res) => {
      try{
          oirItems = JSON.parse(JSON.stringify(res.data.body.hits.hits))      
        }catch(error){
          console.log(error)
        }      
    });
  
    
  return {
    props: {oirItems}, 
  }
}

export default menuSections;