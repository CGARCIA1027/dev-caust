import Head from "next/head";
import BannerArticulos from "../../components/BannerArticulos";
import SliderArticulos from "../../components/SliderArticulos";
import InfoImage from "../../components/InfoImage";
import InfoImageSmall from "../../components/InfoImageSmall";
import InfoAdd from "../../components/InfoAdd";
import BlockQuote from "../../components/Blockquote";
import GaleryX3 from "../../components/GaleryX3";
import BurguerMenu from "../../components/BurguerMenu";
import React, {useState,useEffect} from "react";

import HomeItemGrid from '../../components/HomeItemGrid'
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import {useRouter} from "next/router"
import axios from "axios";

/**
 * Componente funcional encargado de mostrar el detalle de los articulos *
*/ 

const DetalleArticulo = (detalleArticulo) => {
  const [itemCategoria, setItemCategoria] = useState([]);
  const [DataDinamica, setDataDinamica] = useState([]);
  const router = useRouter()
  const articulo = router.query.articulo

  useEffect( async() => {
    if (!router.isReady) return;

        var hayScrollLoad = document.querySelector("body")
        if (hayScrollLoad.classList.contains("scroll")) {
           try{
            document.querySelector("body").classList.remove("scroll");
           }catch(error){
            //  console.log(error)
           }
         }
    
        
    const paramId = articulo[1].toString()
    let params = new URLSearchParams();
    params.set("nid", paramId);
    const newDetail =     
    await axios
      .get("/api/elastic", { params })
      .then((res) => {
        setDataDinamica(res.data.body.hits.hits)

        let subCategoria = res.data.body.hits.hits[0]._source.id_subcategoria
        const dataCategoria = encodeURI(process.env.URL_DOMAIN_ELTIEMPO+"q=id_subcategoria:"+subCategoria)
        fetch(dataCategoria)
            .then(res => res.json())
            .then(
              (result) => {
                setItemCategoria(result.hits.hits);
              },       
              (error) => {
                // console.log(error)
              }
            )
      })

      .catch((err) => {
        // console.error(err);
      });

  }, [router.isReady])

  return (
    <>
      <Head>
        <title>Cáustica - Articulos</title>
        <meta name="description" content="Lorem ipsum dolor sit amet" />
        <meta name="viewport" content="width=device-width" />
      </Head>
      
      {DataDinamica.length > 0 ? <BannerArticulos detalleBannerArticulo={DataDinamica} /> : null}

      <div className='content-max acticulos'>
        <div className='content'>
          <article>
           {DataDinamica.length > 0 ? <InfoImage detalleInfoImage={DataDinamica} /> : null}
           {DataDinamica.length > 0 ? <InfoAdd detalleInfoAdd={DataDinamica} /> : null}
           {DataDinamica.length > 0 ? <BlockQuote citaArticulo={DataDinamica} /> : null}

           {DataDinamica.length > 0 ? <InfoImageSmall detalleInfoImageSmall={DataDinamica} /> : null}

            <aside className='publicidad pauta_970x90 '>
               <img
                    src="/static/assets/add-970x90.png"
                />
              </aside>

              {DataDinamica.length > 0 ? <GaleryX3 articuloGaleryx3={DataDinamica} /> : null}
          </article>
          
        </div>
      </div>
      {DataDinamica.length > 0 ? <SliderArticulos detalleSliderArticulos={DataDinamica} /> : null}

      <div className="puede-interesar">
        <h2>Te puede interesar</h2>
      <ResponsiveMasonry
        columnsCountBreakPoints={{ 576: 1, 768: 2, 1200: 3 }}
      >
        <Masonry className="masonry-content-dinamicas" style={{ padding: "90px" }}>
          {itemCategoria.length > 0 ? (
            itemCategoria.map((item) => {               
               return <HomeItemGrid key={item._id} {...item} tipoRelaciondo="relacionado" />;
            })
          ) : (
            <p>No se encontraron resultados para tu búsqueda</p>
          )}
        </Masonry>
      </ResponsiveMasonry>
      </div>
    </>
  );
};


export default DetalleArticulo;
