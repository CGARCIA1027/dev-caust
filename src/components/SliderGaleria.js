import Link from "next/link";
import Carousel, { consts } from "react-elastic-carousel";
import arrowLeft from "../../public/static/assets/arrow-left.png";
import arrowRigth from "../../public/static/assets/arrow-right.png";
import { gtmDowloadDetalle } from "../components/Gtm";
import { LazyLoadImage } from "react-lazy-load-image-component";
import React, {useState,useEffect, useRef } from "react";
/**
 * Componente funcional encargado de mostrar el banner de articulos
*/

const myArrow = ({ type, onClick, isEdge }) => {
  const pointer = type === consts.PREV ? arrowLeft : arrowRigth;
  return (
    <button onClick={onClick} disabled={isEdge} className="carousel-button">
      <img src={pointer} />
    </button>
  );
};

const total = function(pages) { 
  return (
    pages.length
  )
}

const myPagination = ({pages, activePage, onClick}) => {
  return (

    <div className="content-dots">
      <span>0{activePage+1}</span>
        <div className="dots">
            {pages.map(page => {
              const isActivePage = activePage === page
              return (
                <div
                  className={isActivePage === true ? "dot active" : "dot"}
                  key={page}
                  onClick={() => onClick(page)}
                  active={isActivePage}
                >
                </div>
              )
            })}
        </div>
      <span>0{total(pages)}</span>
  </div>
  )
}

const SliderGaleria = (props) => {
const urlBase = process.env.URL_ELTIEMPO_IMG
const [descripcionGaleria, setdescripcionGaleria] = useState([]);
const [imagenGaleriaDesktop, setimagenGaleriaDesktop] = useState([]);
const [imagenGaleriaMobile, setimagenGaleriaMobile] = useState([]);
const [imagenGaleriaAlt, setimagenGaleriaAlt] = useState([]);
const [imagenGaleriaTitle, setimagenGaleriaTitle] = useState([]);

var error = false
useEffect(() => { 
  try {
    setdescripcionGaleria (props.detalleGaleriaArticulo[0]._source.descripcion_galeria_interna)
    setimagenGaleriaDesktop (props.detalleGaleriaArticulo[0]._source.banner_desktop_galeria)
    setimagenGaleriaMobile (props.detalleGaleriaArticulo[0]._source.banner_mobile_galeria)
    setimagenGaleriaAlt (props.detalleGaleriaArticulo[0]._source.alt_desktop_galeria)
    setimagenGaleriaTitle (props.detalleGaleriaArticulo[0]._source.title_desktop_galeria)
  }catch(error){
    // console.log(error)
    error = true
  }
},[props])

let datosGaleria = []
const creationDate = new Date(props.detalleGaleriaArticulo[0]._source.fecha_creacion*1000)

for(const id in descripcionGaleria){
  datosGaleria.push({descripcion: descripcionGaleria[id], link: imagenGaleriaDesktop[id], linkMobile: imagenGaleriaMobile[id], alt: imagenGaleriaAlt[id], title: imagenGaleriaTitle[id]});
}

 if(error === false){
  return (
    <div className='SliderArticulos'>
      {/* <h1>slider galeria</h1> */}
        <div className='content-max'>

        <Carousel
              // enableAutoPlay={true}
              // autoPlaySpeed={3500}
              focusOnSelect={true}
              enableMouseSwipe={false}
              renderArrow={myArrow}
              renderPagination={myPagination}
              className="slider"
              
            >

            {datosGaleria.map((item, p) => {                               
                  return (
                    <div key={p} className='item'>      
                      <picture>
                        <source 
                            srcSet={urlBase + item.linkMobile.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                            media="(max-width: 1024px)" />
                            <LazyLoadImage
                              src={urlBase + item.link.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                              alt={item.alt}
                              title={item.title}
                        />
                    </picture>

                        <div className='info'>
                          <p>{item.descripcion}</p>
                          <a 
                          download="" 
                          href={urlBase + item.link.toString().replace("/sites","").replace("/caustica","").replace("/files","")} 
                          className='down-load'
                          onClick={() => {
                            gtmDowloadDetalle(
                              props.detalleGaleriaArticulo[0]._source.title[0],
                              props.detalleGaleriaArticulo[0]._source.nombre_categoria[0],
                              props.detalleGaleriaArticulo[0]._source.subcategoria[0],
                              props.detalleGaleriaArticulo[0]._source.name[0],
                              props.detalleGaleriaArticulo[0]._source.nombre_personaje[0],
                              props.detalleGaleriaArticulo[0]._source.tipo[0] === "Informativo" ? "Articulo" : props.detalleGaleriaArticulo[0]._source.tipo[0],
                              p+1,
                              props.detalleGaleriaArticulo[0]._source.nid[0]
                              )
                          }}
                          ></a>
                        </div>
                      </div>  
                  );
                    
                })
            }

          </Carousel>
              
        </div>
    </div>
  );
 }else {
   return (
     <></>
   )
 }
 
};

export default SliderGaleria;
