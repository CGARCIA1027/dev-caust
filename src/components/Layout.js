import Header from "./Header"
import Footer from "./Footer"

/**
 * Componente principal estructura
 * @author Alejandro Galvis
 */

 const handleClick = (e) =>{        
  if(e.currentTarget.className=='close-add-header'){
   document.querySelector(".publicidad.pauta_header").classList.add("Up");
   try{
    document.querySelector("nav.menu-sections").classList.add("UpPautaHeader");
   }catch(error){
     //console.log(error)
   }
   
  }
  if(e.currentTarget.className=='close-add-footer'){
    document.querySelector(".publicidad.pauta_970x90.footer").classList.add("Down");
  }

  if(e.currentTarget.className=='aceptar-cookies'){
    document.querySelector(".content-add-footer .content-cookies").classList.add("Down");
    window.localStorage.setItem('cookies', 'true')
  }
 }
 const Layout = ({children}) => {
  try{
    if(window.localStorage.getItem('cookies')){
        document.querySelector(".content-add-footer .content-cookies").classList.add("Down");
    }
 }catch(error){
  //  console.log(error)
 }

  return (
    <>
    <aside className='publicidad pauta_header'>
      <div className="content-add">
        <div className="close-add-header" onClick={handleClick}>x</div>
          <img
            src="/static/assets/add-970x250.jpg"
          />
      </div>
    </aside>
    <Header/>
        {children}
        <div className="content-add-footer">
          <aside className='publicidad pauta_970x90 footer'> 
            <div className="content-add">
                <div className="close-add-footer" onClick={handleClick}>x</div>
                  <img
                    src="/static/assets/add-970x90.png"
                  />
            </div>
          </aside>
          <div className="content-cookies">
            <p>En este portal utilizamos "cookies" propias y de terceros para gestionar el portal, elaborar información estadística, optimizar la funcionalidad del sitio y mostrar publicidad relacionada con preferencias a través del análisis de la navegación. Puede conocer como deshabilitarlas u obtener más información <a href="/politica-de-datos-de-navegacion/">aquí</a></p>
            <div className="aceptar-cookies" onClick={handleClick}></div>
          </div>
        </div>
    <Footer/>
    </>
  );
   
 };

 export default Layout;





