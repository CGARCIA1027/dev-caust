import Link from "next/link";
import { gtmBtnPerfilHeader } from "../components/Gtm";

const LoginButton = () => {
    return (
        <>
            <Link href="/iniciar-sesion">
                    <a title="home">                    
                        <i 
                        className='login' 
                        style={{display: 'inline-block'}}
                        onClick={() => {
                            gtmBtnPerfilHeader()
                          }}
                        >
                            <img
                                src="/static/assets/ico-profile.svg"
                                alt="Login"
                                title="Login"
                                className="img-ico login"
                            />                          
                        </i>                      
                    </a>
            </Link>  
        </>
    )
}
export default LoginButton;