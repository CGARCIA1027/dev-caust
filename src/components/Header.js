import React, { useEffect, useRef } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { gtmSocialNetworkHeader, gtmLinksHeader, gtmSubseccionesHeader, gtmBtnBuscadorHeader, gtmBuscador } from "../components/Gtm";
import CheckLogin from "./CheckLogin";
import { Navegacion } from "./Header/Navegacion";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { useState } from "react";
import BurguerMenu from '../components/BurguerMenu';


var document = require("global/document");
/**
 * Componente funcional encargado de mostrar el header
 **/

const Header = () => {
  const router = useRouter();
  const [searchParams , setSearchParams] = useState('')
  let dataNavegacion = Navegacion;
  
  let showSearch = false;
  //Function scroll
  const prevScrollY = useRef(0);
  let searchInput = useRef('');

  const handleChange = async (e) =>{    
    setSearchParams(searchInput.current.value);
  } 

  const handleKeypress = (e) => {
    let path = window.location.pathname       
    if (e.charCode === 13 ) { 
      if(searchParams == ''){

      }else{
        e.preventDefault();    
        window.localStorage.setItem('search', JSON.stringify(searchParams))
        window.localStorage.removeItem('filters0'+path)
        window.localStorage.removeItem('filters1'+path)
        window.localStorage.removeItem('filters2'+path)
        window.location.assign('/busqueda')
        gtmBuscador(searchInput.current.value)
      }
    }
  }
  const handleClick = () =>{
    if(searchParams !== ''){
      window.localStorage.setItem('search', JSON.stringify(searchParams))
      window.location.assign('/busqueda')
      gtmBuscador(searchInput.current.value)
      
    }else{

    }

  }  
  useEffect(() => {
    const handleScroll = () => {
      const currentScrollY = window.scrollY;
      if (prevScrollY.current > 250) {
        document.querySelector("body").classList.add("scroll");
      } else {
        document.querySelector("body").classList.remove("scroll");
      }
      prevScrollY.current = currentScrollY;
    };
    window.addEventListener("scroll", handleScroll, { passive: true });
    if(window.location.pathname == '/busqueda/'){
      showSearch = true;
      document.getElementsByClassName("search-box")[0].style.display = "block";
      document.getElementsByClassName("search-box")[0].style.position = "absolute";
      document.querySelector(".user-action").classList.add("open");
      let keepValue = window.localStorage.getItem('search');
      document.querySelector(".search-input").value = keepValue.replace(/['"]+/g, '');
    } else {
      
    }
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return (
    <>
      <header>
        <div className="top-header">
          <div className="content-int">
            <div className="social">
              <Link href="https://www.facebook.com/caustica.col/">
                <a 
                className="fb" 
                target="_blank"
                onClick={() => {
                  gtmSocialNetworkHeader("Facebook")
                }}
                >
                  <LazyLoadImage
                    src="/static/assets/ico-fb.png"
                    alt="Facebook"
                    title="Facebook"
                    className="img-ico fb"
                  />
                </a>
              </Link>
              <Link href="https://twitter.com/CausticaCo/">
                <a 
                className="tw" 
                target="_blank"
                onClick={() => {
                  gtmSocialNetworkHeader("Twitter")
                }}
                >
                  <LazyLoadImage
                    src="/static/assets/ico-tw.png"
                    alt="Twitter"
                    title="Twitter"
                    className="img-ico tw"
                  />
                </a>
              </Link>
              <Link href="https://www.instagram.com/caustica.co/">
                <a 
                className="ig" 
                target="_blank"
                onClick={() => {
                  gtmSocialNetworkHeader("Instagram")
                }}
                >
                  <LazyLoadImage
                    src="/static/assets/ico-ig.png"
                    alt="Instagram"
                    title="Instagram"
                    className="img-ico ig"
                  />
                </a>
              </Link>
              <Link href="https://www.tiktok.com/@caustica?">
                <a 
                className="ig" 
                target="_blank"
                onClick={() => {
                  gtmSocialNetworkHeader("Tiktok")
                }}
                >
                  <LazyLoadImage
                    src="/static/assets/ico-tt.png"
                    alt="TikTok"
                    title="TikTok"
                    className="img-ico tt"
                  />
                </a>
              </Link>
            </div>
            <div className="user-action">
              <i className="search">
                <LazyLoadImage
                  src="/static/assets/ico-search.svg"
                  alt="Buscador"
                  title="Buscador"
                  className="img-ico search"
                  onClick={() => {
                    if (
                      document.getElementsByClassName("search-box")[0].style
                        .display == "none" ||
                      document.getElementsByClassName("search-box")[0].style
                        .display == ""
                    ) {
                      showSearch = true;
                      document.getElementsByClassName(
                        "search-box"
                      )[0].style.display = "block";
                      document.getElementsByClassName(
                        "search-box"
                      )[0].style.position = "absolute";
                      document.querySelector(".user-action").classList.add("open");
                      gtmBtnBuscadorHeader()
                    } else {
                      showSearch = false;
                      document.getElementsByClassName(
                        "search-box"
                      )[0].style.display = "none";
                      document.querySelector(".user-action").classList.remove("open");
                    }
                  }}
                />
              </i>

              <CheckLogin />
            </div>
          </div>
        </div>
        <BurguerMenu/>          
        <div className="main-brand"> 
          <Link href="/">
            <a className="caustica">
              <LazyLoadImage
                src="/static/assets/brand-caustica.svg"
                alt="Cáustica"
                title="Cáustica"
                onClick={() => {
                  document.getElementsByClassName(
                    "search-box"
                  )[0].style.display = "none";
                  document.querySelector(".user-action").classList.remove("open");
                }}
              />
            </a>
          </Link>
        </div>

        <nav className="menu">
          <ul>
            {dataNavegacion.length > 0
              ? dataNavegacion.map((item, index) => {
                  return index == 2 ? (
                    <div key={index} className="contentMenuitem">
                      <li className="brand"></li>
                      <li
                        className={`menu-item ${
                          router.pathname === item.uri ? "active" : ""
                        }`}
                      >
                        <Link href={item.uri}>
                          <a
                          className="itemMenu"
                          onClick={() => {
                            gtmLinksHeader(item.title)
                          }}
                          >{item.title}</a>
                        </Link>
                        <ul className="actualidad-list subItems">
                          {item.below.map((element, h) => {
                            return (
                              <li key={h}>
                                <Link href={`${element.uri}`}>
                                  <a
                                  onClick={() => {
                                    gtmSubseccionesHeader(item.title.toLowerCase().replace(/\b[a-zA-Z]/g, (match) => match.toUpperCase()) +" - "+ element.title)
                                  }}
                                  >{element.title}</a>
                                </Link>
                              </li>
                            );
                          })}
                        </ul>
                      </li>
                    </div>
                  ) : (
                    <li
                      className={`menu-item ${
                        router.pathname === item.uri ? "active" : ""
                      }`}
                      key={index}
                    >
                      <Link href={item.uri}>
                        <a
                        className="itemMenu"
                         onClick={() => {
                          gtmLinksHeader(item.title)
                        }}
                        >{item.title}</a>
                      </Link>
                      <ul className="actualidad-list subItems">
                        {item.below.map((element, k) => {
                          return (
                            <li key={k}>
                              <Link href={`${element.uri}`}>
                                <a
                                 onClick={() => {
                                  gtmSubseccionesHeader(item.title.toLowerCase().replace(/\b[a-zA-Z]/g, (match) => match.toUpperCase()) +" - "+ element.title)
                                }}
                                >{element.title}</a>
                              </Link>
                            </li>
                          );
                        })}
                      </ul>
                    </li>
                  );
                })
              : null}
          </ul>
        </nav>
        <div className="search-box">
        <div className="search-background">
          <input
              className="search-input"
              type="text"
              placeholder="BUSCAR"
              onKeyPress={handleKeypress}
              onChange={handleChange}
              ref={searchInput}
          ></input>
          <a>
            <LazyLoadImage
              className="search-icon"
              src="/static/assets/ico-search.svg"
              onClick={handleClick}
            />
          </a>
        </div>
      </div>
      </header>
    
    </>
  );
};

export default Header;
