import { hidden } from 'min-document';
import React from 'react'
import { gtmFiltros, gtmFiltrosSentidos } from "../components/Gtm";

class FloatinButton extends React.Component {
    constructor(props) {
      super(props);
      this.state = {clicked: false};  
     
      this.handleClick = this.handleClick.bind(this);
    }
  
    handleClick() {
      this.setState(prevState => ({
        clicked: !prevState.clicked
      }));

      if(document.querySelector(".floatin-boxes-container").offsetLeft === 0){
        gtmFiltros()
      }

    }
  
    render() {
      return (
        <>
            <div className="floatin-button-container">
                <div className="floatin-button" onClick={this.handleClick}>                
                        <div className="floatin-boxes-container" hidden={this.state.clicked ?  false : true}>
                            <a href="/random" className="floatin-box" id="randomf" onClick={() => {gtmFiltrosSentidos("Random")}}></a>
                            <a href="/leer" className="floatin-box" id="leerf" onClick={() => {gtmFiltrosSentidos("Leer")}}></a>
                            <a href="/ver" className="floatin-box" id="verf" onClick={() => {gtmFiltrosSentidos("Ver")}}></a>
                            <a href="/oir" className="floatin-box" id="oirf" onClick={() => {gtmFiltrosSentidos("Oir")}}></a>
                        </div>   
                </div>
            </div>           
        </>
        
      );
    }
  }
  export default FloatinButton