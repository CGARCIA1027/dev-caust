import Link from "next/link";
import React, {useState, useEffect } from "react";
var document = require("global/document")
import { LazyLoadImage } from "react-lazy-load-image-component";

const SearchItemGrid = (searchResult) => {

  const [result, setResult ] = useState([])
  const [isLoaded, setIsLoaded] = useState(false)
 
  var serverURL = process.env.URL_ELTIEMPO_IMG;
  let imageResult, imageCharacter;

  useEffect(() => {    
    setResult(searchResult)     
    setIsLoaded(true)    
  }, [])

  try{      
      if(searchResult._source.tipo[0]=='Video' || searchResult._source.tipo[0]=='video' || searchResult._source.content_type[0] == "detalle_video"){
        imageResult =  'https://img.youtube.com/vi/' + searchResult._source.id_video_youtube[0] + '/sddefault.jpg';             
        imageCharacter = serverURL + searchResult._source.icono_personaje[0]
      }  
  }catch(error){  
    // console.log(error)
  } 
  const creationDate = new Date(searchResult._source.fecha_creacion[0]*1000) 
  var tipo = searchResult._source.content_type[0];

  if(isLoaded === true){
      if (tipo == "calendario") {
        return (
          <div className="content-card">
            <Link href={searchResult._source.enlace_calendario[0]}>
              <a href={searchResult._source.enlace_calendario[0]} target="_blank">
                <div
                  className="calendar-home-grid-item-under-elements"
                  style={{
                    backgroundImage: `url(${
                      serverURL +
                      searchResult._source.fondo_calendario[0]
                        // .replace("default", "caustica")
                        // .replace("html-caustica", "")
                    })`,
                  }}
                >
                  <div className="calendar-home-grid-item-classification">
                    <div className="calendar-home-grid-item-classification-text">
                      {searchResult._source.subcategoria[0]}
                    </div>
                    <div className="calendar-home-grid-item-banner-icon">
                      <LazyLoadImage
                        src={
                          serverURL +
                          searchResult._source.icono_tipo[0]
                            // .replace("default", "caustica")
                            // .replace("html-caustica", "")
                        }
                        className="item-image-banner-icon"
                      />
                    </div>
                  </div>
    
                  <h5 className="calendar-home-grid-item-title">
                    {searchResult._source.title[0]}
                  </h5>
    
                  <div className="calendar-home-grid-item-footer">
                    <h6 className="calendar-home-grid-item-date">
                      {creationDate.toLocaleDateString("es-ES")}
                    </h6>
                  </div>
                </div>
              </a>
            </Link>
          </div>
        );
      }else if (tipo == "detalle_video") { 
        let categoria = searchResult._source.nombre_categoria[0].toLowerCase().replace(",","").replaceAll(" ","-")
        let subcategoria = searchResult._source.subcategoria[0].toLowerCase().replace(",","").replaceAll(" ","-")      
        let urlAlias = ""
        if(searchResult._source.alias_url[0].includes('/node/')){
          urlAlias = "/node"
        }else {
          urlAlias = searchResult._source.alias_url[0].replace("/en","").replace("/detalle-articulo","") 
        }      
        return (
           <div className='search-content-card '>
            <Link href={categoria + "/" + subcategoria + urlAlias + "/"+ searchResult._source.nid[0]}>          
              <a href={categoria + "/" + subcategoria + urlAlias + "/"+ searchResult._source.nid[0]}>          
                        <div className="card-search-grid-item-banner">
                            <img
                                  src={imageResult}
                                  className="item-image-banner-search"                                          
                            /> 
                        </div>  
                        
                        <div className="search-grid-item-right-elements" style={{ boxShadow: '-6px 6px 0px 0px'+searchResult._source.field_color[0] }}>
                            <div className="search-grid-item-classification">
                              <div className="search-grid-item-classification-text" style={{ 'backgroundColor': searchResult._source.field_color[0] }}>{searchResult._source.nombre_categoria[0]}</div>
                            </div>                       
                              <h5 className="search-grid-item-title">{searchResult._source.title[0]}</h5>
                                              
                            <div className="search-grid-item-footer">
                              <h6 className="search-grid-item-date">{creationDate.toLocaleDateString("en-US")}</h6>
                              <div className="search-grid-item-footer-align">
                                  <h6 className="search-grid-item-class">{searchResult._source.tipo[0]}</h6>                    
                                  <img
                                      src={imageCharacter}
                                      className="search-grid-item-character"
                                      alt={searchResult._source.tipo} 
                                  />
                              </div>    
                            </div>   
                        </div>
              
              </a> 
            </Link>
            </div>
        );
      }else if(searchResult._source.nombre_categoria){       
        let categoria = searchResult._source.nombre_categoria[0].toLowerCase().replace(",","").replaceAll(" ","-")
        let subcategoria = searchResult._source.subcategoria[0].toLowerCase().replace(",","").replaceAll(" ","-")      
        let urlAlias = ""
        if(searchResult._source.alias_url[0].includes('/node/')){
          urlAlias = "/node"
        }else {
          urlAlias = searchResult._source.alias_url[0].replace("/en","").replace("/detalle-articulo","") 
        }
        return (
          <div className='search-content-card '>
            <Link href={categoria + "/" + subcategoria + urlAlias + "/"+ searchResult._source.nid[0]}>          
            <a href={categoria + "/" + subcategoria + urlAlias + "/"+ searchResult._source.nid[0]}>                   
                        <div className="card-search-grid-item-banner">
                            <img
                              src={
                                serverURL + searchResult._source.imagen_contenido_relacionado[0]
                                // .replace("/sites","").replace("/caustica","")
                                // .replace("/files","")
                                // .replace("default", "caustica")
                                // .replace("html-caustica", "")
                              }
                              className="item-image-banner-search"                                          
                            /> 
                        </div>  
                        
                        <div className="search-grid-item-right-elements" style={{ boxShadow: '-6px 6px 0px 0px'+searchResult._source.field_color[0] }}>
                            <div className="search-grid-item-classification">
                              <div className="search-grid-item-classification-text" style={{ 'backgroundColor': searchResult._source.field_color[0] }}>{searchResult._source.nombre_categoria[0]}</div>
                            </div>                       
                              <h5 className="search-grid-item-title">{searchResult._source.title[0]}</h5>
                                              
                            <div className="search-grid-item-footer">
                              <h6 className="search-grid-item-date">{creationDate.toLocaleDateString("en-US")}</h6>
                              <div className="search-grid-item-footer-align">
                                  <h6 className="search-grid-item-class">{searchResult._source.tipo[0]}</h6>                    
                                  <img
                                      src={
                                        serverURL +
                                        searchResult._source.icono_personaje[0]
                                        // .replace("/sites","").replace("/caustica","")
                                        // .replace("/files","")
                                        // .replace("default", "caustica")
                                        // .replace("html-caustica", "")
                                      }
                                      className="search-grid-item-character"
                                      alt={searchResult._source.tipo} 
                                  />
                              </div>    
                            </div>   
                        </div>
              
              </a> 
            </Link>
            </div>
          );
        }
      }else{
        return(null)
      }
  
}

export default SearchItemGrid;