import Link from "next/link";
import React, {useState,useEffect, useRef } from "react";
/**
 * Componente funcional encargado de mostrar el contenido de articulo copy y add
 */ 

const InfoAdd = (props) => {
  const [dataFull, setdataFull] = useState([]);

  useEffect(() => { 
    try {
      setdataFull(
        props.detalleInfoAdd[0]._source
      )
    }catch(error){
      // console.log(error)
    }
  },[props])

  if(dataFull.segundo_texto_articulo){
    return (
      <div className='content-info-ad'>
      <div className='info' dangerouslySetInnerHTML={{
                    __html: dataFull.segundo_texto_articulo,
                  }} />
      <aside className='publicidad pauta_300x250 '>
      <img
            src="/static/assets/add-300x250.png"
          />
      </aside>
    </div>
    );
    
  }else {
    return (
      <></>
    );
  }

};

export default InfoAdd;