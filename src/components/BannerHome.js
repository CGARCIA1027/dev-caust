import Link from "next/link";
import { gtmSentidosHome } from "../components/Gtm";
import ImageSlider from "./CarouselHeaderNew";

const BannerHome = ({promoteItems}) => {
  return (
    <div className='BannerHome'>
        <div className='content-max'>        
            <div className='content'>              
              <div className='info'>                
                  <div  className='banner-lefty'>                  
                    <Link href="/ver">   
                      <a 
                      className="ver"
                      onClick={() => {
                        gtmSentidosHome("Ver")
                      }}  
                      >         
                        </a>
                     </Link>
                     <Link href="/oir">
                        <a 
                        className="oir"
                        onClick={() => {
                          gtmSentidosHome("Oir")
                        }} 
                        >
                            <div
                              className="oir-oreja"
                          />
                        </a>
                      </Link>
                      <Link href="/leer">
                            <a
                              className="leer"
                              onClick={() => {
                                gtmSentidosHome("Leer")
                              }} 
                              >
                            </a>
                      </Link >
                      <Link href="/random">
                            <a
                              className="random"
                              onClick={() => {
                                gtmSentidosHome("Random")
                              }} 
                              >
                            </a>
                      </Link>
                    </div> 
                    
              </div>

              <div className='contentSlider'>                                 
                <ImageSlider promoteItems={promoteItems} />                              
              </div>
            </div>
        </div>
    </div>
  );
};

export default BannerHome;
