import { LazyLoadImage } from "react-lazy-load-image-component";
import React, {useState,useEffect, useRef } from "react";
/**
 * Componente funcional encargado de mostrar el banner de articulos
 */ 
const GaleryCopyAdd = (props) => {
const urlBase = process.env.URL_ELTIEMPO_IMG
const [dataFull, setdataFull] = useState([]);

useEffect(() => { 
  try {
    setdataFull(
      props.detalleVideoArticulo[0]._source
    )
  }catch(error){
    // console.log(error)
  }
},[props])

let nombreAutor = ""
if(dataFull.field_nombre_autor === undefined){
  nombreAutor = dataFull.autor_video
}else {
  nombreAutor = dataFull.field_nombre_autor
}

let imagenAutor = ""
if(dataFull.imagen_autor_articulo === undefined){
  imagenAutor = dataFull.url
}else {
  imagenAutor = dataFull.imagen_autor_articulo
}
imagenAutor = urlBase + imagenAutor

const creationDate = new Date(dataFull.fecha_creacion * 1000);
let iconoPersonaje = urlBase + dataFull.icono_personaje

  return (
    <div className='GaleryCopyAdd'>
            <div className='content'>
              <div className='info'>

                <div className='content-info'>
                  <div className='content-tags'>
                    <div className='tags'>
                    {dataFull.subcategoria}
                    </div>
                    <div className='ico-tag'>
                        <LazyLoadImage
                          src={iconoPersonaje.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                          alt={dataFull.nombre_personaje}
                          title={dataFull.nombre_personaje}
                        />
                    </div>
                  </div>

                  <div className='date'> {creationDate.toLocaleDateString("es-ES")}</div>
                  
                    <h1>{dataFull.title}</h1>
                         

                  <div className='autor'>
                    <div className='ico-autor'>
                        <LazyLoadImage
                          src={imagenAutor.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                          alt={nombreAutor}
                          title={nombreAutor}
                        />
                    </div>
                    <p>{nombreAutor}</p>

                    
                  </div>

                  <div className='description'  dangerouslySetInnerHTML={{
                  __html: dataFull.texto_largo_articulo,
                }} /> 
              
                </div>
                
              </div>

              <aside className='publicidad pauta_300x250 '>
                <img
                      src="/static/assets/add-300x250.png"
                    />
              </aside>
             
            </div>

    </div>
  );
};

export default GaleryCopyAdd;
