import React, {useState,useEffect, useRef } from "react";


const BlockQuote = (props) => {
  const [dataFull, setdataFull] = useState([]);

  useEffect(() => { 
    try {
      setdataFull(props.citaArticulo[0]._source)
    }catch(error){
      // console.log(error)
    }
  },[props])
  
  if(dataFull.cita_literaria){   
    return (
      <blockquote>
      <h3>{dataFull.cita_literaria[0]}</h3>
      <p>{dataFull.autor_cita[0]}</p>
    </blockquote>
    );
    
  }else {
    return (
      <></>
    );
  }


};

export default BlockQuote;