import Link from "next/link";
import Carousel, { consts } from "react-elastic-carousel";

import { gtmClickPersonajeHome, gtmClickVerMasPersonajeHome } from "../components/Gtm";
import React, { useState, useEffect } from 'react';
import axios from "axios";
var window = require("global/window")

const total = function(pages) { 
  return (
    pages.length
  )
}

const myPagination = ({pages, activePage, onClick}) => {
  return (

    <div className="content-dots">
      <span>0{activePage+1}</span>
        <div className="dots">
            {pages.map(page => {
              const isActivePage = activePage === page
              return (
                <div
                  className={isActivePage === true ? "dot active" : "dot"}
                  key={page}
                  onClick={() => onClick(page)}
                  active={isActivePage}
                >
                </div>
              )
            })}
        </div>
      <span>0{total(pages)}</span>
  </div>
  )
}

const Characters = ({charactersData}) => {  
  let urlBaseIMG = process.env.URL_ELTIEMPO_IMG
  
  let params = new URLSearchParams()
  const [itemsPersonajes, setItemsPersonajes] = useState([]);

  useEffect( async() => {    
    let characters = []
    let charactersIds = [38,39,40,41]
    for(var i = 0; i < charactersIds.length; i++) {
      params.set("id_personaje", charactersIds[i])
      await axios.get('/api/elastic/', {params}).then((res) => {
        try{
          characters.push(res.data.body.hits.hits[0])
        }catch(error){
          console.log(error)
        }
      });
    }

    setItemsPersonajes(characters)

  }, [])    

let descripcionPaloma = ""
let imagenPaloma = ""
let fotoPaloma = ""
let fotoPalomaMobile = ""
let nombrePaloma = ""

let descripcionRata = ""
let imagenRata = ""
let fotoRata = ""
let fotoRataMobile = ""
let nombreRata = ""

let descripcionPayaso = ""
let imagenPayaso = ""
let fotoPayaso = ""
let fotoPayasoMobile = ""
let nombrePayaso = ""

let descripcionSemaforo = ""
let imagenSemaforo = ""
let fotoSemaforo = ""
let fotoSemaforoMobile = ""
let nombreSemaforo = ""

itemsPersonajes.map((item, i) => { 
  //Paloma
  if(item._source.id_personaje == 38){
    descripcionPaloma = item._source.descripcion_corta_personaje[0]
    imagenPaloma = item._source.imagen_personaje[0]
    fotoPaloma = item._source.foto_personaje[0]
    fotoPalomaMobile = item._source.personaje_mobile[0]
    nombrePaloma = item._source.nombre_personaje[0]
  }

  //Rata
  if(item._source.id_personaje == 39){
    descripcionRata = item._source.descripcion_corta_personaje[0]
    imagenRata = item._source.imagen_personaje[0]
    fotoRata = item._source.foto_personaje[0]
    fotoRataMobile = item._source.personaje_mobile[0]
    nombreRata = item._source.nombre_personaje[0]
  }

  //Payaso
  if(item._source.id_personaje == 40){
    descripcionPayaso = item._source.descripcion_corta_personaje[0]
    imagenPayaso = item._source.imagen_personaje[0]
    fotoPayaso = item._source.foto_personaje[0]
    fotoPayasoMobile = item._source.personaje_mobile[0]
    nombrePayaso = item._source.nombre_personaje[0]
  }

  //Semaforo
  if(item._source.id_personaje == 41){
    descripcionSemaforo = item._source.descripcion_corta_personaje[0]
    imagenSemaforo = item._source.imagen_personaje[0]
    fotoSemaforo = item._source.foto_personaje[0]
    fotoSemaforoMobile = item._source.personaje_mobile[0]
    nombreSemaforo = item._source.nombre_personaje[0]
  }

})

  return (
    <>

        <Carousel
              autoPlaySpeed={3500}
              focusOnSelect={true}
              showArrows={false}
              enableMouseSwipe={false}
              renderPagination={myPagination}
              className="slider-personajes"
            >

        <div className="personajes-item">
              <img 
                src={urlBaseIMG+fotoPalomaMobile}
                alt={nombrePaloma}
                title={nombrePaloma}
              />
               <div className="caption-container">
                    <h3>{nombrePaloma}</h3>
                    <p>{descripcionPaloma}</p>
                    <Link href="/paloma/">
                      <a>Ver más</a>
                  </Link>
                </div>
        </div>
        <div className="personajes-item">
              <img 
                src={urlBaseIMG+fotoSemaforoMobile}
                alt={nombreSemaforo}
                title={nombreSemaforo}
              />
               <div className="caption-container">
                    <h3>{nombreSemaforo}</h3>
                    <p>{descripcionSemaforo}</p>
                    <Link href="/semaforo/">
                      <a>Ver más</a>
                  </Link>
                </div>
        </div>
        <div className="personajes-item">
              <img 
                src={urlBaseIMG+fotoRataMobile}
                alt={nombreRata}
                title={nombreRata}
              />
               <div className="caption-container">
                    <h3>{nombreRata}</h3>
                    <p>{descripcionRata}</p>
                    <Link href="/rata/">
                      <a>Ver más</a>
                  </Link>
                </div>
        </div>
        <div className="personajes-item">
              <img 
                src={urlBaseIMG+fotoPayasoMobile}
                alt={nombrePayaso}
                title={nombrePayaso}
              />
               <div className="caption-container">
                    <h3>{nombrePayaso}</h3>
                    <p>{descripcionPayaso}</p>
                    <Link href="/payaso/">
                      <a>Ver más</a>
                  </Link>
                </div>
        </div>
       
          </Carousel>

      <div className="personajes-content"
          
        
      >
          <div className="background-inicial"
            onMouseOver={() => ([ 
              document.getElementsByClassName("background-inicial")[0].classList.remove("inactive"),
            
            ])
            }
            onMouseOut={() => ([                                   
              document.getElementsByClassName("background-inicial")[0].classList.add("inactive"),
              
            ])
          } 
          >
            <img src="/static/assets/back-characters.jpg" />
            <Link href="/paloma/">
                <div className="za-paloma"
                    onClick={() => {
                      gtmClickPersonajeHome("Paloma")
                    }}
                    onMouseOver={() => ([ 
                        document.getElementsByClassName("paloma")[0].classList.add("active"),
                        document.getElementsByClassName("paloma-caption-container")[0].classList.add("active"),
                        document.getElementsByClassName("semaforo-caption-container")[0].classList.remove("active"),
                        document.getElementsByClassName("rata-caption-container")[0].classList.remove("active"),
                        document.getElementsByClassName("payaso-caption-container")[0].classList.remove("active")
                    ])
                      }
                        onMouseOut={() => ([                                   
                          document.getElementsByClassName("paloma")[0].classList.remove("active"),
                          // document.getElementsByClassName("paloma-caption-container")[0].classList.remove("active")
                        ])
                      }    
                >               
                </div>
            </Link> 
            <Link href="/semaforo/">
            <div className="za-semaforo"
             onClick={() => {
              gtmClickPersonajeHome("Semaforo")
            }}
                  onMouseOver={() => ([ 
                      document.getElementsByClassName("semaforo")[0].classList.add("active"),
                      document.getElementsByClassName("semaforo-caption-container")[0].classList.add("active"),
                      document.getElementsByClassName("paloma-caption-container")[0].classList.remove("active"),
                      document.getElementsByClassName("rata-caption-container")[0].classList.remove("active"),
                      document.getElementsByClassName("payaso-caption-container")[0].classList.remove("active")
                    ])
                    }
                    onMouseOut={() => ([                                   
                      document.getElementsByClassName("semaforo")[0].classList.remove("active"),
                      // document.getElementsByClassName("semaforo-caption-container")[0].classList.remove("active")
                    ])
                  }    
              >
            </div>
            </Link>
            <Link href="/rata/">
                <div className="za-rata"
                 onClick={() => {
                  gtmClickPersonajeHome("Rata")
                }}
                      onMouseOver={() => ([ 
                          document.getElementsByClassName("rata")[0].classList.add("active"),
                          document.getElementsByClassName("rata-caption-container")[0].classList.add("active"),
                          document.getElementsByClassName("semaforo-caption-container")[0].classList.remove("active"),
                          document.getElementsByClassName("paloma-caption-container")[0].classList.remove("active"),
                          document.getElementsByClassName("payaso-caption-container")[0].classList.remove("active")
                        ])
                        }
                        onMouseOut={() => ([                                   
                          document.getElementsByClassName("rata")[0].classList.remove("active"),
                          // document.getElementsByClassName("rata-caption-container")[0].classList.remove("active")
                        ])
                      }    
                  >
                </div>
            </Link>
            <Link href="/payaso/">
                <div className="za-payaso"
                 onClick={() => {
                  gtmClickPersonajeHome("Payaso")
                }}
                      onMouseOver={() => ([ 
                          document.getElementsByClassName("payaso")[0].classList.add("active"),
                          document.getElementsByClassName("payaso-caption-container")[0].classList.add("active"),
                          document.getElementsByClassName("semaforo-caption-container")[0].classList.remove("active"),
                          document.getElementsByClassName("paloma-caption-container")[0].classList.remove("active"),
                          document.getElementsByClassName("rata-caption-container")[0].classList.remove("active")
                        ])
                        }
                        onMouseOut={() => ([                                   
                          document.getElementsByClassName("payaso")[0].classList.remove("active"),
                          // document.getElementsByClassName("payaso-caption-container")[0].classList.remove("active")
                        ])
                      }    
                  >
                </div>
            </Link>
            <div className="paloma">
              <div className="back-paloma">
                <img 
                    src={urlBaseIMG+fotoPaloma}
                    alt={nombrePaloma}
                    title={nombrePaloma}
                  />
              </div>
            </div>

            <div className="paloma-caption-container">
                    <h3>{nombrePaloma}</h3>
                    <p>{descripcionPaloma}</p>
                    <Link href="/paloma/">
                      <a
                        onClick={() => {
                          gtmClickVerMasPersonajeHome("Paloma")
                        }}
                      >Ver más</a>
                  </Link>
                </div>

            <div className="semaforo">
              <div className="back-semaforo">
                <img 
                    src={urlBaseIMG+fotoSemaforo}
                    alt={nombreSemaforo}
                    title={nombreSemaforo}
                  />
              </div>
            </div>

            <div className="semaforo-caption-container">
                    <h3>{nombreSemaforo}</h3>
                    <p>{descripcionSemaforo}</p>
                    <Link href="/semaforo/">
                      <a
                        onClick={() => {
                          gtmClickVerMasPersonajeHome("Semaforo")
                        }}
                      >Ver más</a>
                  </Link>
                </div>

            <div className="rata">
              <div className="back-rata">
                <img 
                    src={urlBaseIMG+fotoRata}
                    alt={nombreRata}
                    title={nombreRata}
                  />
              </div>
            </div>

            <div className="rata-caption-container">
                    <h3>{nombreRata}</h3>
                    <p>{descripcionRata}</p>
                    <Link href="/rata/">
                      <a
                        onClick={() => {
                          gtmClickVerMasPersonajeHome("Rata")
                        }}
                      >Ver más</a>
                  </Link>
                </div>

            <div className="payaso">
              <div className="back-payaso">
                <img 
                    src={urlBaseIMG+fotoPayaso}
                    alt={nombrePayaso}
                    title={nombrePayaso}
                  />
              </div>
            </div>

            <div className="payaso-caption-container">
                    <h3>{nombrePayaso}</h3>
                    <p>{descripcionPayaso}</p>
                    <Link href="/payaso/">
                      <a
                        onClick={() => {
                          gtmClickVerMasPersonajeHome("Payaso")
                        }}
                      >Ver más</a>
                  </Link>
                </div>

          </div>
      </div>

    </>
  );
};


export default Characters;