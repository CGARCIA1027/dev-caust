import Link from "next/link";
import React, {useState,useEffect, useRef } from "react";
/**
 * Componente funcional encargado de mostrar el contenido de articulo galeria con 3 imagenes
 */

const GaleryX3 = (props) => {
  const urlBase = process.env.URL_ELTIEMPO_IMG
  const [dataFull, setdataFull] = useState([]);
  const [tres_imagenes_imagen_grande, settres_imagenes_imagen_grande] = useState([]);
  const [tres_imagenes_primera_imagen_p, settres_imagenes_primera_imagen_p] = useState([]);
  const [tres_imagenes_segunda_imagen_p, settres_imagenes_segunda_imagen_p] = useState([]);

  useEffect(() => { 
    try {
      setdataFull(props.articuloGaleryx3[0]._source)
      settres_imagenes_imagen_grande(props.articuloGaleryx3[0]._source.tres_imagenes_imagen_grande[0])
      settres_imagenes_primera_imagen_p(props.articuloGaleryx3[0]._source.tres_imagenes_primera_imagen_p[0])
      settres_imagenes_segunda_imagen_p(props.articuloGaleryx3[0]._source.tres_imagenes_segunda_imagen_p[0])
    }catch(error){
      // console.log(error)
    }
  },[props])

if(dataFull.tres_imagenes_descripcion){
  return (
    <div className='galery-x3'>
                <div className='info'>
                  <h2>{dataFull.tres_imagenes_titulo}</h2>
                  <p>{dataFull.tres_imagenes_descripcion}</p>
                </div>
                <div className='content-g3'>

                    <div className='content-images'>
                      <div className='image secondary'>
                          <img
                            src={urlBase + tres_imagenes_primera_imagen_p.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                            alt="Cáustica"
                            title="Cáustica"
                          />
                          <img
                            src={urlBase + tres_imagenes_segunda_imagen_p.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                            alt="Cáustica"
                            title="Cáustica"
                          />
                        </div>
                        <div className='image primary'>
                          <img
                            src={urlBase + tres_imagenes_imagen_grande.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                            alt="Cáustica"
                            title="Cáustica"
                          />
                        </div>  
                    </div>                  
                 
                </div>
              </div>
  );
  
}else {
  return (
    <></>
  );
}
  
};

export default GaleryX3;