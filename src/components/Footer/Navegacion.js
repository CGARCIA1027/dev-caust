/**
 * Función encargada de mostrar la seccion de Navegación del Footer
 */
export const ItemsFooter = () => {
  return {
    one: [
      {
        id: 1,
        title: "Actualidad",
        uri: "/actualidad",
        items: [
          {
            id: 1,
            title: "Política",
            uri: "/actualidad/politica",
          },
          {
            id: 2,
            title: "Economía",
            uri: "/actualidad/economia",
          },
          {
            id: 3,
            title: "Educación",
            uri: "/actualidad/educacion",
          },
          {
            id: 4,
            title: "Tecnología",
            uri: "/actualidad/tecnologia",
          },
        ],
      },
    ],
    two: [
      {
        id: 2,
        title: "Bienestar",
        uri: "/bienestar",
        items: [
          {
            id: 1,
            title: "Salud mental",
            uri: "/bienestar/salud-mental",
          },
          {
            id: 2,
            title: "Cambio de hábitos",
            uri: "/bienestar/cambio-de-habitos",
          },
          {
            id: 3,
            title: "Sexo",
            uri: "/bienestar/sexo",
          },
        ],
      },
    ],
    three: [
      {
        id: 3,
        title: "Entorno",
        uri: "/entorno",
        items: [
          {
            id: 1,
            title: "Sociedades",
            uri: "/entorno/sociedades",
          },
          {
            id: 2,
            title: "Activismo",
            uri: "/entorno/activismo",
          },
          {
            id: 3,
            title: "Género",
            uri: "/entorno/genero",
          },
          {
            id: 4,
            title: "Medio ambiente",
            uri: "/entorno/medio-ambiente",
          },
        ],
      },
    ],
    four: [
      {
        id: 4,
        title: "Cultura",
        uri: "/cultura",
        items: [
          {
            id: 1,
            title: "Arte y música",
            uri: "/cultura/arte-y-musica",
          },
          {
            id: 2,
            title: "Películas, series y libros",
            uri: "/cultura/peliculas-series-y-libros",
          },
          {
            id: 3,
            title: "Viajes y gastronomía",
            uri: "/cultura/viajes-y-gastronomia",
          },
          {
            id: 4,
            title: "Gamers",
            uri: "/cultura/gamers",
          },
        ],
      },
    ],
  };
};
