import Link from "next/link";
import {FacebookShare, TwitterShare, LinkedIn} from "../components/share/Share"
import { gtmSocialNetworkDetalle } from "../components/Gtm";
import React, {useState,useEffect, useRef } from "react";
/**
 * Componente funcional encargado de mostrar el banner de articulos
 */

const urlBase = process.env.URL_ELTIEMPO_IMG

const BannerArticulos = (props) => {
const [dataFull, setdataFull] = useState([]);
const [icono_personaje, seticono_personaje] = useState([]);
const [imagen_autor_articulo, setimagen_autor_articulo] = useState([]);
const [banner_desktop, setbanner_desktop] = useState([]);
const [banner_mobile, setbanner_mobile] = useState([]);

useEffect(() => { 
  try {
    setdataFull(props.detalleBannerArticulo[0]._source)
    seticono_personaje(props.detalleBannerArticulo[0]._source.icono_personaje[0])
    setimagen_autor_articulo(props.detalleBannerArticulo[0]._source.imagen_autor_articulo[0])
    setbanner_desktop(props.detalleBannerArticulo[0]._source.banner_desktop[0])
    setbanner_mobile(props.detalleBannerArticulo[0]._source.banner_mobile[0])
  }catch(error){
    // console.log(error)
  }
},[props])


const creationDate = new Date(dataFull.fecha_creacion*1000)

return (
  <div className='BannerArticulos'>
      <div className='content-max'>
      <div className='social'>
            <span onClick={() => { gtmSocialNetworkDetalle("Facebook", dataFull.nombre_categoria[0], dataFull.subcategoria[0], dataFull.name[0], dataFull.nombre_personaje[0], dataFull.tipo[0] === "Informativo" ? "Articulo" : dataFull.tipo[0], dataFull.nid[0], dataFull.title[0]) }}>
              <Link href="https://www.facebook.com/">
                <FacebookShare text={"facebook"}/>
              </Link>
            </span>

            <span onClick={() => { gtmSocialNetworkDetalle("Twitter", dataFull.nombre_categoria[0], dataFull.subcategoria[0], dataFull.name[0], dataFull.nombre_personaje[0], dataFull.tipo[0] === "Informativo" ? "Articulo" : dataFull.tipo[0], dataFull.nid[0], dataFull.title[0]) }}>
              <Link href="https://www.twitter.com/"> 
                <TwitterShare text={"twitter"}/>
              </Link>
            </span>

            <span onClick={() => { gtmSocialNetworkDetalle("Linkedin", dataFull.nombre_categoria[0], dataFull.subcategoria[0], dataFull.name[0], dataFull.nombre_personaje[0], dataFull.tipo[0] === "Informativo" ? "Articulo" : dataFull.tipo[0], dataFull.nid[0], dataFull.title[0]) }}>
              <Link href="https://www.linkedin.com/">
              <LinkedIn text={"linkedin"}/>
              </Link>
            </span>
          </div>
          <div className='content'>
            
            <div className='info'>

              <div className='content-info'>
                <div className='content-tags'>
                  <div className='tags'>
                  {dataFull.subcategoria}
                  </div>
                  <div className='ico-tag'>
                      <img
                        src={urlBase + icono_personaje.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                        alt={dataFull.nombre_personaje}
                        title={dataFull.nombre_personaje}
                      />
                  </div>
                </div>

                <div className='date'>{creationDate.toLocaleDateString("en-US")}</div>
                
                  <h1>{dataFull.title}</h1>

                <div className='autor'>
                  <div className='ico-autor'>
                      <img
                        src={urlBase + imagen_autor_articulo.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                        alt={dataFull.nombre_autor_articulo}
                        title={dataFull.nombre_autor_articulo}
                      />
                  </div>
                  <p>{dataFull.nombre_autor_articulo}</p>
                </div>

                <div className='description'>
                  <p>{dataFull.descripcion_corta}</p> 
                </div>
              </div>
              
            </div>

            <div className='image-banner'>
                <picture>
                      <source 
                        srcSet={urlBase + banner_mobile.toString().replace("/sites","").replace("/caustica","").replace("/files","")} 
                        media="(max-width: 1024px)" />
                      <img
                        src={urlBase + banner_desktop.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                        alt={dataFull.alt_desktop}
                        title={dataFull.title_desktop}
                  />
                  </picture>
              </div>

          </div>
      </div>
  </div>
);
};

export default BannerArticulos;
