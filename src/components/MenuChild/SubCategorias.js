/**
 * Función encargada de mostrar las subcategorias de cada item del menú principal
 */

export const ItemsActualidad = () => {
  return {
    items: [
      {
        id: 1,
        section: "Actualidad",
        title: "Política",
        uri: "/actualidad/politica",
      },
      {
        id: 2,
        section: "Actualidad",
        title: "Economía",
        uri: "/actualidad/economia",
      },
      {
        id: 3,
        section: "Actualidad",
        title: "Educación",
        uri: "/actualidad/educacion",
      },
      {
        id: 4,
        section: "Actualidad",
        title: "Tecnología",
        uri: "/actualidad/tecnologia",
      },
    ],
  };
};

export const ItemsBienestar = () => {
  return {
    items: [
      {
        id: 1,
        section: "Bienestar",
        title: "Salud mental",
        uri: "/bienestar/salud-mental",
      },
      {
        id: 2,
        section: "Bienestar",
        title: "Cambio de hábitos",
        uri: "/bienestar/cambio-de-habitos",
      },
      {
        id: 3,
        section: "Bienestar",
        title: "Sexo",
        uri: "/bienestar/sexo",
      },
    ],
  };
};

export const ItemsEntorno = () => {
  return {
    items: [
      {
        id: 1,
        section: "Entorno",
        title: "Sociedades",
        uri: "/entorno/sociedades",
      },
      {
        id: 2,
        section: "Entorno",
        title: "Activismo",
        uri: "/entorno/activismo",
      },
      {
        id: 3,
        section: "Entorno",
        title: "Género",
        uri: "/entorno/genero",
      },
      {
        id: 4,
        section: "Entorno",
        title: "Medio ambiente",
        uri: "/entorno/medio-ambiente",
      },
    ],
  };
};

export const ItemsCultura = () => {
  return {
    items: [
      {
        id: 1,
        section: "Cultura",
        title: "Arte y música",
        uri: "/cultura/arte-y-musica",
      },
      {
        id: 2,
        section: "Cultura",
        title: "Películas, series y libros",
        uri: "/cultura/peliculas-series-y-libros",
      },
      {
        id: 3,
        section: "Cultura",
        title: "Viajes y gastronomía",
        uri: "/cultura/viajes-y-gastronomia",
      },
      {
        id: 4,
        section: "Cultura",
        title: "Gamers",
        uri: "/cultura/gamers",
      },
    ],
  };
};
