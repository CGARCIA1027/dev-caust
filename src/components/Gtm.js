import TagManager from "react-gtm-module"

//2A OK
 export const gtmSocialNetworkHeader = (tipo) => {
     let Userid = ""
     let Estado_Usuario = "Anonimo"
     let catchUser = JSON.parse(window.localStorage.getItem('userEmptor')); 

     if (catchUser === null){
        Userid = undefined
     }else {
        Userid = catchUser.uid
        Estado_Usuario = "Registrado"
     }

    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Redes Sociales",
        label: tipo,
        Userid: Userid,
        Estado_Usuario: Estado_Usuario,
      },
    }
    TagManager.dataLayer(tagManagerArgs)
  }


//2B OK
  export const gtmLinksHeader = (cual) => {
       const tagManagerArgs = {
         dataLayer: {
           event: "ga_event",
           category: "Home",
           action: "Clic Menu",
           label: cual,
         },
       }
       TagManager.dataLayer(tagManagerArgs)
}

//2C OK
export const gtmSubseccionesHeader = (cual) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Submenu",
        label: cual,
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//2D OK
export const gtmBtnBuscadorHeader = () => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Header",
        label: "Buscador",
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//2E OK
export const gtmBtnPerfilHeader = () => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Header",
        label: "Perfil",
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}
 
//3A OK
export const gtmBuscador = (termino) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Buscador",
        label: termino,
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//4A OK
export const gtmbannerSliderHome = (titleNotice, seccion, subseccion, categoria, personaje, fecha, tipo, posicion, idContent) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Banner Principal",
        label: titleNotice,
        Seccion: seccion,
        Subseccion: subseccion,
        Categoria: categoria,
        Personaje: personaje,
        Fecha_Publicacion: fecha,
        Tipo_Contenido: tipo,
        Posicion: posicion,
        id_contenido: idContent,
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//4B OK
export const gtmSentidosHome = (termino) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Filtro Categorias",
        label: termino,
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//5A OK
export const gtmNoticiasMasonryHome = (titleNotice, seccion, subseccion, categoria, personaje, fecha, tipo, posicion, idContent) => {

    if(personaje == "N/A"){
      personaje = undefined
    }
    if(posicion == "N/A"){
      posicion = undefined
    }

    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Noticias",
        label: titleNotice,
        Seccion: seccion,
        Subseccion: subseccion,
        Categoria: categoria,
        Personaje: personaje,
        Fecha_Publicacion: fecha,
        Tipo_Contenido: tipo,
        Posicion: posicion,
        id_contenido: idContent,
        origin: "Masonry Home",
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//6A OK
export const gtmLoadMoreHome = () => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Noticias",
        label: "Cargar Mas",
        origin: "Masonry Home btn",
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}


//6B OK
export const gtmClickPersonajeHome = (personaje) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Seleccionar Personaje",
        label: personaje,
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//7A OK
export const gtmClickVerMasPersonajeHome = (personaje) => {
    let cualAction = "Clic Seleccionar " + personaje
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: cualAction,
        label: "Ver Mas",
        origin: "Ver mas personaje home",
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//8A OK
export const gtmFooter = (termino) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Footer",
        label: termino,
        origin: "Footer Menu",
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//8B OK
export const gtmSocialNetworkFooter = (termino) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Footer",
        label: termino,
        origin: "Footer Social Network",
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//8C OK
export const gtmDarkMode = (termino) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Funcion",
        label: termino,
        origin: "Dark Mode",
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//8D OK
export const gtmLastLinksFooter = (termino) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Terminos y Condiciones",
        label: termino,
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//9A OK
export const gtmLinksSections = (termino) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home",
        action: "Clic Menu Secciones",
        label: termino,
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}


//10A OK
export const gtmNoticiasMasonryHomeSeccion = (titleNotice, seccion, subseccion, categoria, personaje, fecha, tipo, posicion, idContent) => {
  if(personaje == "N/A"){
    personaje = undefined
  }
  if(posicion == "N/A"){
    posicion = undefined
  }

    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Home Seccion",
        action: "Clic Noticias",
        label: titleNotice,
        Seccion: seccion,
        Subseccion: subseccion,
        Categoria: categoria,
        Personaje: personaje,
        Fecha_Publicacion: fecha,
        Tipo_Contenido: tipo,
        Posicion: posicion,
        id_contenido: idContent,
        origin: "Masonry Home Secciones",

      },
    }
    TagManager.dataLayer(tagManagerArgs)
}
 
//11A
export const gtmDetalle = (seccion, subseccion, categoria, personaje, fecha, tipo, idContent, titleNotice) => {
        const tagManagerArgs = {
          dataLayer: {
            event: "ga_detail",
            Seccion: seccion,
            Subseccion: subseccion,
            Categoria: categoria,
            Personaje: personaje,
            Fecha_Publicacion: fecha,
            Tipo_Contenido: tipo,
            id_contenido: idContent,
            titulo_noticia: titleNotice,
            origin: "Detalle Articulo",
          },
        }
        TagManager.dataLayer(tagManagerArgs)
    }


//11B OK
export const gtmSocialNetworkDetalle = (red, seccion, subseccion, categoria, personaje, tipo, idContent, titleNotice) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Articulo",
        action: "Clic Redes Sociales",
        label: red,
        Seccion: seccion,
        Subseccion: subseccion,
        Categoria: categoria,
        Personaje: personaje,
        Tipo_Contenido: tipo,
        id_contenido: idContent,
        titulo_noticia: titleNotice,
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}


//12A OK
export const gtmDowloadDetalle = (title, seccion, subseccion, categoria, personaje, tipo, posicion, id) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Articulo",
        action: "Clic Descargar",
        label: title,
        Seccion: seccion,
        Subseccion: subseccion,
        Categoria: categoria,
        Personaje: personaje,
        Tipo_Contenido: tipo,
        id_contenido: id,
        Posicion: posicion,
        origin: "Download img Detalle"
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}


//13A OK
export const gtmDetalleMasonry = (titleNotice, seccion, subseccion, categoria, personaje, fecha, tipo, idContent) => {
  if(personaje == "N/A"){
    personaje = undefined
  }
  if(seccion == "N/A"){
    seccion = undefined
  }
  
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Articulo",
      action: "Clic Te puede Interesar",
      label: titleNotice,
      Seccion: seccion,
      Subseccion: subseccion,
      Categoria: categoria,
      Personaje: personaje,
      Fecha_Publicacion: fecha,
      Tipo_Contenido: tipo,
      id_contenido: idContent,
      origin: "Masonry Detalle"
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}

//14A OK
export const gtmFiltros = () => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Filtros",
        action: "Clic Filtro Categorias",
        label: "Desplegar",
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}

//14B OK
export const gtmFiltrosSentidos = (sentido) => {
    const tagManagerArgs = {
      dataLayer: {
        event: "ga_event",
        category: "Filtros",
        action: "Clic Filtro Categorias",
        label: sentido,
        origin: "Clic Sentido Flotante",
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}


//15A OK
export const gtmFiltrosPersonajesCategorias = (selecccion) => {
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Personajes",
      action: "Clic Filtro",
      label: selecccion,
      origin: "Clic Filtro Personaje Categoria",
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}



//15B OK
export const gtmFiltrosPersonajesSentidos = (selecccion) => {
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Personajes",
      action: "Clic Filtro",
      label: selecccion,
      origin: "Clic Filtro Personaje Sentidos",
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}


//15C
export const gtmBtnBuscadorPersonajes = (busqueda) => {
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Personajes",
      action: "Clic Buscador",
      label: busqueda,
      origin: "Clic Buscador Personaje",
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}


//16A OK
export const gtmPersonajesMasonry = (titleNotice, seccion, subseccion, categoria, personaje, fecha, tipo, idContent) => {
  if(personaje == "N/A"){
    personaje = undefined
  }
  if(seccion == "N/A"){
    seccion = undefined
  }
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Personajes",
      action: "Clic Noticias",
      label: titleNotice,
      Seccion: seccion,
      Subseccion: subseccion,
      Categoria: categoria,
      Personaje: personaje,
      Fecha_Publicacion: fecha,
      Tipo_Contenido: tipo,
      id_contenido: idContent,
      origin: "Masonry Personajes"
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}


//16B
export const gtmMasonryPersonajesCargarMas = () => {
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Personajes",
      action: "Clic Noticias",
      label: "Cargar Mas",
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}


//17A OK
export const gtmFiltrosSentidoCategorias = (selecccion) => {
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Categorias",
      action: "Clic Filtro",
      label: selecccion,
      origin: "Clic Filtro Sentido Categoria",
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}



//17B OK
export const gtmFiltrosSentidoSentidos = (selecccion) => {
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Categorias",
      action: "Clic Filtro",
      label: selecccion,
      origin: "Clic Filtro Sentido Sentidos",
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}


//17C
export const gtmBtnBuscadorSentidos = (busqueda) => {
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Categorias",
      action: "Clic Buscador",
      label: busqueda,
      origin: "Clic Buscador Sentidos",
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}


//18A OK
export const gtmSentidosMasonry = (titleNotice, seccion, subseccion, categoria, personaje, fecha, tipo, idContent) => {
  if(personaje == "N/A"){
    personaje = undefined
  }
  if(seccion == "N/A"){
    seccion = undefined
  }
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Categorias",
      action: "Clic Noticias",
      label: titleNotice,
      Seccion: seccion,
      Subseccion: subseccion,
      Categoria: categoria,
      Personaje: personaje,
      Fecha_Publicacion: fecha,
      Tipo_Contenido: tipo,
      id_contenido: idContent,
      origin: "Masonry Sentidos",
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}

//18B
export const gtmMasonryCategoriasCargarMas = () => {
  const tagManagerArgs = {
    dataLayer: {
      event: "ga_event",
      category: "Categorias",
      action: "Clic Noticias",
      label: "Cargar Mas",
    },
  }
  TagManager.dataLayer(tagManagerArgs)
}

//Transversal OK
export const gtmTransversal = () => {
    let Userid = ""
    let Estado_Usuario = "Anonimo"
    let catchUser = JSON.parse(window.localStorage.getItem('userEmptor')); 

    if (catchUser === null){
        Userid = undefined
     }else {
        Userid = catchUser.uid
        Estado_Usuario = "Registrado"
     }

    const tagManagerArgs = {
      dataLayer: {
        event: 'ga_usuario',
        Userid:Userid,
        Estado_Usuario: Estado_Usuario,
        origin: "Page View"
      },
    }
    TagManager.dataLayer(tagManagerArgs)
}