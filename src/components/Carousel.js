import Carousel from "react-elastic-carousel";
class ImageSlider extends React.Component {
    constructor(props) {
    super(props);
    var img1, img2, img3, img4;
    this.state = {
        images: [img1=["1","/static/assets/bannerHome.png", "banner", "banner"],]
                 //img2=["2","/static/assets/bannerArticulo.png", "banner", "banner"]]            
      }
    }
    render() {
      return (
        <>                        
          <Carousel 
            ref={ref => (this.carousel = ref)}
            enableAutoPlay={true}
            autoPlaySpeed={3500}
            focusOnSelect={true}
            enableMouseSwipe={false}                     
            className="slider"
            itemsToShow={1}
          >
            {
              this.state.images.length > 0
                    ? this.state.images.map((item,q) => {                       
                            return (
                              <div className="slider-images-container" key={`q-${q}`}>  
                                  <div className="image-banner-caption">
                                      <div className="image-banner-icon">
                                          <img className="banner-icon" src="/static/assets/paloma-icon.png"></img>
                                      </div>
                                      <div className="image-banner-class">
                                          <h4 className="banner-class">Sociedad</h4>
                                      </div>
                                      <div className="image-banner-title">Magistradas juzgan al estado Colombiano por no proteger a las mujeres</div>
                                      <div className="image-banner-date">Ago/29/20</div>
                                  </div>                        
                                  <img                                    
                                    src={item[1]}
                                    alt={item[2]}
                                    title={item[3]}
                                  />                    
                              </div>                                                                      
                        );
                      }): "Not found items"
            }           
          </Carousel>                    
          <div className='control-buttons'>
              <div className='prev' onClick={() => this.carousel.slidePrev()}>Prev</div>
              <div className='next' onClick={() => this.carousel.slideNext()}>Next</div>
          </div>            
        </>
      )
    }
  }
export default ImageSlider
