import Link from "next/link";
import React, {useState,useEffect, useRef } from "react";
/**
 * Componente funcional encargado de mostrar el contenido de articulo copy e imagen
 */
 
const InfoImage = (props) => {
const urlBase = process.env.URL_ELTIEMPO_IMG 
const [body_texto_articulo, setbody_texto_articulo] = useState([]);
const [imagen_texto_largo, setimagen_texto_largo] = useState([]);

useEffect(() => { 
  try {
    setbody_texto_articulo(props.detalleInfoImage[0]._source.body[0])
    setimagen_texto_largo(props.detalleInfoImage[0]._source.imagen_texto_largo[0])
  }catch(error){
    // console.log(error)
  }
},[props])

  
  if(body_texto_articulo != "" && imagen_texto_largo != ""){
    return (
      <div className='content-info-image'>
        <div className='info' dangerouslySetInnerHTML={{
                  __html: body_texto_articulo,
                }} />
          
        <div className='image'>
            <img
              src={urlBase + imagen_texto_largo.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
              alt="Cáustica"
              title="Cáustica"
            />
        </div>
      </div>
  );
  }else if(body_texto_articulo != "" && imagen_texto_largo == ""){
    return (
      <div className='content-info-image full'>
        <div className='info' dangerouslySetInnerHTML={{
                  __html: body_texto_articulo,
                }} />
      </div>
  );
  }else {
    return (
      <></>
    );
  }

};

export default InfoImage;
