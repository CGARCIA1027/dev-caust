import React, {useState,useEffect, useRef } from "react";
import Link from "next/link";
import {FacebookShare, TwitterShare, LinkedIn} from "../components/share/Share"

/**
 * Componente funcional encargado de mostrar el video en articulos
 */
const VideoArticulo = (props) => {

const [urlVideo, seturlVideo] = useState("");
useEffect(() => { 
  try {
    seturlVideo(
      props.detalleVideoArticulo[0]._source.id_video_youtube
    )
  }catch(error){
    // console.log(error)
  }
},[props])

let urlVideoFinal = "https://www.youtube.com/embed/" + urlVideo

  return (
    <div className="video-acticulo">
        <div className='content'>
        <div className='social'>
            <Link href="https://www.facebook.com/">
              <FacebookShare text={"facebook"}/>
            </Link>
            <Link href="https://www.twitter.com/">
            <TwitterShare text={"facebook"}/>
            </Link>
            <Link href="https://www.linkedin.com/">
            <LinkedIn text={"linkedin"}/>
            </Link>
          </div>
          <div className="content-iframe">
          <iframe width="560" height="315" src={urlVideoFinal} frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
          </div>
        </div>
      </div>
  );
};

export default VideoArticulo;