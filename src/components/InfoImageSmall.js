import Link from "next/link";
import React, {useState,useEffect, useRef } from "react";
/**
 * Componente funcional encargado de mostrar el contenido de articulo copy e imagen
 */

const InfoImageSmall = (props) => {
  const urlBase = process.env.URL_ELTIEMPO_IMG
  const [dataFull, setdataFull] = useState([]);

const [segundo_texto_articulo, setsegundo_texto_articulo] = useState([]);
const [imagen_segundo_texto, setimagen_segundo_texto] = useState([]);

  useEffect(() => { 
    try {
      setdataFull(
        props.detalleInfoImageSmall[0]._source
      )

      setsegundo_texto_articulo(props.detalleInfoImageSmall[0]._source.segundo_texto_articulo[0])
      setimagen_segundo_texto(props.detalleInfoImageSmall[0]._source.imagen_segundo_texto[0])
    }catch(error){
      // console.log(error)
    }
  },[props])
  

if(segundo_texto_articulo != "" && imagen_segundo_texto != ""){
  return (
    <div className='content-info-image'>
      <div className='info' dangerouslySetInnerHTML={{
                __html: segundo_texto_articulo,
              }} />
         
      <div className='image'>
          <img
            src={urlBase + imagen_segundo_texto.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
            alt="Cáustica"
            title="Cáustica"
          />
      </div>
    </div>
);
}else if(segundo_texto_articulo != "" && imagen_segundo_texto == ""){
  return (
    <div className='content-info-image full'>
      <div className='info' dangerouslySetInnerHTML={{
                __html: segundo_texto_articulo,
              }} />
    </div>
);
}else {
  return (
    <></>
  );
}
 
};

export default InfoImageSmall;
