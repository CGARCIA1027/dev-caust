import React from "react";

/**
 * Funciones encargadas de compartir un articulo de noticia en redes sociales
 */

export const FacebookShare = (props) => {
  const handleClick = (e) => {
    const height = 600;
    const width = 600;

    let y = window.outerHeight / 2 + window.screenY - height / 2;
    let x = window.outerWidth / 2 + window.screenX - width / 2;
    let text = "Facebook";

    window.open(
      `https://www.facebook.com/sharer/sharer.php?u=${
        window.location.href
      }&t=${props.text.replace("%", " porciento")}`,
      "Facebook",
      `width=${width},height=${height},top=${y},left=${x}menubar=no,resizable=no,scrollbars=no,status=no`
    );
  };
 
  return (
    <a  onClick={handleClick} className='fb'>
    <img
        src="/static/assets/ico-fb-share.svg"
        alt="Facebook"
        title="Facebook"
      />
  </a>   
  );
};

export const TwitterShare = (props) => {
  const height = 600;
  const width = 600;

  const handleClick = (e) => {
    let y = window.outerHeight / 2 + window.screenY - height / 2;
    let x = window.outerWidth / 2 + window.screenX - width / 2;
    let text = "Twitter";

    window.open(
      `http://twitter.com/intent/tweet?source=webclient&text=${
        window.location.href
      } ${props.text.replace("%", " porciento").replace(" ", "+")}`,
      "window",
      `width=${width},height=${height},top=${y},left=${x}`
    );
  };

  return (
    <a onClick={handleClick}  className='tw'>
    <img
      src="/static/assets/ico-tw-share.svg"
      alt="Twitter"
      title="Twitter"
    />
    </a>
  );
};


export const LinkedIn = (props) => {
  const height = 600;
  const width = 600;

  const handleClick = (e) => {
    let y = window.outerHeight / 2 + window.screenY - height / 2;
    let x = window.outerWidth / 2 + window.screenX - width / 2;
    let text = "LinkedIn";

    window.open(
      `https://www.linkedin.com/sharing/share-offsite/?url=${
        window.location.href
      } ${props.text.replace("%", " porciento").replace(" ", "+")}`,
      "window",
      `width=${width},height=${height},top=${y},left=${x}`
    );
  };

  return (
    <a className='ig' onClick={handleClick}>
      <img
        src="/static/assets/ico-in-share.svg"
        alt="linkedin"
        title="linkedin"
      />
  </a>
  );
};

export const WhatsAppShare = (props) => {
  const height = 800;
  const width = 800;

  const handleClick = (e) => {
    let y = window.outerHeight / 2 + window.screenY - height / 2;
    let x = window.outerWidth / 2 + window.screenX - width / 2;
    let text = "Whatsapp";
    window.open(
      `whatsapp://send?text=${window.location.href} ${props.text.replace(
        "%",
        " porciento"
      )}`,
      "Whatsapp",
      `width=${width},height=${height},top=${y},left=${x}menubar=no,resizable=no,scrollbars=no,status=no`
    );
  };

  return (
    <a onClick={handleClick} className="whatsapp-share">
      <img
        src="/static/assets/whatsapp.png"
        alt="Logo WhatsApp"
        className="social-links__image"
      />
    </a>
  );
};

export const MessengerShare = () => {
  const height = 500;
  const width = 500;

  const handleClick = (e) => {
    let y = window.outerHeight / 2 + window.screenY - height / 2;
    let x = window.outerWidth / 2 + window.screenX - width / 2;

    window.open(
      `fb-messenger://share?link=${encodeURIComponent(
        window.location.href)}&app_id=${encodeURIComponent("329528741587330")}`,`width=${width},height=${height},top=${y},left=${x}menubar=no,resizable=no,scrollbars=no,status=no`
    );
  };

  return (
    <a onClick={handleClick} className="messenger-share">
      <img
        src="/static/assets/messenger.png"
        alt="Logo Messenger"
        className="social-links__image"
      />
    </a>
  );
};
