import { useEffect, useState } from "react";
import Link from "next/link";
import axios from 'axios';


const LoginUserData = () => {   
    const [expiration, setExpiration] = useState('null')
    const [username, setUsername] = useState('null') 

    useEffect(() => {
        try{
            const catchExpiration = window.localStorage.getItem('exp')
            const item = JSON.parse(catchExpiration)
            setExpiration(item.expiry)            
            const now = new Date()          
            
            if (now.getTime() > expiration) {                	
                closeSession()                
            }

        }catch(error){
            console.log(error)
        }
        ///////////
        try{
            var userData = JSON.parse(window.localStorage.getItem('userEmptor'));
            var authType = JSON.parse(window.localStorage.getItem('authType'));
            
            if(authType=='Facebook' || authType=='Google' || authType=='Classic' ){               
                if(userData === null){
                    username=undefined
                }else{                    
                    setUsername(userData.first_name +' '+ userData.last_name)
                }        
            }else{
                username=undefined
            }
        }catch(err){
            console.log(err)
        }    


    }, [])
    
   
    const mostrar = () =>{
        var mostrar = document.getElementsByClassName('user-menu')[0].style.display
       
        if(mostrar=='none' || mostrar == ''){
            document.getElementsByClassName('user-menu')[0].style.display = 'block'            
            document.getElementById('arrow-user-name').style.transform = 'rotate(180deg)';
        }else{
            document.getElementsByClassName('user-menu')[0].style.display = 'none'
            document.getElementById('arrow-user-name').style.transform = 'rotate(360deg)';
        }
    }

    const closeSession = () =>{          
        let EMPTOR = JSON.parse(process.env.EMPTOR);
        let bearer = JSON.parse(window.localStorage.getItem('user')).accessToken;
        let url = EMPTOR.BASE_DOMAIN + EMPTOR.LOGOUT.BASIC;     
       
        axios
        .post(
          url,
          {},
          {
            headers: { Authorization: "Bearer " + bearer, },
          }
        )
        .then(async (res) => {                
            if(res.data.data.message.includes("Sesión terminada")){
                let keysToRemove = ["email", "user", "authType", "exp", "userEmptor"];
                keysToRemove.forEach(k => localStorage.removeItem(k))
                open("/", "_self");
            }       
        })
        .catch((err) => {
            console.log(err)     
            alert('fallo el cierrre de sesión')
        });
    }
    return (      
        <>
            <div className="menu-user-data">
                <div className="user-name-container">
                    <div className="user-name" onClick={mostrar}>{username}</div>
                    <div id="arrow-user-name" onClick={mostrar}/>
                </div>
                <ul className="user-menu">
                    <Link href="/cuenta"> 
                        <a href=''><li>Configurar mis datos</li></a>
                   </Link>
                   <li className="close-session" onClick={closeSession}>Cerrar sesión</li>
               </ul>
            </div>
        </>
    )
}

export default LoginUserData;
