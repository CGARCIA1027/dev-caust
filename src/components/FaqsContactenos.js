import React, {useState,useEffect, useRef } from "react";
import axios from "axios";
/**
 * Componente funcional encargado de mostrar el contenido de preguntas frecuentes 
 */
const faqsContact = (props) => {

  let params = new URLSearchParams()
  const [itemFaqs, setItemFaqs] = useState([]);
  const [itemPreguntas, setItemPreguntas] = useState([]);
  const [itemRespuestas, setItemRespuestas] = useState([]);

  useEffect(() => {
    params.set("nid", "8")
    axios.get('/api/elastic/', {params}).then((res) => { 
      try{
        setItemFaqs(res.data.body.hits.hits);
        setItemPreguntas(res.data.body.hits.hits[0]._source.pregunta);
        setItemRespuestas(res.data.body.hits.hits[0]._source.respuesta);  
        }catch(error){
          //console.log(error)
        }    
    });
  }, []) 

  let title = ""
  let descripcion = ""

  itemFaqs.map((item, i) => { 
      title = item._source.title[0]
      descripcion = item._source.descripcion_preguntas[0]
  })

let datosFaqs = []

for(const id in itemPreguntas){
  datosFaqs.push({pregunta: itemPreguntas[id], respuesta: itemRespuestas[id]});
}

  return (

    <div className="preguntas-frecuentes">
        <div className='content'>
                <div className="title">
                    <h2>{title}</h2>
                    <p>{descripcion}</p>
                </div>

                <div className="accordion" id="accordion">
                  
                {datosFaqs.length > 0
                  ? datosFaqs.map((item, index) => {
                      return (
                        
                        index === 0 ? (
                        
                            <div className="card" key={index}>
                            <div className="card-header" id={"heading" + index} >
                              <h2 className="mb-0">
                                <button className="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target={"#collapse" + index} aria-expanded="true" aria-controls="collapseOne">
                                {item.pregunta}
                                </button>
                              </h2>
                            </div>
                            <div id={"collapse" + index} className="collapse show" aria-labelledby={"heading" + index} data-parent="#accordion">
                              <div className="card-body" dangerouslySetInnerHTML={{
                                __html: item.respuesta,
                              }} />
                            </div>
                          </div>
                            
                          ) : (
                            <div className="card" key={index}>
                            <div className="card-header" id={"heading" + index}>
                              <h2 className="mb-0">
                                <button className="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target={"#collapse" + index} aria-expanded="true" aria-controls="collapseOne">
                                {item.pregunta}
                                </button>
                              </h2>
                            </div>
                            <div id={"collapse" + index} className="collapse" aria-labelledby={"heading" + index} data-parent="#accordion">
                            <div className="card-body" dangerouslySetInnerHTML={{
                                __html: item.respuesta,
                              }} />
                            </div>
                          </div>
                          )                     

                      );
                    })
                  : "Not found notices"}
            
                </div>
        </div>
      </div>
  );
};


export default faqsContact;