import Link from "next/link";
import Carousel, { consts } from "react-elastic-carousel";
import { LazyLoadImage } from "react-lazy-load-image-component";
import arrowLeft from "../../public/static/assets/arrow-left.png";
import arrowRigth from "../../public/static/assets/arrow-right.png";
import { gtmbannerSliderHome } from "../components/Gtm";

const myArrow = ({ type, onClick, isEdge }) => {
  const pointer = type === consts.PREV ? arrowLeft : arrowRigth;
  return (
    <button onClick={onClick} disabled={isEdge} className="carousel-button">
      <img src={pointer} />
    </button>
  );
};

const total = function(pages) { 
  return (
    pages.length
  )
}

const myPagination = ({pages, activePage, onClick}) => {  
  return (
    <div className="content-dots">
      <span>0{activePage+1}</span>
        <div className="dots">
            {pages.map(page => {
              const isActivePage = activePage === page
              return (
                <div
                  className={isActivePage === true ? "dot active" : "dot"}
                  key={page}
                  onClick={() => onClick(page)}
                  active={isActivePage}
                >
                </div>
              )
            })}
        </div>
      <span>0{total(pages)}</span>
  </div>
  )
}
 

const SliderDestacados = ({promoteItems}) => { 
  let urlBase = process.env.URL_ELTIEMPO_IMG 
  return (
    <>

        <Carousel
              enableAutoPlay={false}
              // autoPlaySpeed={3500}
              focusOnSelect={true}
              enableSwipe={true}
              enableMouseSwipe={true}
              renderArrow={myArrow}
              renderPagination={myPagination}
              className="slider-destacados"
            >
                {promoteItems.length > 0 ?
                  promoteItems.map((item, i) => {                
                      if(item._source.content_type[0] == 'detalle_del_articulo'){                      
                        const creationDate = new Date(item._source.fecha_creacion[0]*1000)
                        let categoriaLink = item._source.nombre_categoria[0].toLowerCase().replace(",","").replace(/ /g, "-")
                        let subcategoriaLink = item._source.subcategoria[0].toLowerCase().replace(",","").replace(/ /g, "-")
                        let icono_personaje = item._source.icono_personaje[0]
                        let banner_desktop = item._source.banner_desktop[0]
                        let banner_mobile = item._source.banner_mobile[0]
                        let urlAlias = ""
                        if(item._source.alias_url[0].includes('/node/')){
                          urlAlias = "/node"
                        }else {
                          urlAlias = item._source.alias_url[0].replace("/en","").replace("/detalle-articulo","") 
                        }

                        return (
                          <div className='item' key={item._source.nid[0]}>
                            <Link href={categoriaLink + "/" + subcategoriaLink + urlAlias + "/"+ item._source.nid[0]}>
                            <a
                              onClick={() => {
                                gtmbannerSliderHome(item._source.title[0], item._source.nombre_categoria[0], item._source.subcategoria[0], "Leer", item._source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), "Articulo", i+1, item._source.nid[0] ) 
                              }}

                            >
                              <div className='info-slider'>
                                <div className="banner-icon">
                                      <img className="icon" src={urlBase + icono_personaje}></img>
                                  </div>
                                  <div className="banner-tag">
                                      <div className="tag">{item._source.subcategoria[0]}</div>
                                  </div>
                                  <h2>{item._source.title[0]}</h2>
                                  <div className="banner-date">{creationDate.toLocaleDateString("en-US")}</div>
                              </div>

                              <div className="imgBanner">
                                <picture>
                                    <source 
                                        srcSet={urlBase + banner_mobile}
                                        media="(max-width: 1024px)" />
                                        <LazyLoadImage
                                          src={urlBase + banner_desktop}
                                          alt={item._source.title[0]}
                                          title={item._source.title[0]}
                                    />
                                </picture>
                              </div>
                              </a>
                              </Link>
                            </div>  
                        );

                      }
                    }): "Not Found Data"
                }

            </Carousel>

      </>

  )

};


export default SliderDestacados;