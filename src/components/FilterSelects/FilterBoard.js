import { useEffect, useState, useRef  } from "react";

const FilterBoard = () => {   
    const [filter, setFilters] = useState([]);
    const [pathName, setPathName] = useState([]);

     
    useEffect(async () => {
        let fullFilters = []
        let path = window.location.pathname
        setPathName(path)
        try{          
            for(var l = 0; l < 3; l++){                    
                let filterPass = JSON.parse(window.localStorage.getItem('filters'+l+path))                    
                if(filterPass === null ){}else{
                    if(filterPass.length > 0){
                        for(var m = 0; m < filterPass.length; m++){
                            fullFilters.push(filterPass[m].value)
                        }
                    }
                }              
            }
        }catch(err){
            console.log(err)
        }
        if(fullFilters.includes('*')){
           let indices = []
           let element = '*'
           let index = fullFilters.indexOf(element)
           while (index != -1) {
                indices.push(index);
                index = fullFilters.indexOf(element, index + 1);                
           }                 
          for(var t = 0; t < indices.length; t++){
            fullFilters[indices[t]] = 'Todos'
          }
           
        }
        setFilters(fullFilters)   
        
    },[])   
    
    const handleClick = (e) => {
        function arrayRemove(arr, value) {    
            return arr.filter(function(ele){ 
                return ele != value; 
            });
        }

        let filterRemove = e.target.parentNode.innerText.replace( 'X', "").replace(/(\r\n|\n|\r)/gm, "");  
              
        if(filter.includes(filterRemove)){
            let index = filter.indexOf(filterRemove)
            let remove = filter[index]  
            let newFilters = arrayRemove(filter, filter[index]);          
            setFilters(newFilters)
            try{
                for(var i=0; i < 3; i++){
                  var filterPass = JSON.parse(window.localStorage.getItem('filters'+i+pathName))
                  if(filterPass === null){}else{               
                    for(var b = 0; b < filterPass.length ; b++){                   
                            if(remove == 'Todos'){
                                remove='*'
                            }
                            if(filterPass[b].value == remove){
                                let upgradeFilter = arrayRemove(filterPass, filterPass[b]);
                                window.localStorage.setItem('filters'+i+pathName, JSON.stringify(upgradeFilter))
                                window.location.reload() 
                            }
                    }
                  }                
                }
            }catch(error){
            //   console.log(error)
            }  
        }
    }      
      return (
        <>     
            <div className="activeFilters">  
                {              
                    filter.length > 0                
                    ? filter.map((item, q) => {                                                   
                        return (
                            <div className="item" key={q}>                                               
                                <span>{item}</span> 
                                <span className="close"  onClick={handleClick}>  X</span>
                            </div>               
                        );           
                    }) : <></>
                }  
            </div>    
        </>
      );
     
     
    };
    
    export default FilterBoard;
    