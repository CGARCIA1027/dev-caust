import React, { Component } from "react";
import makeAnimated from "react-select/animated";
import { gtmFiltrosPersonajesCategorias, gtmFiltrosPersonajesSentidos, gtmFiltrosSentidoCategorias, gtmFiltrosSentidoSentidos } from "../Gtm";
import Select from "./Selects.js";
import { components } from "react-select";

const Option = props => {
  return (
    <div>
      <components.Option {...props}>
        <input
          type="checkbox"
          checked={props.isSelected}
          onChange={() => null}
        />{" "}
        <label>{props.label}</label>
      </components.Option>
    </div>
  );
};

const allOption = {
  label: "Select all",
  value: "*"
};

const ValueContainer = ({ children, ...props }) => {
  const currentValues = props.getValue();
  // console.log(currentValues)
  let toBeRendered = children;
  if (currentValues.some(val => val.value === allOption.value)) {
    toBeRendered = [[children[0][0]], children[1]];
  }

  return (
    <components.ValueContainer {...props}>
      {toBeRendered}
    </components.ValueContainer>
  );
};

const MultiValue = props => {
  let labelToBeDisplayed = `${props.data.label}, `;
  if (props.data.value === allOption.value) {
    labelToBeDisplayed = "Todos"
  }
  return (
    <components.MultiValue {...props}>
      <span>{labelToBeDisplayed}</span>
    </components.MultiValue>
  );
};

const animatedComponents = makeAnimated();
export default class FilterSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionSelected: null,
      pathName: null
    };
    
  }

  handleChange = selected => {
    this.setState({
      optionSelected: selected
    });   

    if(selected.length > 0){
      let pathOrigen = window.location.pathname.replaceAll("/","")
      let lastSelection = selected[selected.length - 1].value
      let lastFiltro = selected[selected.length - 1].filtro

      if(pathOrigen == "ver" || pathOrigen == "oir" || pathOrigen == "leer" || pathOrigen == "random"){
        if(lastFiltro == "Categorias"){
          gtmFiltrosSentidoCategorias(lastSelection)
        }else if(lastFiltro == "Sentidos"){
          gtmFiltrosSentidoSentidos(lastSelection)
        }
      }

      if(pathOrigen == "rata" || pathOrigen == "payaso" || pathOrigen == "semaforo" || pathOrigen == "paloma"){
        if(lastFiltro == "Categorias"){
          gtmFiltrosPersonajesCategorias(lastSelection)
        }else if(lastFiltro == "Sentidos"){
          gtmFiltrosPersonajesSentidos(lastSelection)
        }
      }
     
    }

  };
  
  componentDidMount(){     
      // document.getElementsByClassName('css-1wa3eu0-placeholder')[this.props.id].innerHTML = this.props.filterName
      const path = window.location.pathname
      this.setState({pathName: path})
      const prevFilters = JSON.parse(window.localStorage.getItem('filters'+this.props.id+path))         
      if(prevFilters !== undefined){
        this.setState({optionSelected: prevFilters})
      }
    
  }
  
  render() {   
   
    return (
      <span
        className="d-inline-block"
        data-toggle="popover"
        data-trigger="focus"
        // data-content="Please selecet account(s)"
      >
        <Select
          options={this.props.filterValues}
          isMulti
          closeMenuOnSelect={false}
          hideSelectedOptions={false}
          components={{
            Option,
            MultiValue,
            ValueContainer,
            animatedComponents
          }}
          onChange={this.handleChange}
          allowSelectAll={true}
          value={this.state.optionSelected}
          id={this.props.id}
          path={this.state.pathName}
          // styles={customStyles}
        />
      </span>
    );
  }
}

