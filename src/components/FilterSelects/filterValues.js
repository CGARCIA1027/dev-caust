export const filterValues1 = [

  { value: "Política", label: "Política", filtro: "Categorias"},  
  { value: "Educación", label: "Educación", filtro: "Categorias"},
  { value: "Economía", label: "Economía", filtro: "Categorias"},  
  { value: "Tecnología", label: "Tecnología", filtro: "Categorias"},
   
  { value: "Salud Mental", label: "Salud Mental", filtro: "Categorias"},
  { value: "Cambio de Habitos", label: "Cambio de Hábitos", filtro: "Categorias"},
  { value: "Sexo", label: "Sexo", filtro: "Categorias"},

  { value: "Sociedad", label: "Sociedad", filtro: "Categorias"},
  { value: "Activismo", label: "Activismo", filtro: "Categorias"},
  { value: "Género", label: "Género", filtro: "Categorias"},
  { value: "Medio ambiente", label: "Medio ambiente", filtro: "Categorias"},

  { value: "Arte y música", label: "Arte y m", filtro: "Categorias"},
  { value: "Películas series y libros", label: "Películas series y libros", filtro: "Categorias"}, 
  { value: "Viajes y gastronomía", label: "Viajes y gastronomía", filtro: "Categorias"},
  { value: "Gamers", label: "Gamers", filtro: "Categorias"},  

];
export const filterValues2 = [
  { value: "Paloma", label: "Paloma",  filtro: "Personajes"},
  { value: "Rata", label: "Rata",  filtro: "Personajes"},
  { value: "Semáforo", label: "Semáforo", filtro: "Personajes"},
  { value: "Payaso", label: "Payaso", filtro: "Personajes"},  
];

export const filterValues3 = [
  { value: "Ver", label: "Ver",  filtro: "Sentidos"},
  { value: "Oir", label: "Oir",  filtro: "Sentidos"},
  { value: "Leer", label: "Leer",  filtro: "Sentidos"},
  
];
export const groupedOptions = [
  {
    label: 'Personajes',
    options: filterValues1,
  },
  
];
  