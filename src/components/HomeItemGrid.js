import React from "react";
import Link from "next/link";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { gtmNoticiasMasonryHome } from "../components/Gtm";
var document = require("global/document");

const HomeItemGrid = ({ _source, tipoRelaciondo }) => {  
  var serverURL = process.env.URL_ELTIEMPO_IMG;
  const creationDate = new Date(_source.fecha_creacion[0] * 1000);
 
var  tipo = _source.content_type[0];
  if (tipo == "calendario") {
    let fondo_calendario = _source.fondo_calendario[0]
    let icono_tipo = _source.icono_tipo[0]
    return (
      <div className={fondo_calendario.toString().indexOf("morado") > 1 ? "content-card morado" : "content-card"}>
        <Link href={_source.enlace_calendario[0]} target="_blank">
          <a 
          href={_source.enlace_calendario[0]} target="_blank"
          onClick={() => {
            gtmNoticiasMasonryHome(_source.title[0], _source.content_type[0], _source.subcategoria[0], "Leer", "N/A", creationDate.toLocaleDateString("en-US"), _source.content_type[0], "N/A", _source.nid[0] ) 
          }}
          >
            <div
              className="calendar-home-grid-item-under-elements"
              style={{
                backgroundImage: `url(${
                  serverURL +
                  fondo_calendario
                  // .replace("/sites","").replace("/caustica","").replace("/files","")
                  //   .replace("default", "caustica")
                  //   .replace("html-caustica", "")
                })`,
              }}
            >
              <div className="calendar-home-grid-item-classification">
                <div className="calendar-home-grid-item-classification-text">
                  {_source.subcategoria[0]}
                </div>
                <div className="calendar-home-grid-item-banner-icon">
                  <LazyLoadImage
                    src={
                      serverURL +
                      icono_tipo
                      // .replace("/sites","").replace("/caustica","").replace("/files","")
                      //   .replace("default", "caustica")
                      //   .replace("html-caustica", "")
                    }
                    className="item-image-banner-icon"
                  />
                </div>
              </div>

              <h5 className="calendar-home-grid-item-title">
                {_source.title[0]}
              </h5>

              <div className="calendar-home-grid-item-footer">
                <h6 className="calendar-home-grid-item-date">
                  {creationDate.toLocaleDateString("es-ES")}
                </h6>
              </div>
            </div>
          </a>
        </Link>
      </div>
    );
  } else if (tipo == "detalle_video") {
    let categoria = _source.nombre_categoria[0].toLowerCase().replace(",","").replace(/ /g, "-")
    let subcategoria = _source.subcategoria[0].toLowerCase().replace(",","").replace(/ /g, "-")
    let icono_tipo = _source.icono_tipo[0]
    let icono_personaje = _source.icono_personaje[0]
    let urlAlias = ""
    if(_source.alias_url[0].includes('/node/')){
      urlAlias = "/node"
    }else {
      urlAlias = _source.alias_url[0].replace("/en","").replace("/detalle-articulo","") 
    }
    return (
      <div className="content-card video">
        <Link href={categoria + "/" + subcategoria + urlAlias + "/"+ _source.nid[0]}>
          <a
          onClick={() => {
            gtmNoticiasMasonryHome(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Ver", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), "Articulo", "N/A", _source.nid[0] ) 
          }}
          >
            <div className="card-home-grid-item-banner-icon">
              <LazyLoadImage
                src={
                  serverURL +
                  icono_tipo
                  // .replace("/sites","").replace("/caustica","").replace("/files","")
                  //   .replace("default", "caustica")
                  //   .replace("html-caustica", "")
                }
                className="item-image-banner-icon"
                alt={_source.nombre_personaje[0]}
              />
            </div>
            <div className="home-grid-item-banner">
              <div className="home-grid-item-banner-background">
                <div className="boxInt">
                  <LazyLoadImage
                    src={
                      "https://img.youtube.com/vi/" +
                      _source.id_video_youtube[0] +
                      "/sddefault.jpg"
                    }
                    className="home-grid-item-image"
                    alt="Video"
                  />
                </div>
              </div>
            </div>
            <div
              className="home-grid-item-under-elements"
              style={{ boxShadow: "-6px 6px 0px 0px" + _source.field_color[0] }}
            >
              <div className="home-grid-item-classification">
                <div
                  className="home-grid-item-classification-text"
                  style={{ backgroundColor: _source.field_color[0] }}
                >
                  {_source.subcategoria[0]}
                </div>
              </div>

              <h5 className="home-grid-item-title">{_source.title[0]}</h5>

              <div className="home-grid-item-footer">
                <h6 className="home-grid-item-date">
                  {creationDate.toLocaleDateString("es-ES")}
                </h6>
                <div className="home-grid-item-footer-align">
                  <h6 className="home-grid-item-class">Video</h6>
                  <LazyLoadImage
                    src={
                      serverURL +
                      icono_personaje
                      // .replace("/sites","").replace("/caustica","").replace("/files","")
                      //   .replace("default", "caustica")
                      //   .replace("html-caustica", "")
                    }
                    className="home-grid-item-character"
                    alt={_source.nombre_personaje[0]}
                  />
                </div>
              </div>
            </div>
          </a>
        </Link>
      </div>
    );
  } else if (tipo == "detalle_galeria") {
    let categoria = _source.nombre_categoria[0].toLowerCase().replace(",","").replace(/ /g, "-")
    let subcategoria = _source.subcategoria[0].toLowerCase().replace(",","").replace(/ /g, "-")
    let imagen_contenido_relacionado = _source.imagen_contenido_relacionado[0]
    let icono_tipo = _source.icono_tipo[0]
    let icono_personaje = _source.icono_personaje[0]
    let urlAlias = ""
    if(_source.alias_url[0].includes('/node/')){
      urlAlias = "/node"
    }else {
      urlAlias = _source.alias_url[0].replace("/en","").replace("/detalle-articulo","") 
    }
    return (
      <div className="content-card">
        <Link href={categoria + "/" + subcategoria + urlAlias + "/"+ _source.nid[0]}>
          <a
           onClick={() => {
            gtmNoticiasMasonryHome(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Ver", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), "Articulo", "N/A", _source.nid[0] ) 
          }}
          >
            <div className="card-home-grid-item-banner-icon">
              <LazyLoadImage
                src={
                  serverURL +
                  icono_tipo
                  // .replace("/sites","").replace("/caustica","").replace("/files","")
                  //   .replace("default", "caustica")
                  //   .replace("html-caustica", "")
                }
                className="item-image-banner-icon"
                alt={_source.nombre_personaje[0]}
              />
            </div>

            <div className="home-grid-item-banner">
              <div className="home-grid-item-banner-background">
                <LazyLoadImage
                  src={
                    serverURL + imagen_contenido_relacionado
                    // .replace("/sites","").replace("/caustica","").replace("/files","")
                    //   .replace("default", "caustica")
                    //   .replace("html-caustica", "")
                  }
                  className="home-grid-item-image"
                  alt={_source.title[0]}
                />
              </div>
            </div>

            <div
              className="home-grid-item-under-elements"
              style={{ boxShadow: "-6px 6px 0px 0px" + _source.field_color[0] }}
            >
              <div className="home-grid-item-classification">
                <div
                  className="home-grid-item-classification-text"
                  style={{ backgroundColor: _source.field_color[0] }}
                >
                  {_source.subcategoria[0]}
                </div>
              </div>

              <h5 className="home-grid-item-title">{_source.title[0]}</h5>

              <div className="home-grid-item-footer">
                <h6 className="home-grid-item-date">
                  {creationDate.toLocaleDateString("es-ES")}
                </h6>
                <div className="home-grid-item-footer-align">
                  <h6 className="home-grid-item-class">Galería</h6>
                  <LazyLoadImage
                    src={
                      serverURL +
                      icono_personaje
                      // .replace("/sites","").replace("/caustica","").replace("/files","")
                      //   .replace("default", "caustica")
                      //   .replace("html-caustica", "")
                    }
                    className="home-grid-item-character"
                    alt={_source.nombre_personaje[0]}
                  />
                </div>
              </div>
            </div>
          </a>
        </Link>
      </div>
    );
  } else if (tipo == "detalle_del_articulo") {
    let categoria = _source.nombre_categoria[0].toLowerCase().replace(",","").replace(/ /g, "-")
    let subcategoria = _source.subcategoria[0].toLowerCase().replace(",","").replace(/ /g, "-")
    let imagen_contenido_relacionado = _source.imagen_contenido_relacionado[0]
    let icono_personaje = _source.icono_personaje[0]
    let urlAlias = ""
    if(_source.alias_url[0].includes('/node/')){
      urlAlias = "/node"
    }else {
      urlAlias = _source.alias_url[0].replace("/en","").replace("/detalle-articulo","") 
    }
    return (
      <div className="content-card">
        <Link href={categoria + "/" + subcategoria + urlAlias + "/"+ _source.nid[0]}>
          <a
           onClick={() => {
            gtmNoticiasMasonryHome(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Leer", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), "Articulo", "N/A", _source.nid[0] ) 
          }}
          >
            <div className="home-grid-item-banner">
              <div className="home-grid-item-banner-background">
                <LazyLoadImage                  
                  src={
                    serverURL +
                    imagen_contenido_relacionado
                    // .replace("/sites","").replace("/caustica","").replace("/files","")
                  }
                  className="home-grid-item-image"
                  alt={_source.title[0]}
                />
              </div>
            </div>

            <div
              className="home-grid-item-under-elements"
              style={{ boxShadow: "-6px 6px 0px 0px" + _source.field_color[0] }}
            >
              <div className="home-grid-item-classification">
                <div
                  className="home-grid-item-classification-text"
                  style={{ backgroundColor: _source.field_color[0] }}
                >
                  {_source.subcategoria[0]}
                </div>
              </div>

              <h5 className="home-grid-item-title">{_source.title[0]}</h5>

              <div className="home-grid-item-footer">
                <h6 className="home-grid-item-date">
                  {creationDate.toLocaleDateString("es-ES")}
                </h6>
                <div className="home-grid-item-footer-align">
                  <h6 className="home-grid-item-class">Articulo</h6>
                  <LazyLoadImage
                    src={
                      serverURL +
                      icono_personaje
                      // .replace("/sites","").replace("/caustica","").replace("/files","")
                      //   .replace("default", "caustica")
                      //   .replace("html-caustica", "")
                    }
                    className="home-grid-item-character"
                    alt={_source.nombre_personaje[0]}
                  />
                </div>
              </div>
            </div>
          </a>
        </Link>
      </div>
    );
  }else {
    return (
      null
    )
  }
};

export default HomeItemGrid;
