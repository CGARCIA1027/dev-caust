import HomeItemGrid from "../components/HomeItemGrid";
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import { gtmLoadMoreHome } from "../components/Gtm";
import axios from "axios";
import React, { useState, useEffect } from 'react';

const MasonryGrid = ({tipoData}) => {
  console.log(tipoData)
  const [data, setData] = useState([])
  const [itemsLoaded, setitemsLoaded] = useState(13)
  const handleClick = (e) =>{
    e.preventDefault();
    setitemsLoaded(itemsLoaded + 6)
    gtmLoadMoreHome()
  }
  useEffect(()=>{
    setData(tipoData)     
    if(itemsLoaded >= tipoData.length){
      document.getElementById("btn-load").style.display = "none";
    } 
  }, [])    
  
    return (
      <>
        <ResponsiveMasonry
          columnsCountBreakPoints={{ 576: 1, 768: 2, 992: 3, 1500: 4 }}
        >
          <Masonry className="home-all-content">
            {data.length > 0
              ? data.map((item, q) => {
                    if ( q <= itemsLoaded ) {
                      return <HomeItemGrid key={q} {...item} />;
                    }
                 
                })
              : "Not found items"}
          </Masonry>
        </ResponsiveMasonry>
        <div id="btn-load" className="home-grid-loadMore" onClick={handleClick}>Cargar más</div>
      </>
    );
  
}


export default MasonryGrid;
