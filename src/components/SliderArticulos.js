import Link from "next/link";
import Carousel, { consts } from "react-elastic-carousel";
import arrowLeft from "../../public/static/assets/arrow-left.png";
import arrowRigth from "../../public/static/assets/arrow-right.png";
import { gtmDowloadDetalle } from "../components/Gtm";
import { LazyLoadImage } from "react-lazy-load-image-component";
import React, {useState,useEffect, useRef } from "react";
/**
 * Componente funcional encargado de mostrar el banner de articulos
*/

const myArrow = ({ type, onClick, isEdge }) => {
  const pointer = type === consts.PREV ? arrowLeft : arrowRigth;
  return (
    <button onClick={onClick} disabled={isEdge} className="carousel-button">
      <img src={pointer} />
    </button>
  );
};

const myPagination = ({pages, activePage, onClick}) => {
  return (

    <div className="content-dots">
      <span>0{activePage+1}</span>
        <div className="dots">
            {pages.map(page => {
              const isActivePage = activePage === page
              return (
                <div
                  className={isActivePage === true ? "dot active" : "dot"}
                  key={page}
                  onClick={() => onClick(page)}
                  active={isActivePage.toString()}
                >
                </div>
              )
            })}
        </div>
      <span>0{pages.length}</span>
  </div>
  )
}


const SliderArticulos = (props) => {
  const urlBase = process.env.URL_ELTIEMPO_IMG
  const [descripcionGaleria, setdescripcionGaleria] = useState([]);
  const [imagenGaleriaDesktop, setimagenGaleriaDesktop] = useState([]);
  const [imagenGaleriaMobile, setimagenGaleriaMobile] = useState([]);
  const [imagenGaleriaAlt, setimagenGaleriaAlt] = useState([]);
  const [imagenGaleriaTitle, setimagenGaleriaTitle] = useState([]);

  useEffect(() => { 
    try {
      setdescripcionGaleria (props.detalleSliderArticulos[0]._source.descripcion_galeria)
      setimagenGaleriaDesktop (props.detalleSliderArticulos[0]._source.imagenes_galeria)
      setimagenGaleriaMobile (props.detalleSliderArticulos[0]._source.mobile_galeria_detalle)
      setimagenGaleriaAlt (props.detalleSliderArticulos[0]._source.alt_mobile_galeria_detalle)
      setimagenGaleriaTitle (props.detalleSliderArticulos[0]._source.alt_mobile_galeria_detalle)
    }catch(error){
      // console.log(error)
    }
  },[props])

  let datosGaleria = []
  const creationDate = new Date(props.detalleSliderArticulos[0]._source.fecha_creacion*1000)
  
try {
  for(const id in descripcionGaleria){
    datosGaleria.push({descripcion: descripcionGaleria[id], link: imagenGaleriaDesktop[id], linkMobile: imagenGaleriaMobile[id], alt: imagenGaleriaAlt[id], title: imagenGaleriaTitle[id]});
  }
}catch(error){
  // console.log(error)
}

if(descripcionGaleria == undefined || imagenGaleriaMobile == undefined || imagenGaleriaAlt == undefined || imagenGaleriaTitle == undefined){
  return (
    <></>
  )
}else {
  return (
    
    <div className='SliderArticulos'>
        <div className='content-max'>
        <Carousel
              // enableAutoPlay={true}
              // autoPlaySpeed={3500}
              focusOnSelect={true}
              enableMouseSwipe={false}
              renderArrow={myArrow}
              renderPagination={myPagination}
              className="slider"
              stopOnHover={true}
            >

            {datosGaleria.map((item, p) => {                              
                  return (
                    <div key={p} className='item'>      
                      <picture>
                        <source 
                            srcSet={urlBase + item.linkMobile.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                            media="(max-width: 1024px)" />
                            <LazyLoadImage
                              src={urlBase + item.link.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                              alt={item.alt}
                              title={item.title}
                        />
                    </picture>

                        <div className='info'>
                          <p>{item.descripcion}</p>
                          <a 
                          download="" 
                          href={urlBase + item.link.toString().replace("/sites","").replace("/caustica","").replace("/files","")} 
                          className='down-load'
                          onClick={() => {
                            gtmDowloadDetalle(
                              props.detalleSliderArticulos[0]._source.title[0],
                              props.detalleSliderArticulos[0]._source.nombre_categoria[0],
                              props.detalleSliderArticulos[0]._source.subcategoria[0],
                              props.detalleSliderArticulos[0]._source.name[0],
                              props.detalleSliderArticulos[0]._source.nombre_personaje[0],
                              props.detalleSliderArticulos[0]._source.tipo[0] === "Informativo" ? "Articulo" : props.detalleSliderArticulos[0]._source.tipo[0],
                              p+1,
                              props.detalleSliderArticulos[0]._source.nid[0]
                              )
                          }}
                          ></a>
                        </div>
                      </div>  
                  );
                    
                })
            }

          </Carousel>
              
        </div>
    </div>
  );
}
  
};

export default SliderArticulos;
