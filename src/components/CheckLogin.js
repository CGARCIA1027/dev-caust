import LoginUserData from "./LoginUserData";
import LoginButton from "./LoginButton";
import { useState, useEffect } from "react";
var window = require("global/window")

const CheckLogin = () => { 
    const [session, setSession] = useState(undefined)  
    useEffect(() => {
        try {            
            var currentUser = window.localStorage.getItem('userEmptor')                  
        }catch(error){
            console.log(error)
        } 
        setSession(currentUser)             
    }, [])
    
    return(
        session===null  ||  session===undefined || session == "undefined"? <LoginButton /> : <LoginUserData />
    )  
};

export default CheckLogin;