import Link from "next/link";
import { slide as Menu } from "react-burger-menu";
import React from 'react'
import axios from 'axios';


class BurguerMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked1: false,
      clicked2: false,
      clicked3: false,
      clicked4: false,
      isLogged: false, 
      expiration: undefined,
      closeSession: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    try{
      var arrow = event.target;
      var index = event.target.attributes[0].value;
    }catch(err){}  
    if (index == "arrow-mobile-1") {
      this.setState((prevState) => ({
        clicked1: !prevState.clicked1,
      }));
      if (this.state.clicked1 == false) {
        arrow.style.transform = "rotate(180deg)";
      } else {
        arrow.style.transform = "rotate(360deg)";
      }
    } else if (index == "arrow-mobile-2") {
      this.setState((prevState) => ({
        clicked2: !prevState.clicked2,
      }));
      if (this.state.clicked2 == false) {
        arrow.style.transform = "rotate(180deg)";
      } else {
        arrow.style.transform = "rotate(360deg)";
      }
    } else if (index == "arrow-mobile-3") {
      this.setState((prevState) => ({
        clicked3: !prevState.clicked3,
      }));
      if (this.state.clicked3 == false) {
        arrow.style.transform = "rotate(180deg)";
      } else {
        arrow.style.transform = "rotate(360deg)";
      }
    } else if (index == "arrow-mobile-4") {
      this.setState((prevState) => ({
        clicked4: !prevState.clicked4,
      }));
      if (this.state.clicked4 == false) {
        arrow.style.transform = "rotate(180deg)";
      } else {
        arrow.style.transform = "rotate(360deg)";
      }
    }else if(event.currentTarget.id="close-session-text"){
      this.setState({
        closeSession: true
      })
    }
    
  }
  componentDidMount(){                     
    let catchUser = JSON.parse(window.localStorage.getItem('userEmptor'));    
    if(catchUser === null || catchUser === undefined){              
    }else{
      this.setState({
        isLogged: true,
      });
    }    
    try{
      const catchExpiration = JSON.parse(window.localStorage.getItem('exp'))     
      this.setState({
        expiration: catchExpiration.expiry
      })
    }catch(error){
        // console.log(error)
    }    
  }
  componentDidUpdate(){    
    const now = new Date()     
    if (now.getTime() > this.state.expiration || this.state.closeSession == true) {               	
      try{        
        var bearer = JSON.parse(window.localStorage.getItem('user')).accessToken;            
      }catch(err){
        // console.log(err)
      }
      let EMPTOR = JSON.parse(process.env.EMPTOR);
      let url = EMPTOR.BASE_DOMAIN + EMPTOR.LOGOUT.BASIC; 
      axios.post( url,{},{ headers: { Authorization: "Bearer " + bearer, },})
      .then(async (res) => {                
          if(res.data.data.message.includes("Sesión terminada")){
              let keysToRemove = ["email", "user", "authType", "exp", "userEmptor"];
              keysToRemove.forEach(k => localStorage.removeItem(k))
              open("/", "_self");
          }       
      })
      .catch((err) => {
          console.log(err)     
          alert('fallo el cierrre de sesión')
      });                
    }
  }
  
  render() {    
    return (
      <div className="menu-burguer-container">
        
        <Menu>
          <div className="profile-btn-mobile item-menu-mobile" hidden={this.state.isLogged ?  false : true}>
              <a className="menu-item bm-item perfil-redirect" href="/cuenta" >
                <img src="/static/assets/profile-image-mobile.png"></img>
                <div className="perfil-text-mobile">Perfil</div>
              </a>       
          </div>
          <div className="item-menu-mobile">
            <div className="item-capsule">
              
              <a id="home" className="menu-item bm-item" href="/actualidad">
                Actualidad
              </a>
              <div
                id="arrow-mobile-1"
                className="item-arrow-mobile"
                onClick={this.handleClick}
              ></div>
            </div>
            <ul
              className="actualidad-list subItems-mobile"
              hidden={this.state.clicked1 ? false : true}
            >
              <li>
                <Link href="/actualidad/politica">
                  <a>Política</a>
                </Link>
              </li>
              <li>
                <Link href="/actualidad/economia">
                  <a>Economia</a>
                </Link>
              </li>
              <li>
                <Link href="/actualidad/educacion">
                  <a>Eduación</a>
                </Link>
              </li>
              <li>
                <Link href="/actualidad/tecnologia">
                  <a>Tecnología</a>
                </Link>
              </li>
            </ul>
          </div>
          <div className="item-menu-mobile">
            <div className="item-capsule">
              <a id="about" className="menu-item bm-item" href="/bienestar">
                Bienestar
              </a>
              <div
                id="arrow-mobile-2"
                className="item-arrow-mobile"
                onClick={this.handleClick}
              ></div>
            </div>
            <ul
              className="bienestar-list subItems-mobile"
              hidden={this.state.clicked2 ? false : true}
            >
              <li>
                <Link href="/bienestar/salud-mental">
                  <a>Salud mental</a>
                </Link>
              </li>
              <li>
                <Link href="/bienestar/cambio-de-habitos">
                  <a>Cambio de hábitos</a>
                </Link>
              </li>
              <li>
                <Link href="/bienestar/sexo">
                  <a>Sexo</a>
                </Link>
              </li>
            </ul>
          </div>
          <div className="item-menu-mobile">
            <div className="item-capsule">
              <a id="contact" className="menu-item bm-item" href="/entorno">
                Entorno
              </a>
              <div
                id="arrow-mobile-3"
                className="item-arrow-mobile"
                onClick={this.handleClick}
              ></div>
            </div>
            <ul
              className="entorno-list subItems-mobile"
              hidden={this.state.clicked3 ? false : true}
            >
              <li>
                <Link href="/entorno/sociedades">
                  <a>Sociedades</a>
                </Link>
              </li>
              <li>
                <Link href="/entorno/activismo">
                  <a>Activismo</a>
                </Link>
              </li>
              <li>
                <Link href="/entorno/genero">
                  <a>Género</a>
                </Link>
              </li>
              <li>
                <Link href="/entorno/medio-ambiente">
                  <a>Medio Ambiente</a>
                </Link>
              </li>
            </ul>
          </div>
          <div className="item-menu-mobile">
            <div className="item-capsule">
              <a id="contact" className="menu-item bm-item" href="/cultura">
                Cultura
              </a>
              <div
                id="arrow-mobile-4"
                className="item-arrow-mobile"
                onClick={this.handleClick}
              ></div>
            </div>
            <ul
              className="entorno-list subItems-mobile"
              hidden={this.state.clicked4 ? false : true}
            >
              <li>
                <Link href="/cultura/arte-y-musica">
                  <a>Arte y Música</a>
                </Link>
              </li>
              <li>
                <Link href="/cultura/peliculas-series-y-libros">
                  <a>Películas, series y libros</a>
                </Link>
              </li>
              <li>
                <Link href="/cultura/viajes-y-gastronomia">
                  <a>Viajes y Gastronomía</a>
                </Link>
              </li>
              <li>
                <Link href="/cultura/gamers">
                  <a>Gamers</a>
                </Link>
              </li>
            </ul>
          </div>
          <div className="burguer-social-footer">
            <Link href="https://www.facebook.com/">
              <a className="fb" target="_blank">
                <img
                  src="/static/assets/ico-fb.svg"
                  alt="Facebook"
                  title="Facebook"
                />
              </a>
            </Link>
            <Link href="https://www.twitter.com/">
              <a className="tw" target="_blank">
                <img
                  src="/static/assets/ico-tw.svg"
                  alt="Twitter"
                  title="Twitter"
                />
              </a>
            </Link>
            <Link href="https://www.linkedin.com/">
              <a className="ig" target="_blank">
                <img
                  src="/static/assets/ico-ig.svg"
                  alt="linkedin"
                  title="linkedin"
                />
              </a>
            </Link>
          </div>
          <div
            className="item-menu-mobile close-session"
            id="burguer-close-session"
            onClick={this.handleClick}
          >
            <div className="item-capsule">
              <div id="close-session-text" className="menu-item bm-item close-session" onClick={this.handleClick} hidden={this.state.isLogged ?  false : true} >
                Cerrar Sesión
              </div>
            </div>
          </div>
        </Menu>
      </div>
    );
  }
}
export default BurguerMenu;
