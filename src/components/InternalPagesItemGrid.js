import React from "react";
import Link from "next/link";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { gtmNoticiasMasonryHomeSeccion, gtmDetalleMasonry, gtmPersonajesMasonry, gtmSentidosMasonry } from "../components/Gtm";
import { documentElement } from "min-document";
var document = require("global/document");
 
const InternalPagesItemGrid = ({ _source }) => { 
  var serverURL = process.env.URL_ELTIEMPO_IMG;
  const creationDate = new Date(_source.fecha_creacion[0] * 1000);

  const tipo = _source.content_type[0];
  
  if (tipo == "calendario") {
    let fondo_calendario = _source.fondo_calendario[0]
    let icono_tipo = _source.icono_tipo[0]
    return (
      <div className="content-card">
        <Link href={_source.enlace_calendario[0]}>
          <a 
          target="_blank"
          onClick={() => {
            let url = window.location.href.split("/")
            let menuItems = []

            for (var i = 0; i < 4; i++) {
              menuItems.push(document.getElementsByClassName("itemMenu")[i].childNodes[0].nodeValue.toLowerCase() )
            }
           
            let mItem1 = menuItems[0]
            let mItem2 = menuItems[1]
            let mItem3 = menuItems[2]
            let mItem4 = menuItems[3]
         
            if(menuItems.indexOf(mItem1) === 0 || menuItems.indexOf(mItem2) === 0 || menuItems.indexOf(mItem3) === 0 || menuItems.indexOf(mItem4) === 0){
              if(url[5] === "" || url[5] === undefined){

                 if(url[3] !== "paloma" && url[3] !== "rata" && url[3] !== "semaforo" && url[3] !== "payaso" && url[3] !== "ver" && url[3] !== "leer" && url[3] !== "oir" && url[3] !== "random"){
                  console.log("10A MasonrySecciones")
                  gtmNoticiasMasonryHomeSeccion(_source.title[0], "N/A", _source.subcategoria[0], "Leer", "N/A", creationDate.toLocaleDateString("en-US"), _source.tipo[0], "N/A", _source.nid[0]); 
                 }

                 if(url[3] == "paloma" || url[3] == "rata" || url[3] == "semaforo" || url[3] == "payaso"){
                  console.log("16A MasonryPersonajes")
                  gtmPersonajesMasonry(_source.title[0], "N/A", _source.subcategoria[0], "Leer", "N/A", creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                } 
                if(url[3] == "ver" || url[3] == "leer" || url[3] == "oir" || url[3] == "random"){
                  console.log("18A MasonryCategoria (OIR, Ver)")
                  gtmSentidosMasonry(_source.title[0], "N/A", _source.subcategoria[0], "Leer", "N/A", creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                } 

              }else {
                if(url[3] !== "paloma" && url[3] !== "rata" && url[3] !== "semaforo" && url[3] !== "payaso" && url[3] !== "ver" && url[3] !== "oir" && url[3] !== "leer" && url[3] !== "random"){
                  console.log("13A MasonryDetalles")
                  gtmDetalleMasonry(_source.title[0], "N/A", _source.subcategoria[0], "Leer", "N/A", creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                 }

              }
            }

            
          }}
          >
            <div
              className="calendar-home-grid-item-under-elements"
              style={{
                backgroundImage: `url(${
                  serverURL +
                  fondo_calendario
                  // .replace("/sites","").replace("/caustica","").replace("/files","")
                })`,
              }}
            >
              <div className="calendar-home-grid-item-classification">
                <div
                  className="calendar-home-grid-item-classification-text">
                  {_source.subcategoria[0]}
                </div>
                <div className="calendar-home-grid-item-banner-icon">
                  <LazyLoadImage
                    src={
                      serverURL +
                      icono_tipo
                    }
                    className="item-image-banner-icon"
                  />
                </div>
              </div>

              <h5 className="calendar-home-grid-item-title">
                {_source.title[0]}
              </h5>

              <div className="calendar-home-grid-item-footer">
                <h6 className="calendar-home-grid-item-date">
                  {creationDate.toLocaleDateString("es-ES")}
                </h6>
              </div>
            </div>
          </a>
        </Link>
      </div>
    );
  } else if (tipo == "detalle_video") {
    let categoria = _source.nombre_categoria[0]
    let subcategoria = _source.subcategoria[0]
    let icono_tipo = _source.icono_tipo[0]
    let icono_personaje = _source.icono_personaje[0]
    let urlAlias = ""
    if(_source.alias_url[0].includes('/node/')){
      urlAlias = "/node"
    }else {
      urlAlias = _source.alias_url[0].replace("/en","").replace("/detalle-articulo","") 
    }
    return (
      <div className="content-card video">
          <a 
          href={"/"+categoria + "/" + subcategoria + urlAlias + "/"+ _source.nid[0]}
          onClick={() => {
            let url = window.location.href.split("/")
            let menuItems = []

            for (var i = 0; i < 4; i++) {
              menuItems.push(document.getElementsByClassName("itemMenu")[i].childNodes[0].nodeValue.toLowerCase() )
            }
           
            let mItem1 = menuItems[0]
            let mItem2 = menuItems[1]
            let mItem3 = menuItems[2]
            let mItem4 = menuItems[3]
         
            if(menuItems.indexOf(mItem1) === 0 || menuItems.indexOf(mItem2) === 0 || menuItems.indexOf(mItem3) === 0 || menuItems.indexOf(mItem4) === 0){
              if(url[5] === "" || url[5] === undefined){

                 if(url[3] !== "paloma" && url[3] !== "rata" && url[3] !== "semaforo" && url[3] !== "payaso" && url[3] !== "ver" && url[3] !== "leer" && url[3] !== "oir" && url[3] !== "random"){
                  console.log("10A MasonrySecciones")
                  gtmNoticiasMasonryHomeSeccion(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Ver", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], "N/A", _source.nid[0]); 
                 }

                 if(url[3] == "paloma" || url[3] == "rata" || url[3] == "semaforo" || url[3] == "payaso"){
                  console.log("16A MasonryPersonajes")
                  gtmPersonajesMasonry(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Ver", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                } 
                if(url[3] == "ver" || url[3] == "leer" || url[3] == "oir" || url[3] == "random"){
                  console.log("18A MasonryCategoria (OIR, Ver)")
                  gtmSentidosMasonry(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Ver", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                } 

              }else {
                if(url[3] !== "paloma" && url[3] !== "rata" && url[3] !== "semaforo" && url[3] !== "payaso" && url[3] !== "ver" && url[3] !== "oir" && url[3] !== "leer" && url[3] !== "random"){
                  console.log("13A MasonryDetalles")
                  gtmDetalleMasonry(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Ver", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                 }

              }
            }

            
          }}
          >
            <div className="card-home-grid-item-banner-icon">
              <LazyLoadImage
                src={
                  serverURL +
                  icono_tipo
                }
                className="item-image-banner-icon"
                alt={_source.nombre_personaje[0]}
              />
            </div>
            <div className="home-grid-item-banner">
              <div className="home-grid-item-banner-background">
                <div className="boxInt">
                  <LazyLoadImage
                    src={
                      "https://img.youtube.com/vi/" +
                      _source.id_video_youtube[0] +
                      "/sddefault.jpg"
                    }
                    className="home-grid-item-image"
                    alt="Video"
                  />
                </div>
              </div>
            </div>
            <div
              className="home-grid-item-under-elements"
              style={{ boxShadow: "-6px 6px 0px 0px" + _source.field_color[0] }}
            >
              <div className="home-grid-item-classification">
                <div
                  className="home-grid-item-classification-text"
                  style={{ backgroundColor: _source.field_color[0] }}
                >
                  {_source.subcategoria[0]}
                </div>
              </div>

              <h5 className="home-grid-item-title">{_source.title[0]}</h5>

              <div className="home-grid-item-footer">
                <h6 className="home-grid-item-date">
                  {creationDate.toLocaleDateString("es-ES")}
                </h6>
                <div className="home-grid-item-footer-align">
                  <h6 className="home-grid-item-class">Video</h6>
                  <LazyLoadImage
                    src={
                      serverURL +
                      icono_personaje
                    }
                    className="home-grid-item-character"
                    alt={_source.nombre_personaje[0]}
                  />
                </div>
              </div>
            </div>
          </a>
        {/* </Link> */}
      </div>
    );
  } else if (tipo == "detalle_galeria") {
    let categoria = _source.nombre_categoria[0]
    let subcategoria = _source.subcategoria[0]
    let imagen_contenido_relacionado = _source.imagen_contenido_relacionado[0]
    let icono_tipo = _source.icono_tipo[0]
    let icono_personaje = _source.icono_personaje[0]
    let urlAlias = ""
    if(_source.alias_url[0].includes('/node/')){
      urlAlias = "/node"
    }else {
      urlAlias = _source.alias_url[0].replace("/en","").replace("/detalle-articulo","") 
    }
    return (
      <div className="content-card">
          <a 
          href={"/"+categoria + "/" + subcategoria + urlAlias + "/"+ _source.nid[0]}
          onClick={() => {
            let url = window.location.href.split("/")
            let menuItems = []

            for (var i = 0; i < 4; i++) {
              menuItems.push(document.getElementsByClassName("itemMenu")[i].childNodes[0].nodeValue.toLowerCase() )
            }
           
            let mItem1 = menuItems[0]
            let mItem2 = menuItems[1]
            let mItem3 = menuItems[2]
            let mItem4 = menuItems[3]
         
            if(menuItems.indexOf(mItem1) === 0 || menuItems.indexOf(mItem2) === 0 || menuItems.indexOf(mItem3) === 0 || menuItems.indexOf(mItem4) === 0){
              if(url[5] === "" || url[5] === undefined){

                 if(url[3] !== "paloma" && url[3] !== "rata" && url[3] !== "semaforo" && url[3] !== "payaso" && url[3] !== "ver" && url[3] !== "leer" && url[3] !== "oir" && url[3] !== "random"){
                  console.log("10A MasonrySecciones")
                  gtmNoticiasMasonryHomeSeccion(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Ver", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], "N/A", _source.nid[0]); 
                 }

                 if(url[3] == "paloma" || url[3] == "rata" || url[3] == "semaforo" || url[3] == "payaso"){
                  console.log("16A MasonryPersonajes")
                  gtmPersonajesMasonry(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Ver", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                } 
                if(url[3] == "ver" || url[3] == "leer" || url[3] == "oir" || url[3] == "random"){
                  console.log("18A MasonryCategoria (OIR, Ver)")
                  gtmSentidosMasonry(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Ver", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                } 

              }else {
                if(url[3] !== "paloma" && url[3] !== "rata" && url[3] !== "semaforo" && url[3] !== "payaso" && url[3] !== "ver" && url[3] !== "oir" && url[3] !== "leer" && url[3] !== "random"){
                  console.log("13A MasonryDetalles")
                  gtmDetalleMasonry(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Ver", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                 }

              }
            }

            
          }}
          >
            <div className="card-home-grid-item-banner-icon">
              <LazyLoadImage
                src={
                  serverURL +
                  icono_tipo
                }
                className="item-image-banner-icon"
                alt={_source.nombre_personaje[0]}
              />
            </div>

            <div className="home-grid-item-banner">
              <div className="home-grid-item-banner-background">
                <LazyLoadImage
                  src={
                    serverURL +
                    imagen_contenido_relacionado
                  }
                  className="home-grid-item-image"
                  alt={_source.title[0]}
                />
              </div>
            </div>

            <div
              className="home-grid-item-under-elements"
              style={{ boxShadow: "-6px 6px 0px 0px" + _source.field_color[0] }}
            >
              <div className="home-grid-item-classification">
                <div
                  className="home-grid-item-classification-text"
                  style={{ backgroundColor: _source.field_color[0] }}
                >
                  {_source.subcategoria[0]}
                </div>
              </div>

              <h5 className="home-grid-item-title">{_source.title[0]}</h5>

              <div className="home-grid-item-footer">
                <h6 className="home-grid-item-date">
                  {creationDate.toLocaleDateString("es-ES")}
                </h6>
                <div className="home-grid-item-footer-align">
                  <h6 className="home-grid-item-class">Galería</h6>
                  <LazyLoadImage
                    src={
                      serverURL +
                      icono_personaje
                    }
                    className="home-grid-item-character"
                    alt={_source.nombre_personaje[0]}
                  />
                </div>
              </div>
            </div>
          </a>
        {/* </Link> */}
      </div>
    );
  } else if (tipo == "detalle_del_articulo") {
    let categoria = _source.nombre_categoria[0]  
    let subcategoria = _source.subcategoria[0]
    let imagen_contenido_relacionado = _source.imagen_contenido_relacionado[0]
    let icono_personaje = _source.icono_personaje[0]
    let urlAlias = ""
    if(_source.alias_url[0].includes('/node/')){
      urlAlias = "/node"
    }else {
      urlAlias = _source.alias_url[0].replace("/en","").replace("/detalle-articulo","") 
    }
    return (
      <div className="content-card">
          <a 
          href={"/"+categoria + "/" + subcategoria + urlAlias + "/"+ _source.nid[0]}
          onClick={() => {
            let url = window.location.href.split("/")
            let menuItems = []

            for (var i = 0; i < 4; i++) {
              menuItems.push(document.getElementsByClassName("itemMenu")[i].childNodes[0].nodeValue.toLowerCase() )
            }
           
            let mItem1 = menuItems[0]
            let mItem2 = menuItems[1]
            let mItem3 = menuItems[2]
            let mItem4 = menuItems[3]
         
            if(menuItems.indexOf(mItem1) === 0 || menuItems.indexOf(mItem2) === 0 || menuItems.indexOf(mItem3) === 0 || menuItems.indexOf(mItem4) === 0){
              if(url[5] === "" || url[5] === undefined){

                 if(url[3] !== "paloma" && url[3] !== "rata" && url[3] !== "semaforo" && url[3] !== "payaso" && url[3] !== "ver" && url[3] !== "leer" && url[3] !== "oir" && url[3] !== "random"){
                  console.log("10A MasonrySecciones")
                  gtmNoticiasMasonryHomeSeccion(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Leer", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], "N/A", _source.nid[0]); 
                 }

                 if(url[3] == "paloma" || url[3] == "rata" || url[3] == "semaforo" || url[3] == "payaso"){
                  console.log("16A MasonryPersonajes")
                  gtmPersonajesMasonry(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Leer", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                } 
                if(url[3] == "ver" || url[3] == "leer" || url[3] == "oir" || url[3] == "random"){
                  console.log("18A MasonryCategoria (OIR, Ver)")
                  gtmSentidosMasonry(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Leer", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                } 

              }else {
                if(url[3] !== "paloma" && url[3] !== "rata" && url[3] !== "semaforo" && url[3] !== "payaso" && url[3] !== "ver" && url[3] !== "oir" && url[3] !== "leer" && url[3] !== "random"){
                  console.log("13A MasonryDetalles")
                  gtmDetalleMasonry(_source.title[0], _source.nombre_categoria[0], _source.subcategoria[0], "Leer", _source.nombre_personaje[0], creationDate.toLocaleDateString("en-US"), _source.tipo[0], _source.nid[0]);
                 }

              }
            }

            
          }}
          >
            <div className="home-grid-item-banner">
              <div className="home-grid-item-banner-background">
                <LazyLoadImage
                  src={
                    serverURL +
                    imagen_contenido_relacionado
                  }
                  className="home-grid-item-image"
                  alt={_source.title[0]}
                />
              </div>
            </div>

            <div
              className="home-grid-item-under-elements"
              style={{ boxShadow: "-6px 6px 0px 0px" + _source.field_color[0] }}
            >
              <div className="home-grid-item-classification">
                <div
                  className="home-grid-item-classification-text"
                  style={{ backgroundColor: _source.field_color[0] }}
                >
                  {_source.subcategoria[0]}
                </div>
              </div>

              <h5 className="home-grid-item-title">{_source.title[0]}</h5>

              <div className="home-grid-item-footer">
                <h6 className="home-grid-item-date">
                  {creationDate.toLocaleDateString("es-ES")}
                </h6>
                <div className="home-grid-item-footer-align">
                  <h6 className="home-grid-item-class">Articulo</h6>
                  <LazyLoadImage
                    src={
                      serverURL +
                      icono_personaje
                      // replace("/sites","").replace("/caustica","").replace("/files","")
                    }
                    className="home-grid-item-character"
                    alt={_source.nombre_personaje[0]}
                  />
                </div>
              </div>
            </div>
          </a>
        {/* </Link> */}
      </div>
    );
  }
};

export default InternalPagesItemGrid;
