import React, {useState,useEffect, useRef } from "react";
import axios from "axios";
/**
 * Componente funcional encargado de mostrar el contenido de articulo para citas
 */
const urlBase = process.env.URL_ELTIEMPO_IMG
const bannerContact = () => {

  let params = new URLSearchParams()
  const [bannerContactenos, setBannerContactenos] = useState([]);

  useEffect(() => {
    params.set("nid", "33")
    axios.get('/api/elastic/', {params}).then((res) => { 
      try{
        setBannerContactenos(res.data.body.hits.hits)  
        }catch(error){
          //console.log(error)
        }    
    });
  }, [])  

  let banner_desktop = ""
  let banner_mobile = ""
  let ciudad_departamento = ""
  let correo_contacto = ""
  let descripcion_medios_de_atencion = ""
  let direccion = ""
  let horarios_de_atencion = ""
  let horarios_de_atencion_2 = ""
  let icono_correo = ""
  let icono_horario_atencion = ""
  let icono_medios_atencion = ""
  let icono_telefono = ""
  let icono_ubicacion = ""
  let medio_de_contacto = ""
  let telefono_1 = ""
  let telefono_2 = ""
  let title = ""
  let titulo_telefono_1 = ""
  let titulo_telefono_2 = ""

  bannerContactenos.map((item, i) => { 

      banner_desktop = item._source.banner_desktop[0]
      banner_mobile = item._source.banner_mobile[0]
      titulo_telefono_1 =  item._source.titulo_telefono_1[0]
      titulo_telefono_2 =  item._source.titulo_telefono_2[0]
      telefono_1 =  item._source.telefono_1[0]
      telefono_2 =  item._source.telefono_2[0]
      icono_telefono = item._source.icono_telefono[0]
      icono_horario_atencion =  item._source.icono_horario_atencion[0]
      horarios_de_atencion = item._source.horarios_de_atencion[0]
      horarios_de_atencion_2 = item._source.horarios_de_atencion_2[0]
      icono_medios_atencion = item._source.icono_medios_atencion[0]
      descripcion_medios_de_atencion = item._source.descripcion_medios_de_atencion[0]
      icono_correo = item._source.icono_correo[0]
      medio_de_contacto = item._source.medio_de_contacto[0]
      icono_ubicacion = item._source.icono_ubicacion[0]
      ciudad_departamento = item._source.ciudad_departamento[0]
      direccion = item._source.direccion[0]
      title = item._source.title[0]
         
    })

  return (
    <>
    <div className="bannerContactenos">
      <div className='content-max contactenos'>
        <div className='content'>

          <div className='Contactenos'>
    
                <div className='info'>

                  <div className='content-info'>
                      <h1>Contáctenos</h1>

                      <div className='phone'>
                        <i style={{ 'backgroundImage': 'url('+urlBase + icono_telefono.toString().replace("/sites","").replace("/caustica","").replace("/files","")+')' }}></i>
                        <div className="copy">
                          <p><b>{titulo_telefono_1}: </b><a href={"tel:+57" + telefono_1}>{telefono_1}</a></p>
                          <p><b>{titulo_telefono_2}: </b><a href={"tel:+57031" + telefono_2}>{telefono_2}</a></p>
                        </div>
                      </div>
                      
                      <div className='calendar'>
                      <i style={{ 'backgroundImage': 'url('+urlBase + icono_horario_atencion.toString().replace("/sites","").replace("/caustica","").replace("/files","")+')' }}></i>
                        <div className="copy">
                          <p><b>Horario de atención</b></p>
                          <p>{horarios_de_atencion}</p>
                          <p>{horarios_de_atencion_2}</p>
                        </div>
                      </div>

                      <div className='email'>
                      <i style={{ 'backgroundImage': 'url('+urlBase + icono_medios_atencion.toString().replace("/sites","").replace("/caustica","").replace("/files","")+')' }}></i>
                        <div className="copy">
                          <p><b>Medios de atención</b></p>
                          <p>{descripcion_medios_de_atencion}</p>
                        </div>
                      </div>

                      <div className='email'>
                      <i style={{ 'backgroundImage': 'url('+urlBase + icono_correo.toString().replace("/sites","").replace("/caustica","").replace("/files","")+')' }}></i>
                        <div className="copy">
                          <p><b>Servicio al cliente</b></p>
                          <p>{medio_de_contacto}</p>
                        </div>
                      </div>

                      <div className='place'>
                      <i style={{ 'backgroundImage': 'url('+urlBase + icono_ubicacion.toString().replace("/sites","").replace("/caustica","").replace("/files","")+')' }}></i>
                        <div className="copy">
                          <p><b>Ubicación</b></p>
                          <p>{direccion}</p>
                          <p>{ciudad_departamento}</p>
                        </div>
                      </div>

                  </div>
                  
                </div>

                <div className='image-banner'>
                    <picture>
                          <source 
                            srcSet={urlBase + banner_mobile.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                            media="(max-width: 1024px)" />
                          <img
                            src={urlBase + banner_desktop.toString().replace("/sites","").replace("/caustica","").replace("/files","")}
                            alt={title}
                            title={title}
                      />
                      </picture>
                  </div>
              

            </div>  
            
        </div>
      </div>
      </div>
    </>
  );
};



export default bannerContact;