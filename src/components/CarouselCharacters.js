import Carousel from "react-elastic-carousel";
class ImageSliderCharacters extends React.Component {
    constructor(props) {
    super(props);
    var img1, img2, img3, img4;
    
    this.state = {
        images: [img1=["1","http://via.placeholder.com/400x450/3F51B5/fff", "banner", "banner"],
                 img2=["2","http://via.placeholder.com/400x450/3F51B5/fff", "banner", "banner"]]            
      }
    }
    render() {
        const Pagination = ({pages, activePage, onClick}) => {
            return (
          
              <div className="content-dots">
                <span>0{activePage+1}</span>
                  <div className="dots">
                      {pages.map(page => {
                        const isActivePage = activePage === page                        
                        return (
                          <div
                            className={isActivePage === true ? "dot active" : "dot"}
                            key={page}
                            onClick={() => onClick(page)}
                            active={isActivePage.toString()}
                          >
                          </div>
                        )
                      })}
                  </div>
                <span>0{pages.length}</span>
            </div>
            )
          }
      return (
        <>                                
          <Carousel 
            ref={ref => (this.carousel = ref)}
            enableAutoPlay={true}
            autoPlaySpeed={3500}
            focusOnSelect={true}
            enableMouseSwipe={false}
            renderPagination={Pagination}                     
            className="slider"
            itemsToShow={1}
          >
            {
              this.state.images.length > 0
                    ? this.state.images.map((item,q) => {                       
                            return (
                              <div className="character-caption-mobile" key={`q-${q}`}>                                  
                                  <div className="character-caption-container">
                                    <div className="quote-marks-top"></div>
                                        <div className="character-caption-name">
                                        RATA
                                        {/* {captionName} */}
                                        </div>
                                        <div className="character-caption-description">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                        {/* {captionDescription} */}
                                        </div>
                                        <div className="quote-marks-bottom"></div>
                                    </div>
                                                   
                                  <img                                    
                                    src={item[1]}
                                    alt={item[2]}
                                    title={item[3]}
                                  /> 
                                  <div className='button-character-mobile'>
                                    <a href="">
                                        	<div className='ver-mas'/>
                                    </a> 
                                  </div>                         
                              </div>                                                                      
                        );
                      }): "Not found items"
            }           
          </Carousel>                    
                 
        </>
      )
    }
  }
export default ImageSliderCharacters
