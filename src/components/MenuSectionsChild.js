import React from "react";
import Link from "next/link";
import { gtmLinksSections } from "../components/Gtm";
import { useRouter } from "next/router";
import { useEffect } from "react";
/**
 * Componente funcional encargado de mostrar el banner de subcategorias
 **/

const MenuSectionsChildConst = (props) => {
  const router = useRouter();
  const MenuChild = props.MenuSectionsChild.items;

  useEffect(() => {
    var pautaHeader = document.querySelector("aside.publicidad.pauta_header")
    if (pautaHeader.classList.contains("Up")) {
       try{
        document.querySelector("nav.menu-sections").classList.add("UpPautaHeader");
       }catch(error){
         //console.log(error)
       }
     }else {
      document.querySelector("body").classList.remove("scroll");
     }

  }, []);

  return (
    <>
      <nav className="menu-sections">
        <ul>
          {MenuChild.length > 0 ? (
            MenuChild.map((item) => {
              return (
                <li
                  key={item.id}
                  className={router.pathname === item.uri ? "active" : ""}
                >
                  <Link href={item.uri}>
                    <a
                     onClick={() => {
                      gtmLinksSections(item.section+' - '+item.title)
                    }}
                    >{item.title}</a>
                  </Link>
                </li>
              );
            })
          ) : (
            <li>No hay secciones</li>
          )}
        </ul>
      </nav>
    </>
  );
};

export default MenuSectionsChildConst;
