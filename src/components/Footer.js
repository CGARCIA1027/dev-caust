import Link from "next/link";
import React from "react";
import { ItemsFooter } from "./Footer/Navegacion";
import { gtmFooter, gtmSocialNetworkFooter, gtmDarkMode, gtmLastLinksFooter } from "../components/Gtm";
import { LazyLoadImage } from "react-lazy-load-image-component";

/**
 * Componente funcional encargado de mostrar el footer
 */

function openMenu(e) {  
  if(e.target.style.transform == '' || e.target.style.transform == "rotate(360deg)"){
    e.target.style.transform = "rotate(180deg)"
    //e.target.nextSibling.style.display = "block";
    e.target.parentNode.parentNode.classList.add("active");
  }else{
    e.target.style.transform = "rotate(360deg)"
    //e.target.nextSibling.style.display = "none";
    e.target.parentNode.parentNode.classList.remove("active");
  }
}

const Footer = () => {
  const items = ItemsFooter();

  return (
    <footer>
      <div className="content">
        <div className="brand">
          <Link href="/">
            <a className="caustica">
              <img
                src="/static/assets/brand-caustica.svg"
                alt="Cáustica"
                title="Cáustica"
              />
            </a>
          </Link>
        </div>
        <nav>
          {Object.keys(items).map((container, index) => (
            <div className="colum-footer" key={index}>
              <>
                {items[container].map((item) => (
                  <ul key={item.title}>
                    <h3>
                      <span onClick={openMenu}></span>
                      <Link href={item.uri}>
                        <a 
                        target="_blank"
                        onClick={() => {
                          gtmFooter(item.title)
                        }}
                        >{item.title}</a>
                      </Link>
                    </h3>
                    {item.items.map((element) => (
                      <li
                        key={element.id}
                        className={`property-${container}-item-${element.id}`}
                      >
                        <Link href={element.uri}>
                          <a
                          onClick={() => {
                            gtmFooter(item.title +' - '+ element.title)
                          }}
                          >{element.title}</a>
                        </Link>
                      </li>
                    ))}
                  </ul>
                ))}
              </>
            </div>
          ))}
        </nav>
        <div className="content-social">
          <div className="social">
            <p>Redes sociales</p>
            <Link href="https://www.facebook.com/caustica.col/">
              <a 
              className="fb" 
              target="_blank"
              onClick={() => {
                gtmSocialNetworkFooter("Facebook")
              }}
              >
                <LazyLoadImage
                  src="/static/assets/ico-fb.png"
                  alt="Facebook"
                  title="Facebook"
                  className="img-ico fb"
                />
              </a>
            </Link>
            <Link href="https://twitter.com/CausticaCo/">
              <a 
              className="tw" 
              target="_blank"
              onClick={() => {
                gtmSocialNetworkFooter("Twitter")
              }}
              >
                <LazyLoadImage
                  src="/static/assets/ico-tw.png"
                  alt="Twitter"
                  title="Twitter"
                  className="img-ico tw"
                />
              </a>
            </Link>
            <Link href="https://www.instagram.com/caustica.co/">
              <a 
              className="ig" 
              target="_blank"
              onClick={() => {
                gtmSocialNetworkFooter("Instagram")
              }}
              >
                <LazyLoadImage
                  src="/static/assets/ico-ig.png"
                  alt="Instagram"
                  title="Instagram"
                  className="img-ico ig"
                />
              </a>
            </Link>
            <Link href="https://www.tiktok.com/@caustica?">
              <a 
              className="tt" 
              target="_blank"
              onClick={() => {
                gtmSocialNetworkFooter("Tiktok")
              }}
              >
                <LazyLoadImage
                  src="/static/assets/ico-tt.png"
                  alt="TikTok"
                  title="TikTok"
                  className="img-ico tt"
                />
              </a>
            </Link>
          </div>
          <div className="modo-oscuro">
            <p>Modo oscuro</p>
            <label className="switch">
              <input type="checkbox" />
              <span 
              className="slider round"
              onClick={() => {
                gtmDarkMode("Modo Oscuro")
              }}
              >
              </span>
            </label>
          </div>
        </div>
      </div>
      <div className='footer-bottom'>
          <nav>
            <ul>
                <li>
                    <Link href="/aviso-de-privacidad/">
                      <a 
                      onClick={() => {
                        gtmLastLinksFooter("Aviso de privacidad")
                      }}>
                      Aviso de privacidad
                      </a>
                    </Link>
                </li>
                <li>
                    <Link href="/terminos-y-condiciones">
                      <a
                       onClick={() => {
                        gtmLastLinksFooter("Términos y condiciones")
                      }}
                      >
                      Términos y condiciones
                      </a>
                    </Link>
                </li>
                <li>
                    <Link href="https://www.eltiempo.com/legal/POLITICA_DE_TRATAMIENTO_Y_PROCEDIMIENTOS_EN_MATERIA_DE_PROTECCION_DE_DATOS_PERSONALES.pdf">
                      <a
                      target="_blank"
                      onClick={() => {
                        gtmLastLinksFooter("Política de datos personales")
                      }}
                      >
                      Política de datos personales
                      </a>
                    </Link>
                </li>
                <li>
                    <Link href="/politica-de-datos-de-navegacion/">
                      <a
                       onClick={() => {
                        gtmLastLinksFooter("Política de datos de navegación")
                      }}
                      >
                      Política de datos de navegación
                      </a>
                    </Link>
                </li>
                <li>
                    <Link href="/mapa-sitio/">
                      <a
                      onClick={() => {
                        gtmLastLinksFooter("Mapa del sitio")
                      }}
                      >
                      Mapa del sitio
                      </a>
                    </Link>
                </li>
                <li>
                    <Link href="/contactenos/">
                      <a
                       onClick={() => {
                        gtmLastLinksFooter("Contáctenos")
                      }}
                      >
                      Contáctenos
                      </a>
                    </Link>
                </li>
            </ul>
          </nav>
        </div>
    </footer>
  );
};

export default Footer;