import Link from "next/link";
import { LazyLoadImage } from "react-lazy-load-image-component";

/**
 * Componente funcional encargado de mostrar el banner de articulos
 **/

const BannerMenuSectionsConst = (props) => {

  let categoria = ""
  let color=""
  let imgBack=""
  let imgBackM=""
  let imgFront=""

if(props.categoria != undefined){

  if(props.categoria == "Actualidad"){
    color="#e62e2e"
    categoria="Actualidad"
    imgBack="back-sections-actualidad.jpg"
    imgBackM="back-sections-actualidad-m.jpg"
    imgFront="main-sections-actualidad.png"
  }

  if(props.categoria == "Bienestar"){
    color="#986b58"
    categoria="Bienestar"
    imgBack="back-sections-bienestar.jpg"
    imgBackM="back-sections-bienestar-m.jpg"
    imgFront="main-sections-bienestar.png"
  }

  if(props.categoria == "Entorno"){
    color="#f2ca50"
    categoria="Entorno"
    imgBack="back-sections-entorno.jpg"
    imgBackM="back-sections-entorno-m.jpg"
    imgFront="main-sections-entorno.png"
  }

  if(props.categoria == "Cultura"){
    color="#21c0a6"
    categoria="Cultura"
    imgBack="back-sections-cultura.jpg"
    imgBackM="back-sections-cultura-m.jpg"
    imgFront="main-sections-cultura.png"
  }

  return (
    <>
      <div className="section-menu-banner">
        <div className="content">
          <nav className="main-breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item ">
                <Link href="/">
                  <a>Home</a>
                </Link>
              </li>
              <li className="breadcrumb-item active">{categoria}</li>
            </ol>
          </nav>

          <div className="item">
            <h1>{categoria}</h1>
            <LazyLoadImage
              src={"/static/assets/"+imgFront}
              alt={categoria}
              title={categoria}
            />
          </div>
        </div>

        <div className="background-color" style={{ backgroundColor: color }} />
        <div className="background">
          <picture>
            <source
              srcSet={"/static/assets/"+imgBackM}
              media="(max-width: 768px)"
            />
            <LazyLoadImage
              src={"/static/assets/"+imgBack}
              alt={categoria}
              title={categoria}
            />
          </picture>
        </div>
      </div>
    </>
  );
}else {
  return (
    <></>
  )
}

};

export default BannerMenuSectionsConst;
