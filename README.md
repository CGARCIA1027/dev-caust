# Cáustica 

Website Cáustica

### Pre-requisitos 📋

1. NodeJS **10.15.3** o superior
2. NPM **6.9.0** o superior

### Instalación 🔧

1. Clonar el repositorio

```shell
  git clone <repository>
```

2. Instalar las dependencias

```shell
  npm install
```

3. Probar en entorno de desarrollo local

```shell
  npm run dev
```

## Deployment 📦


1. Para compilar en producción, se debe ejecutar el siguiente comando, automaticamente Next.js construye la aplicación de producción en la carpeta .next

```shell
  npm run build
```

2. Despues de compilar, puede iniciar un servidor de Node, con la aplicacion construida en local

```shell
  npm run start
```

## Stack 🛠️

* [NPM](https://www.npmjs.com/) - Manejador de dependencias
* [NextJS](https://nextjs.org/docs#how-to-use) - Framework de React.js para el Frontend
* [Express.js](https://expressjs.com/es/) - Framework de Node para manejadores de rutas