require("./process.env.config.js").execute();
const WebpackPwaManifest = require("webpack-pwa-manifest");
const withImages = require("next-images");
// const withBabelMinify = require("next-babel-minify")();
const path = require("path");

module.exports = withImages(
  // withBabelMinify( 
       
    {
    env: {
      URL_DOMAIN_ELTIEMPO: process.env.URL_DOMAIN_ELTIEMPO,
      GOOGLE: process.env.GOOGLE,
      URL_ELTIEMPO: process.env.URL_ELTIEMPO,
      URL_ELTIEMPO_IMG: process.env.URL_ELTIEMPO_IMG,
      DETALLE_GALERIA_URL: process.env.DETALLE_GALERIA_URL,
      EMPTOR: process.env.EMPTOR,
      CLIENT_ID: process.env.CLIENT_ID,
      BASE_SERVER: process.env.BASE_SERVER,
      ELASTIC_NODE: process.env.ELASTIC_NODE,
      ELASTIC_INDEX: process.env.ELASTIC_INDEX,
    },
    trailingSlash: true,
    future: {
      webpack5: true,
    },
    devServer: {     
      hot: false, 
      compress: true,     
    },
    watch: false,
    cache: { type: "filesystem" },
    webpack: (config, { isServer, dev }) => {      
      config.experiments = {};
          // console.log(isServer + '----' + dev);
          
      if (!isServer && !dev) { 
        
        new WebpackPwaManifest({
          filename: "/static/manifest.json",
          name: "Website Caustica",
          short_name: "Caustica",
          description: "Aplicación del Website Caustica usando NextJS",
          background_color: "#fff",
          theme_color: "#5755d9",
          display: "standalone",
          orientation: "portrait",
          fingerprints: false,
          inject: false,
          start_url: "/",
          hot: false, 
          compress: true,
          ios: {
            "apple-mobile-web-app-title": "Website Caustica PWA",
            "apple-mobile-web-app-status-bar-style": "#5755d9",
          },
          icons: [
            {
              src: path.resolve("./public/static/assets/avatar_ins.png"),
              size: "130x52",
            },
          ],
          includeDirectory: true,
          publicPath: "..",
          devServer: {     
            hot: false, 
            compress: false,     
          },
          watch: false,
          
        });
        
      }
      return config;
    },
  }
  // )
);
