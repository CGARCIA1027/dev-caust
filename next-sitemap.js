module.exports = {
    siteUrl: process.env.BASE_DOMAIN || 'https://www.caustica.co',
    generateRobotsTxt: true,
    exclude: ["/server-sitemap.xml"],
    robotsTxtOptions: {
      policies: [
        {
            userAgent: "*",
            allow: "/",
            disallow: ["/src/pages/404", "/src/pages/cuenta"],    
        },
      ],
      additionalSitemaps: ["https://www.caustica.co/server-sitemap.xml"],
    },
  }